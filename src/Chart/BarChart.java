/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Chart;

/**
 *
 * @author PHUC
 */
import Model.DoanhThuDoanModel;
import Model.ThongKeDoanhThuModel;
import java.util.ArrayList;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author TVD
 */
public class BarChart {

    public static JFreeChart createChart(ArrayList<ThongKeDoanhThuModel> listDoanhThu) {
        JFreeChart barChart = ChartFactory.createBarChart(
                "BIỂU ĐỒ DOANH THU THEO TỪNG THÁNG",
                "Tháng", "Lợi nhuận",
                createDataset(listDoanhThu), PlotOrientation.VERTICAL, false, false, false);
        return barChart;
    }

    private static CategoryDataset createDataset(ArrayList<ThongKeDoanhThuModel> listDoanhThu) {
        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for(ThongKeDoanhThuModel dt : listDoanhThu){
            dataset.addValue(dt.getLoiNhuan(), "Lợi nhuận", dt.getThang());
        }
        return dataset;
    }
}
