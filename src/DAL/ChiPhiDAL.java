package DAL;

import Config.MyConnectUnit;
import Model.ChiPhiModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letoan
 */
public class ChiPhiDAL {

    MyConnectUnit connect;
    public ArrayList<ChiPhiModel> allChiPhi = new ArrayList<ChiPhiModel>();

    public ChiPhiDAL() {
        this.connect = Config.DAL.getDAL();
        try {
            allChiPhi = getAllCP(null, null);
        } catch (Exception ex) {
            Logger.getLogger(KhachHangDAL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<ChiPhiModel> getAllCP(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("chiphi", condition, OrderBy);
        ArrayList<ChiPhiModel> listCP = new ArrayList<>();
        while (rs.next()) {
            listCP.add(new ChiPhiModel(
                    rs.getString("MaChiPhi"),
                    rs.getString("MaDoan"),
                    rs.getString("MaLoaiChiPhi"),
                    rs.getInt("SoTien"))
            );
        }
        return listCP;
    }

    public void insert(ChiPhiModel chiPhi) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaChiPhi", chiPhi.getMaChiPhi());
        map.put("MaDoan", chiPhi.getMaDoan());
        map.put("MaLoaiChiPhi", chiPhi.getMaLoaiChiPhi());
        map.put("SoTien", chiPhi.getSoTien());
        this.connect.insert("chiphi", map);
    }

    public void update(ChiPhiModel chiPhi) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaChiPhi", chiPhi.getMaChiPhi());
        map.put("MaDoan", chiPhi.getMaDoan());
        map.put("MaLoaiChiPhi", chiPhi.getMaLoaiChiPhi());
        map.put("SoTien", chiPhi.getSoTien());
        this.connect.update("chiphi", map, "MaChiPhi='" + chiPhi.getMaChiPhi() + "'");
    }

    public void delete(String maCP) throws Exception {
        this.connect.delete("chiphi", "MaChiPhi='" + maCP + "'");
    }
}
