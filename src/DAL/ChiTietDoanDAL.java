package DAL;

import Config.MyConnectUnit;
import Model.ChiTietDoanModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author letoan
 */
public final class ChiTietDoanDAL {

    MyConnectUnit connect;

    public ChiTietDoanDAL() {
        this.connect = Config.DAL.getDAL();

    }

    public ArrayList<ChiTietDoanModel> getAllChiTietDoan(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("chitietdoan", condition, OrderBy);
        ArrayList<ChiTietDoanModel> listChiTietDoan = new ArrayList<>();
        while (rs.next()) {
            listChiTietDoan.add(new ChiTietDoanModel(
                    rs.getString("MaDoan"),
                    rs.getString("MaKhachHang")
            ));
        }
        return listChiTietDoan;
    }

    public ArrayList<ChiTietDoanModel> getChiTietDoanByMaDoan(String maDoan) throws Exception {
        ArrayList<ChiTietDoanModel> listChiTietDoan = this.getAllChiTietDoan("MaDoan='" + maDoan + "'", null);
        if (listChiTietDoan.size() > 0) {
            return listChiTietDoan;
        }
        return new ArrayList<>();
    }

    public void insertChiTietDoan(ChiTietDoanModel ctd) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaDoan", ctd.getMaDoan());
        map.put("MaKhachHang", ctd.getMaKhachHang());
        this.connect.insert("chitietdoan", map);
    }
    
    public void deleteChiTietDoan(String maKH) throws Exception {
        this.connect.delete("chitietdoan", "MaKhachHang='" + maKH + "'");
    }
    
    public void deleteChiTietDoan(String maKH, String maDoan) throws Exception {
        this.connect.delete("chitietdoan", "MaKhachHang='" + maKH + "'" + " and MaDoan='" + maDoan + "'");
    }

}
