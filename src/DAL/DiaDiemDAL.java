package DAL;

import Config.MyConnectUnit;
import Model.DiaDiemModel;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class DiaDiemDAL {

    MyConnectUnit connect;

    public DiaDiemDAL() {
        this.connect = Config.DAL.getDAL();
    }

    public ArrayList<DiaDiemModel> getDiaDiem(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("diadiem", condition, OrderBy);
        ArrayList<DiaDiemModel> list_diadiem = new ArrayList<>();
        while (rs.next()) {
            list_diadiem.add(new DiaDiemModel(
                    rs.getString("MaDiaDiem"),
                    rs.getString("TenDiaDiem"))
            );
        }
        return list_diadiem;
    }

//    public ArrayList<DiaDiemModel> getDiaDiem(String condition) throws Exception {
//        return getDiaDiem(condition, null);
//    }
//
//    public ArrayList<DiaDiemModel> getDiaDiem() throws Exception {
//        return getDiaDiem(null);
//    }

    public String getTenDiaDiemByMa(String maDD) throws Exception {
        ArrayList<DiaDiemModel> list_dd = this.getDiaDiem("MaDiaDiem='" + maDD + "'", null);
        if (list_dd.size() > 0) {
            return list_dd.get(0).getTenDiaDiem();
        }
        return null;
    }
    
    public String getMaDiaDiemByTen(String tenDD) throws Exception {
        ArrayList<DiaDiemModel> list_dd = this.getDiaDiem("TenDiaDiem='" + tenDD + "'", null);
        if (list_dd.size() > 0) {
            return list_dd.get(0).getMaDiaDiem();
        }
        return null;
    }
}
