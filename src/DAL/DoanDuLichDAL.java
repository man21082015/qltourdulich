/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import Config.MyConnectUnit;
import Model.DoanDuLichModel;
import Model.TourModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author PHUC
 */
public class DoanDuLichDAL {

    MyConnectUnit connect;

    public DoanDuLichDAL() {
        this.connect = Config.DAL.getDAL();
    }

    public ArrayList<DoanDuLichModel> getAllDoanDuLich(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("doandulich", condition, OrderBy);
        ArrayList<DoanDuLichModel> list_doandulich = new ArrayList<>();
        while (rs.next()) {
            list_doandulich.add(new DoanDuLichModel(rs.getString("MaDoan"),
                    rs.getString("MaTour"),
                    rs.getString("NgayKhoiHanh"),
                    rs.getString("NgayKetThuc"),
                    rs.getDouble("DoanhThu"),
                    rs.getString("HanhTrinh"),
                    rs.getString("KhachSan")));
        }
        return list_doandulich;
    }

//    public ArrayList<TourModel> getTours(String condition) throws Exception{
//        return getAllTours(condition, null);
//    }
//    public ArrayList<TourModel> getTours() throws Exception{
//        return getTours(null);
//    }
//    public NhanVienDTO getNhanVienByMaNV(String manv) throws Exception{
//        ArrayList<NhanVienDTO> list_nv=this.getNhanViens("MaNV='"+manv+"'");
//        if(list_nv.size()>0){
//            return list_nv.get(0);
//        }
//        return null;
//    }
//    public void Insert(TourDuLichModel tour) throws Exception{
//        HashMap<String,Object> map=new HashMap<>();
//        map.put("MaTour", tour.getMaTour());        
//        map.put("TenTour", tour.getTenTour());
//        map.put("DacDiem", tour.getDacDiem());        
//        this.connect.insert("tourdulich", map);
//    }
//    public void Inserts(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv:list_nv){
//            this.Insert(nv);
//        }
//    }
//    public void Delete(NhanVienDTO nv) throws Exception{
//        this.connect.delete("nhanvien", "MaNV='"+nv.getMaNV()+"'");
//    }
//    public void Deletes(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv : list_nv){
//            this.Delete(nv);
//        }
//    }
    public void update(DoanDuLichModel doandulich) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaDoan", doandulich.getMaDoan());
        map.put("MaTour", doandulich.getMaTour());
        map.put("NgayKhoiHanh", doandulich.getNgayKhoiHanh());
        map.put("NgayKetThuc", doandulich.getNgayKetThuc());
        map.put("DoanhThu", doandulich.getDoanhThu());
        map.put("HanhTrinh", doandulich.getHanhTrinh());
        map.put("KhachSan", doandulich.getKhachSan());
        

        this.connect.update("doandulich", map, "MaDoan='" + doandulich.getMaDoan()+ "' AND " + "MaTour='" + doandulich.getMaTour()+ "'");
    }
//    public void Updates(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv:list_nv){
//            this.Update(nv);
//        }
//    }

}
