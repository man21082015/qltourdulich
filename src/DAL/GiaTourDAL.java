package DAL;

import Config.MyConnectUnit;
import Model.GiaTourModel;
import Model.KhachHangModel;
import Model.LoaiHinhModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author letoan
 */
public class GiaTourDAL {

    MyConnectUnit connect;

    public GiaTourDAL() {
        this.connect = Config.DAL.getDAL();
    }

    public ArrayList<GiaTourModel> getGiaTours(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("giatour", condition, OrderBy);
        ArrayList<GiaTourModel> list_gia_tour = new ArrayList<>();
        while (rs.next()) {
            list_gia_tour.add(new GiaTourModel(
                    rs.getString("MaGiaTour"),
                    rs.getString("MaTour"),
                    rs.getDate("ThoiGianBD").toLocalDate(),
                    rs.getDate("ThoiGianKT").toLocalDate(),
                    rs.getInt("GiaTien")
            ));
        }
        return list_gia_tour;
    }

    public ArrayList<GiaTourModel> getListGiaByMaTour(String maTour) throws Exception {
        ArrayList<GiaTourModel> list_gia = this.getGiaTours("MaTour='" + maTour + "'", null);
        if (list_gia.size() > 0) {
            return list_gia;
        }
        return null;
    }

    public void insert(GiaTourModel gia) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaGiaTour", gia.getMaGiaTour());
        map.put("ThoiGianBD", gia.getThoiGianBD());
        map.put("ThoiGianKT", gia.getThoiGianKT());
        map.put("GiaTien", gia.getGiaTien());
        map.put("MaTour", gia.getMaTour());
        this.connect.insert("giatour", map);
    }

    public void update(GiaTourModel gia) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("ThoiGianBD", gia.getThoiGianBD());
        map.put("ThoiGianKT", gia.getThoiGianKT());
        map.put("GiaTien", gia.getGiaTien());
        this.connect.update("giatour", map, "MaGiaTour='" + gia.getMaGiaTour()+ "'");
    }
}
