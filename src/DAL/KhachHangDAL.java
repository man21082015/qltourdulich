package DAL;

import Config.MyConnectUnit;
import Model.KhachHangModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letoan
 */
public final class KhachHangDAL {

    MyConnectUnit connect;
    public ArrayList<KhachHangModel> allKhachHang = new ArrayList<KhachHangModel>();

    public KhachHangDAL() {
        this.connect = Config.DAL.getDAL();
        try {
            allKhachHang = getAllKhachHang(null, null);
        } catch (Exception ex) {
            Logger.getLogger(KhachHangDAL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<KhachHangModel> getAllKhachHang(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("khachhang", condition, OrderBy);
        ArrayList<KhachHangModel> listKhachHang = new ArrayList<>();
        while (rs.next()) {
            listKhachHang.add(new KhachHangModel(
                    rs.getString("MaKhachHang"),
                    rs.getString("HoTen"),
                    rs.getString("SoCMND"),
                    rs.getString("DiaChi"),
                    rs.getString("GioiTinh"),
                    rs.getString("SDT"),
                    rs.getString("QuocTich"))
            );
        }
        return listKhachHang;
    }

    public KhachHangModel getKhachHang(String maKH) throws Exception {
        ArrayList<KhachHangModel> listKhachHang = this.getAllKhachHang("MaKhachHang='" + maKH + "'", null);
        if (listKhachHang.size() > 0) {
            return listKhachHang.get(0);
        }
        return null;
    }

    public void insert(KhachHangModel khachHang) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaKhachHang", khachHang.getMaKhachHang());
        map.put("HoTen", khachHang.getHoTen());
        map.put("SoCMND", khachHang.getSoCMND());
        map.put("DiaChi", khachHang.getDiaChi());
        map.put("GioiTinh", khachHang.getGioiTinh());
        map.put("SDT", khachHang.getSDT());
        map.put("QuocTich", khachHang.getQuocTich());
        this.connect.insert("khachhang", map);
    }

    public void delete(String maKH) throws Exception {
        this.connect.delete("khachhang", "MaKhachHang='" + maKH + "'");
    }

    public void update(KhachHangModel khachHang) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaKhachHang", khachHang.getMaKhachHang());
        map.put("HoTen", khachHang.getHoTen());
        map.put("SoCMND", khachHang.getSoCMND());
        map.put("DiaChi", khachHang.getDiaChi());
        map.put("GioiTinh", khachHang.getGioiTinh());
        map.put("SDT", khachHang.getSDT());
        map.put("QuocTich", khachHang.getQuocTich());

        this.connect.update("khachhang", map, "MaKhachHang='" + khachHang.getMaKhachHang()+ "'");
    }
}
