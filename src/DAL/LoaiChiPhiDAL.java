package DAL;

import Config.MyConnectUnit;
import Model.LoaiChiPhiModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letoan
 */
public class LoaiChiPhiDAL {

    MyConnectUnit connect;
    public ArrayList<LoaiChiPhiModel> allLoaiCP = new ArrayList<LoaiChiPhiModel>();

    public LoaiChiPhiDAL() {
        this.connect = Config.DAL.getDAL();
        try {
            allLoaiCP = getAllLoaiCP(null, null);
        } catch (Exception ex) {
            Logger.getLogger(KhachHangDAL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<LoaiChiPhiModel> getAllLoaiCP(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("loaichiphi", condition, OrderBy);
        ArrayList<LoaiChiPhiModel> listLoaiCP = new ArrayList<>();
        while (rs.next()) {
            listLoaiCP.add(new LoaiChiPhiModel(
                    rs.getString("MaLoaiChiPhi"),
                    rs.getString("TenLoaiChiPhi")
            )
            );
        }
        return listLoaiCP;
    }
    

    public LoaiChiPhiModel getLoaiCPByMaCP(String maLoaiCP) throws Exception {
        ArrayList<LoaiChiPhiModel> list_lh = this.getAllLoaiCP("MaLoaiChiPhi='" + maLoaiCP + "'", null);
        if (list_lh.size() > 0) {
            return list_lh.get(0);
        }
        return null;
    }

}
