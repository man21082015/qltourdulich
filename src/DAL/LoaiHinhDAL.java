/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import Config.MyConnectUnit;
import Model.LoaiHinhModel;
//import Model.TourModel;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author man21
 */
public class LoaiHinhDAL {

    MyConnectUnit connect;

    public LoaiHinhDAL() {
        this.connect = Config.DAL.getDAL();
    }

    public ArrayList<LoaiHinhModel> getAllLoaiHinh(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("loaihinhdulich", condition, OrderBy);
        ArrayList<LoaiHinhModel> list_LoaiHinh = new ArrayList<>();
        while (rs.next()) {
            list_LoaiHinh.add(new LoaiHinhModel(rs.getString("MaLoaiHinh"),
                    rs.getString("TenLoaiHinh")));
        }
        return list_LoaiHinh;
    }

//    public ArrayList<LoaiHinhModel> getLoaiHinh(String condition) throws Exception {
//        return getAllLoaiHinh(condition, null);
//    }
//
//    public ArrayList<LoaiHinhModel> getLoaiHinh() throws Exception {
//        return getLoaiHinh(null);
//    }
    public LoaiHinhModel getLoaiHinhByMaLH(String malh) throws Exception {
        ArrayList<LoaiHinhModel> list_lh = this.getAllLoaiHinh("MaLoaiHinh='" + malh + "'", null);
        if (list_lh.size() > 0) {
            return list_lh.get(0);
        }
        return null;
    }

}
