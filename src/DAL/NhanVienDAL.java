package DAL;

import Config.MyConnectUnit;
import Model.NhanVienModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letoan
 */
public final class NhanVienDAL {

    MyConnectUnit connect;
    public ArrayList<NhanVienModel> allNhanVien = new ArrayList<NhanVienModel>();

    public NhanVienDAL() {
        this.connect = Config.DAL.getDAL();
        try {
            allNhanVien = getAllNhanVien(null, null);
        } catch (Exception ex) {
            Logger.getLogger(NhanVienDAL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<NhanVienModel> getAllNhanVien(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("nhanvien", condition, OrderBy);
        ArrayList<NhanVienModel> listNhanVien = new ArrayList<>();
        while (rs.next()) {
            listNhanVien.add(new NhanVienModel(
                    rs.getString("MaNV"),
                    rs.getString("HoTenNV"),
                    rs.getDate("NgaySinh").toLocalDate(),
                    rs.getString("GioiTinh"),
                    rs.getString("SĐT"),
                    rs.getString("DiaChi"),
                    rs.getString("Luong"),
                    rs.getString("TrangThai"))
            );
        }
        return listNhanVien;
    }


    public NhanVienModel getNhanVienByMaNV(String maNV) throws Exception {

        ArrayList<NhanVienModel> listNhanVien = this.getAllNhanVien("MaNV='" + maNV + "'", null);
        if (listNhanVien.size() > 0) {
            return listNhanVien.get(0);
        }
        return null;
    }
    
    public void Update(NhanVienModel nv) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
//        map.put("MaNV", nv.getMaNhanVien());
        map.put("HoTenNV", nv.getTenNhanVien());
        map.put("NgaySinh", nv.getNgaySinh());
        map.put("GioiTinh", nv.getGioiTinh());
        map.put("SĐT", nv.getSDT());
        map.put("DiaChi", nv.getDiaChi());
        map.put("Luong", nv.getLuong());
        map.put("TrangThai", nv.getTrangThai());

        this.connect.update("nhanvien", map, "MaNV='" + nv.getMaNhanVien()+ "'");
    }
    
    public void Insert(NhanVienModel nv) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaNV", nv.getMaNhanVien());
        map.put("HoTenNV", nv.getTenNhanVien());
        map.put("NgaySinh", nv.getNgaySinh());
        map.put("GioiTinh", nv.getGioiTinh());
        map.put("SĐT", nv.getSDT());
        map.put("DiaChi", nv.getDiaChi());
        map.put("Luong", nv.getLuong());
        map.put("TrangThai", nv.getTrangThai());

        this.connect.insert("nhanvien", map);

    }
}
