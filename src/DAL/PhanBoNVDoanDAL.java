/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import Config.MyConnectUnit;
import Model.PhanBoNVDoanModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PHUC
 */
public class PhanBoNVDoanDAL {

    MyConnectUnit connect;
    public ArrayList<PhanBoNVDoanModel> allNhanVien = new ArrayList<PhanBoNVDoanModel>();

    public PhanBoNVDoanDAL() {
        this.connect = Config.DAL.getDAL();
        try {
            allNhanVien = getAllNhanVien(null, null);
        } catch (Exception ex) {
            Logger.getLogger(NhanVienDAL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<PhanBoNVDoanModel> getAllNhanVien(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("phanbonvdoan", condition, OrderBy);
        ArrayList<PhanBoNVDoanModel> listNhanVien = new ArrayList<>();
        while (rs.next()) {
            listNhanVien.add(new PhanBoNVDoanModel(
                    rs.getString("MaNhanVien"),
                    rs.getString("MaDoan"),
                    rs.getString("NhiemVu"))
            );
        }
        return listNhanVien;
    }

    public ArrayList<PhanBoNVDoanModel> getNhanVienByMaDoan(String maDoan) throws Exception {
        ArrayList<PhanBoNVDoanModel> listNhanVien = this.getAllNhanVien("MaDoan='" + maDoan + "'", null);
        return listNhanVien;
    }

    public void phanCongNhanVien(PhanBoNVDoanModel nv) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaNhanVien", nv.getMaNhanVien());
        map.put("MaDoan", nv.getMaDoan());
        map.put("NhiemVu", nv.getNhiemVu());

        this.connect.insert("phanbonvdoan", map);
    }

    public void deletePhanCong(PhanBoNVDoanModel nv) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        String maDoan = nv.getMaDoan();
        String maNhanVien = nv.getMaNhanVien();
        this.connect.delete("phanbonvdoan", "MaNhanVien='" + maNhanVien + "' and MaDoan='" + maDoan + "'");
    }
}
