/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import Config.MyConnectUnit;
import Model.ThamQuanModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 *
 * @author PHUC
 */
public class ThamQuanDAL {

    MyConnectUnit connect;

    public ThamQuanDAL() {
        this.connect = Config.DAL.getDAL();
    }

    public ArrayList<ThamQuanModel> getThamQuan(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("thamquan", condition, OrderBy);
        ArrayList<ThamQuanModel> list_thamquan = new ArrayList<>();
        while (rs.next()) {
            list_thamquan.add(new ThamQuanModel(rs.getString("MaTour"),
                    rs.getString("MaDiaDiem"),
                    rs.getInt("ThuTu")));
        }
        return list_thamquan;
    }

    public ArrayList<ThamQuanModel> getThamQuan(String condition) throws Exception {
        return getThamQuan(condition, null);
    }

    public ArrayList<ThamQuanModel> getThamQuan() throws Exception {
        return getThamQuan(null);
    }

    public ArrayList<ThamQuanModel> getThamQuanByMaTour(String maTour) throws Exception {
        ArrayList<ThamQuanModel> list_tq = this.getThamQuan("MaTour='" + maTour + "'");
        if (list_tq.size() > 0) {
            Collections.sort(list_tq, ThamQuanModel.sttComparator);
            return list_tq;
        }
        return null;
    }

    public void Insert(ThamQuanModel tq) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaTour", tq.getMaTour());
        map.put("MaDiaDiem", tq.getMaDiaDiem());
        map.put("ThuTu", tq.getThutu());

        this.connect.insert("thamquan", map);
    }
//    public void Inserts(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv:list_nv){
//            this.Insert(nv);
//        }
//    }

    public void Delete(ThamQuanModel tq) throws Exception {
        this.connect.delete("thamquan", "MaTour='" + tq.getMaTour() + "' AND " + "MaDiaDiem='" + tq.getMaDiaDiem() + "'");
    }
//    public void Deletes(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv : list_nv){
//            this.Delete(nv);
//        }
//    }
    public void Update(ThamQuanModel tq) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
//        map.put("MaTour", tq.getMaTour());
//        map.put("MaDiaDiem", tq.getMaTour());
        map.put("ThuTu", tq.getThutu());

        this.connect.update("thamquan", map, "MaTour='" + tq.getMaTour() + "' AND " + "MaDiaDiem='" + tq.getMaDiaDiem() + "'");
    }

//    public void Updates(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv:list_nv){
//            this.Update(nv);
//        }
//    }
}
