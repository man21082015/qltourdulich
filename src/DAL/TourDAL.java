/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import Config.MyConnectUnit;
import Model.TourModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author PHUC
 */
public class TourDAL {

    MyConnectUnit connect;

    public TourDAL() {
        this.connect = Config.DAL.getDAL();
    }

    public ArrayList<TourModel> getAllTours(String condition, String OrderBy) throws Exception {
        ResultSet rs = this.connect.Select("tourdulich", condition, OrderBy);
        ArrayList<TourModel> list_tour = new ArrayList<>();
        while (rs.next()) {
            list_tour.add(new TourModel(rs.getString("MaTour"),
                    rs.getString("MaLoaiHinh"),
                    rs.getString("TenTour"),
                    rs.getString("DacDiem")));
        }
        return list_tour;
    }

//    public ArrayList<TourModel> getTours(String condition) throws Exception{
//        return getAllTours(condition, null);
//    }
//    public ArrayList<TourModel> getTours() throws Exception{
//        return getTours(null);
//    }
//    public NhanVienDTO getNhanVienByMaNV(String manv) throws Exception{
//        ArrayList<NhanVienDTO> list_nv=this.getNhanViens("MaNV='"+manv+"'");
//        if(list_nv.size()>0){
//            return list_nv.get(0);
//        }
//        return null;
//    }
//    public void Insert(TourDuLichModel tour) throws Exception{
//        HashMap<String,Object> map=new HashMap<>();
//        map.put("MaTour", tour.getMaTour());        
//        map.put("TenTour", tour.getTenTour());
//        map.put("DacDiem", tour.getDacDiem());        
//        this.connect.insert("tourdulich", map);
//    }
//    public void Inserts(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv:list_nv){
//            this.Insert(nv);
//        }
//    }
//    public void Delete(NhanVienDTO nv) throws Exception{
//        this.connect.delete("nhanvien", "MaNV='"+nv.getMaNV()+"'");
//    }
//    public void Deletes(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv : list_nv){
//            this.Delete(nv);
//        }
//    }
    public void update(TourModel tour) throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("MaTour", tour.getMaTour());
        map.put("MaLoaiHinh", tour.getMaLoaiHinh());
        map.put("TenTour", tour.getTenTour());
        map.put("DacDiem", tour.getDacDiem());

        this.connect.update("tourdulich", map, "MaTour='" + tour.getMaTour() + "'");
    }
//    public void Updates(ArrayList<NhanVienDTO> list_nv) throws Exception{
//        for(NhanVienDTO nv:list_nv){
//            this.Update(nv);
//        }
//    }

}
