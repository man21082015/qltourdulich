/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form;

import DAL.DiaDiemDAL;
import static Form.formQUANLY_fixVer.workPanel;
import Model.GiaTourModel;
import Model.LoaiHinhModel;
import Model.ThamQuanModel;
import Model.TourModel;
import Model.DiaDiemModel;
import Other.ChuyenPanel;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author W7x64Pro
 */
public class panelQLChiTietTour extends javax.swing.JPanel {

    ArrayList<GiaTourModel> list_gia;
    ArrayList<ThamQuanModel> list_thamQuan;
    ArrayList<LoaiHinhModel> list_loaiHinh;
    ArrayList<DiaDiemModel> list_diaDiem;

    private TourModel tourHienTai = new TourModel();
    private GiaTourModel giaTour = new GiaTourModel();
    private LoaiHinhModel loaiHinh = new LoaiHinhModel();
    private DiaDiemModel diaDiem = new DiaDiemModel();
    private ThamQuanModel thamQuan = new ThamQuanModel();
    
    private int indexGiaSelected = -1;

    private DefaultTableModel giaTableModel;
    private final DefaultTableModel thamQuanTableModel;

    /**
     * Creates new form QLChiTietTour
     */
    public panelQLChiTietTour(TourModel tour) {
        initComponents();
        tableLichTrinh.getColumnModel().getColumn(0).setMinWidth(75);
        tableLichTrinh.getColumnModel().getColumn(0).setMaxWidth(100);
        tableLichTrinh.getColumnModel().getColumn(0).setPreferredWidth(85);
        tableLichTrinh.getColumnModel().getColumn(0).sizeWidthToFit();

        try {
            this.tourHienTai = tour;
            list_gia = giaTour.getGiaByMaTour(tour.getMaTour());
            list_loaiHinh = loaiHinh.getAllLoaiHinh();
            list_thamQuan = thamQuan.getThamQuanByMaTour(tour.getMaTour());
            createComboBoxDiaDiem();

            // Them lua chon cho loai hinh
            createComboBoxLoaiHinh(list_loaiHinh, tourHienTai.getLoaiHinh().getMaLoaiHinh());

        } catch (Exception ex) {
            Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
        }
        dataMaTour.setText(tour.getMaTour());
        dataTenTour.setText(tour.getTenTour());
//        dataLoaiHinh.setText(loaiHinh.getTenLoaiHinh());  // old jtext LoaiHinh

        giaTableModel = (DefaultTableModel) tableGia.getModel();

        for (GiaTourModel gia : list_gia) {
            String ngayBD = formatNgayVietNam(gia.getThoiGianBD().toString());
            String ngayKT = formatNgayVietNam(gia.getThoiGianKT().toString());

            DecimalFormat currency = new DecimalFormat("#,###,###,### VND");
            String giaTien = currency.format(gia.getGiaTien());

            giaTableModel.addRow(new Object[]{ngayBD, ngayKT, giaTien});

        }

        thamQuanTableModel = (DefaultTableModel) tableLichTrinh.getModel();

        for (ThamQuanModel thamQuan : list_thamQuan) {
//            String tenDD = "ERROR";
//            try {
//                tenDD = diaDiem.getTenDiaDiemByMa(thamQuan.getMaDiaDiem());
//            } catch (Exception ex) {
//                Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
//            }

            thamQuanTableModel.addRow(new Object[]{thamQuan.getThutu(), thamQuan.getTenDiaDiem()});

        }

        jTextAreaDacDiem.setText(tour.getDacDiem());
        btnSuaGia.setEnabled(false);

//        System.out.println(list_gia.size());
    }

    private void refreshTableGia() {
        try {
            list_gia = giaTour.getGiaByMaTour(tourHienTai.getMaTour());
            giaTableModel = (DefaultTableModel) tableGia.getModel();

            for (GiaTourModel gia : list_gia) {
                String ngayBD = formatNgayVietNam(gia.getThoiGianBD().toString());
                String ngayKT = formatNgayVietNam(gia.getThoiGianKT().toString());

                DecimalFormat currency = new DecimalFormat("#,###,###,### VND");
                String giaTien = currency.format(gia.getGiaTien());

                giaTableModel.addRow(new Object[]{ngayBD, ngayKT, giaTien});

            }
        } catch (Exception ex) {
            Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void addEventChinhSuaDacDiem() {
        JTextArea areaDacDiem = this.jTextAreaDacDiem;
        areaDacDiem.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                buttonSaveDacDiem.setEnabled(true);
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                buttonSaveDacDiem.setEnabled(true);
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                buttonSaveDacDiem.setEnabled(true);
            }
        });
    }

    private void createComboBoxDiaDiem() {
        try {
            comboLichTrinh.removeAllItems();
            DiaDiemDAL diaDiemDAL = new DiaDiemDAL();
            list_diaDiem = diaDiemDAL.getDiaDiem(null, null);

            for (DiaDiemModel model : list_diaDiem) {
                comboLichTrinh.addItem(model.getTenDiaDiem());
            }
//            buttonSaveLichTrinh.setEnabled(false);
            comboLichTrinh.setSelectedIndex(0);
        } catch (Exception ex) {
            Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void createComboBoxLoaiHinh(ArrayList<LoaiHinhModel> listLoaiHinh, String maLH) {
        comboLoaiHinh.removeAllItems();
        for (LoaiHinhModel loai : listLoaiHinh) {
            comboLoaiHinh.addItem(loai.getTenLoaiHinh());
            if (loai.getMaLoaiHinh().equalsIgnoreCase(maLH)) {
                comboLoaiHinh.setSelectedItem(loai.getTenLoaiHinh());
            }
        }
        buttonSaveLH.setEnabled(false);

    }

    private String formatNgayVietNam(String date) {
        String[] mang = new String[3];
        String kq = "";

        StringTokenizer namThangNgay = new StringTokenizer(date, "-");
        int i = 0;
        while (namThangNgay.hasMoreTokens()) {
            String temp = namThangNgay.nextToken();
            mang[i++] = temp;
        }

        for (int j = mang.length - 1; j != -1; j--) {
            kq += mang[j] + "-";

        }

        kq = kq.substring(0, kq.lastIndexOf("-"));

//        try {
//            startDate = sdf.parse(date);
//            String kq = sdf.format(startDate);
//            return kq;
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        return kq;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelMaTour = new javax.swing.JLabel();
        labelTenTour = new javax.swing.JLabel();
        labelTenLoaiHinh = new javax.swing.JLabel();
        labelDiaDiem = new javax.swing.JLabel();
        labelGiaTour = new javax.swing.JLabel();
        labelDacDiemTour = new javax.swing.JLabel();
        dataTenTour = new javax.swing.JTextField();
        buttonSuaTenTour = new javax.swing.JButton();
        buttonSaveTenTour = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextAreaDacDiem = new javax.swing.JTextArea();
        dataMaTour = new javax.swing.JLabel();
        comboLoaiHinh = new javax.swing.JComboBox<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        tableLichTrinh = new rojerusan.RSTableMetro();
        jScrollPane5 = new javax.swing.JScrollPane();
        tableGia = new rojerusan.RSTableMetro();
        buttonSaveLH = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        labelLichTrinh = new javax.swing.JLabel();
        labelLichTrinh.setVisible(false);
        buttonDeleteLichTrinh = new javax.swing.JButton();
        buttonDeleteLichTrinh.setVisible(false);
        textFieldThuTuLichTrinh = new javax.swing.JTextField();
        textFieldThuTuLichTrinh.setVisible(false);
        comboLichTrinh = new javax.swing.JComboBox<>();
        comboLichTrinh.setVisible(false);
        buttonSaveLichTrinh = new javax.swing.JButton();
        buttonSaveLichTrinh.setVisible(false);
        buttonCancelLichTrinh = new javax.swing.JButton();
        buttonCancelLichTrinh.setVisible(false);
        buttonThemLichTrinh = new javax.swing.JButton();
        buttonChangeDacDiem = new javax.swing.JButton();
        buttonSaveDacDiem = new javax.swing.JButton();
        btnThemGia = new javax.swing.JButton();
        btnSuaGia = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        labelMaTour.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelMaTour.setForeground(new java.awt.Color(0, 112, 192));
        labelMaTour.setText("Mã Tour:");

        labelTenTour.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelTenTour.setForeground(new java.awt.Color(0, 112, 192));
        labelTenTour.setText("Tên Tour:");

        labelTenLoaiHinh.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelTenLoaiHinh.setForeground(new java.awt.Color(0, 112, 192));
        labelTenLoaiHinh.setText("Loại Hình:");

        labelDiaDiem.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelDiaDiem.setForeground(new java.awt.Color(0, 112, 192));
        labelDiaDiem.setText("Lịch trình (Địa điểm - Thứ tự):");

        labelGiaTour.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelGiaTour.setForeground(new java.awt.Color(0, 112, 192));
        labelGiaTour.setText("Giá Tour (Theo thời gian):");

        labelDacDiemTour.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        labelDacDiemTour.setForeground(new java.awt.Color(0, 112, 192));
        labelDacDiemTour.setText("Đặc Điểm Tour:");

        dataTenTour.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        dataTenTour.setEnabled(false);

        buttonSuaTenTour.setBackground(new java.awt.Color(204, 204, 204));
        buttonSuaTenTour.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonSuaTenTour.setText("Sửa");
        buttonSuaTenTour.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonSuaTenTourMousePressed(evt);
            }
        });
        buttonSuaTenTour.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSuaTenTourActionPerformed(evt);
            }
        });

        buttonSaveTenTour.setBackground(new java.awt.Color(51, 255, 51));
        buttonSaveTenTour.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonSaveTenTour.setForeground(new java.awt.Color(255, 255, 255));
        buttonSaveTenTour.setText("Lưu");
        buttonSaveTenTour.setEnabled(false);
        buttonSaveTenTour.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonSaveTenTourMousePressed(evt);
            }
        });

        jTextAreaDacDiem.setEditable(false);
        jTextAreaDacDiem.setBackground(new java.awt.Color(255, 255, 204));
        jTextAreaDacDiem.setColumns(20);
        jTextAreaDacDiem.setFont(new java.awt.Font("Monospaced", 3, 24)); // NOI18N
        jTextAreaDacDiem.setLineWrap(true);
        jTextAreaDacDiem.setRows(5);
        jTextAreaDacDiem.setWrapStyleWord(true);
        jTextAreaDacDiem.setEnabled(false);
        jScrollPane3.setViewportView(jTextAreaDacDiem);

        dataMaTour.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        dataMaTour.setText("T01");

        comboLoaiHinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        comboLoaiHinh.setForeground(new java.awt.Color(0, 112, 192));
        comboLoaiHinh.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboLoaiHinh.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        comboLoaiHinh.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboLoaiHinhItemStateChanged(evt);
            }
        });

        tableLichTrinh.setBackground(new java.awt.Color(255, 255, 204));
        tableLichTrinh.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Thứ tự", "Địa điểm"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableLichTrinh.setColorFilasBackgound2(new java.awt.Color(255, 255, 255));
        tableLichTrinh.setFillsViewportHeight(true);
        tableLichTrinh.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        tableLichTrinh.setFuenteHead(new java.awt.Font("Calibri", 1, 15)); // NOI18N
        tableLichTrinh.setGridColor(new java.awt.Color(0, 112, 192));
        tableLichTrinh.setMultipleSeleccion(false);
        tableLichTrinh.setRowHeight(32);
        tableLichTrinh.setSelectionBackground(new java.awt.Color(0, 112, 192));
        tableLichTrinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableLichTrinhMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tableLichTrinh);
        if (tableLichTrinh.getColumnModel().getColumnCount() > 0) {
            tableLichTrinh.getColumnModel().getColumn(0).setResizable(false);
            tableLichTrinh.getColumnModel().getColumn(0).setPreferredWidth(4);
            tableLichTrinh.getColumnModel().getColumn(1).setResizable(false);
        }

        tableGia.setBackground(new java.awt.Color(255, 255, 204));
        tableGia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Thời gian bắt đầu", "Thời gian kết thúc", "Giá"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableGia.setColorFilasBackgound2(new java.awt.Color(255, 255, 255));
        tableGia.setFillsViewportHeight(true);
        tableGia.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        tableGia.setFuenteHead(new java.awt.Font("Calibri", 1, 15)); // NOI18N
        tableGia.setGridColor(new java.awt.Color(0, 112, 192));
        tableGia.setMultipleSeleccion(false);
        tableGia.setRowHeight(32);
        tableGia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tableGiaMousePressed(evt);
            }
        });
        jScrollPane5.setViewportView(tableGia);
        if (tableGia.getColumnModel().getColumnCount() > 0) {
            tableGia.getColumnModel().getColumn(0).setResizable(false);
            tableGia.getColumnModel().getColumn(1).setResizable(false);
            tableGia.getColumnModel().getColumn(2).setResizable(false);
        }

        buttonSaveLH.setBackground(new java.awt.Color(0, 204, 51));
        buttonSaveLH.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonSaveLH.setText("Lưu thay đổi loại hình");
        buttonSaveLH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonSaveLHMousePressed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        labelLichTrinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        labelLichTrinh.setText("Nhập thứ tự :");

        buttonDeleteLichTrinh.setBackground(new java.awt.Color(204, 51, 0));
        buttonDeleteLichTrinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonDeleteLichTrinh.setForeground(new java.awt.Color(255, 255, 255));
        buttonDeleteLichTrinh.setText("Xóa");
        buttonDeleteLichTrinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonDeleteLichTrinhMousePressed(evt);
            }
        });

        textFieldThuTuLichTrinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        comboLichTrinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        comboLichTrinh.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboLichTrinh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboLichTrinhActionPerformed(evt);
            }
        });

        buttonSaveLichTrinh.setBackground(new java.awt.Color(0, 204, 0));
        buttonSaveLichTrinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonSaveLichTrinh.setForeground(new java.awt.Color(255, 255, 255));
        buttonSaveLichTrinh.setText("Lưu");
        buttonSaveLichTrinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonSaveLichTrinhMousePressed(evt);
            }
        });

        buttonCancelLichTrinh.setBackground(new java.awt.Color(204, 0, 0));
        buttonCancelLichTrinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonCancelLichTrinh.setForeground(new java.awt.Color(255, 255, 255));
        buttonCancelLichTrinh.setText("Hủy");
        buttonCancelLichTrinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buttonCancelLichTrinhMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboLichTrinh, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(labelLichTrinh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textFieldThuTuLichTrinh, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(buttonDeleteLichTrinh)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(buttonSaveLichTrinh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonCancelLichTrinh)))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelLichTrinh)
                    .addComponent(textFieldThuTuLichTrinh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboLichTrinh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonSaveLichTrinh)
                    .addComponent(buttonCancelLichTrinh))
                .addGap(29, 29, 29)
                .addComponent(buttonDeleteLichTrinh)
                .addContainerGap())
        );

        buttonThemLichTrinh.setBackground(new java.awt.Color(0, 153, 0));
        buttonThemLichTrinh.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonThemLichTrinh.setForeground(new java.awt.Color(255, 255, 255));
        buttonThemLichTrinh.setText("Thêm");
        buttonThemLichTrinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonThemLichTrinhMousePressed(evt);
            }
        });

        buttonChangeDacDiem.setBackground(new java.awt.Color(0, 112, 192));
        buttonChangeDacDiem.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonChangeDacDiem.setText("Sửa đặc điểm");
        buttonChangeDacDiem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonChangeDacDiemMousePressed(evt);
            }
        });

        buttonSaveDacDiem.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        buttonSaveDacDiem.setText("Lưu thay đổi");
        buttonSaveDacDiem.setEnabled(false);
        buttonSaveDacDiem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                buttonSaveDacDiemMousePressed(evt);
            }
        });

        btnThemGia.setBackground(new java.awt.Color(0, 153, 102));
        btnThemGia.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        btnThemGia.setForeground(new java.awt.Color(255, 255, 255));
        btnThemGia.setText("Thêm");
        btnThemGia.setPreferredSize(new java.awt.Dimension(97, 50));
        btnThemGia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnThemGiaMousePressed(evt);
            }
        });

        btnSuaGia.setBackground(new java.awt.Color(255, 153, 0));
        btnSuaGia.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        btnSuaGia.setForeground(new java.awt.Color(255, 255, 255));
        btnSuaGia.setText("Sửa");
        btnSuaGia.setPreferredSize(new java.awt.Dimension(97, 50));
        btnSuaGia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSuaGiaMousePressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(labelTenTour)
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(buttonSuaTenTour, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(buttonSaveTenTour, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addComponent(dataTenTour, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(labelDiaDiem)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(buttonThemLichTrinh)
                                                .addGap(37, 37, 37))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelGiaTour)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(buttonSaveLH, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                            .addComponent(labelTenLoaiHinh)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(comboLoaiHinh, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 436, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btnThemGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnSuaGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(44, 44, 44))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(labelDacDiemTour)
                                .addGap(29, 29, 29)
                                .addComponent(buttonChangeDacDiem)
                                .addGap(27, 27, 27)
                                .addComponent(buttonSaveDacDiem))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 1272, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(labelMaTour)
                        .addGap(18, 18, 18)
                        .addComponent(dataMaTour)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(labelMaTour))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(dataMaTour)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelTenLoaiHinh)
                            .addComponent(comboLoaiHinh, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonSaveLH, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelTenTour)
                            .addComponent(dataTenTour, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(buttonSuaTenTour)
                            .addComponent(buttonSaveTenTour))))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buttonThemLichTrinh, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelGiaTour)
                        .addComponent(labelDiaDiem)))
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 10, Short.MAX_VALUE))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addGap(38, 38, 38))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnThemGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSuaGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDacDiemTour)
                    .addComponent(buttonChangeDacDiem)
                    .addComponent(buttonSaveDacDiem))
                .addGap(17, 17, 17)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void comboLoaiHinhItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboLoaiHinhItemStateChanged
        // TODO add your handling code here:
        buttonSaveLH.setEnabled(true);
    }//GEN-LAST:event_comboLoaiHinhItemStateChanged

    private void tableLichTrinhMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableLichTrinhMouseClicked
        // TODO add your handling code here:
        buttonDeleteLichTrinh.setVisible(true);
    }//GEN-LAST:event_tableLichTrinhMouseClicked

    private void buttonCancelLichTrinhMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonCancelLichTrinhMouseClicked
        // TODO add your handling code here:
        labelLichTrinh.setVisible(false);
        textFieldThuTuLichTrinh.setVisible(false);
        textFieldThuTuLichTrinh.setText("");
        comboLichTrinh.setVisible(false);
        buttonSaveLichTrinh.setVisible(false);
        buttonCancelLichTrinh.setVisible(false);
        buttonThemLichTrinh.setEnabled(true);
    }//GEN-LAST:event_buttonCancelLichTrinhMouseClicked

    private void buttonSaveLHMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonSaveLHMousePressed
        if (!buttonSaveLH.isEnabled()) {
            return;
        }
        try {
            // TODO add your handling code here:
            String tenLoaiHinhMoi = (String) comboLoaiHinh.getSelectedItem();
            if (this.tourHienTai.getLoaiHinh().getTenLoaiHinh().equalsIgnoreCase(tenLoaiHinhMoi)) {
                buttonSaveLH.setEnabled(false);
                return;
            }

            // Tìm mã LH theo tên loại hình mới 
            for (LoaiHinhModel loaiHinh : list_loaiHinh) {
                if (loaiHinh.getTenLoaiHinh().equalsIgnoreCase(tenLoaiHinhMoi)) {
                    this.tourHienTai.setMaLoaiHinh(loaiHinh.getMaLoaiHinh());
                    break;
                }
            }

            // Gọi DAL lưu vào DB
            this.tourHienTai.updateTour(tourHienTai);
            JOptionPane.showMessageDialog(null, "Lưu thay đổi loại hình thành công!", "Sửa thông tin loại hình", JOptionPane.PLAIN_MESSAGE);
            buttonSaveLH.setEnabled(false);
        } catch (Exception ex) {
            Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_buttonSaveLHMousePressed

    private void buttonChangeDacDiemMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonChangeDacDiemMousePressed
        // Mở nút lưu và chỗ sửa đặc điểm
        jTextAreaDacDiem.setEnabled(true);
        jTextAreaDacDiem.setEditable(true);
        buttonChangeDacDiem.setEnabled(false);
        buttonChangeDacDiem.setBackground(new java.awt.Color(240, 240, 240));
        buttonSaveDacDiem.setEnabled(true);
        buttonSaveDacDiem.setBackground(new java.awt.Color(255, 153, 0));
    }//GEN-LAST:event_buttonChangeDacDiemMousePressed

    private void buttonSaveDacDiemMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonSaveDacDiemMousePressed
        if (!jTextAreaDacDiem.isEditable()) {
            return;
        }
        try {
            // Gọi DAL lưu vào DB
            String newDacDiem = jTextAreaDacDiem.getText();
            tourHienTai.setDacDiem(newDacDiem);
            tourHienTai.updateTour(tourHienTai);
            JOptionPane.showMessageDialog(null, "Lưu đặc điểm mới thành công!", "Sửa thông tin đặc điểm", JOptionPane.PLAIN_MESSAGE);

            // Đóng nút lưu và chỗ sửa đặc điểm
            jTextAreaDacDiem.setEditable(false);
            jTextAreaDacDiem.setEnabled(false);
            buttonChangeDacDiem.setEnabled(true);
            buttonChangeDacDiem.setBackground(new java.awt.Color(255, 153, 0));
            buttonSaveDacDiem.setEnabled(false);
            buttonSaveDacDiem.setBackground(new java.awt.Color(240, 240, 240));
        } catch (Exception ex) {
            Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_buttonSaveDacDiemMousePressed

    private void buttonSuaTenTourMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonSuaTenTourMousePressed
        // TODO add your handling code here:
        buttonSuaTenTour.setEnabled(false);
        dataTenTour.setEnabled(true);
        dataTenTour.setEditable(true);
        buttonSaveTenTour.setEnabled(true);
    }//GEN-LAST:event_buttonSuaTenTourMousePressed

    private void buttonSaveTenTourMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonSaveTenTourMousePressed
        // TODO add your handling code here:
        if (buttonSuaTenTour.isEnabled()) {
            return;
        }

        // Kiểm tên Tour
        String tenTourMoi = this.dataTenTour.getText();
        if (tenTourMoi.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Vui lòng nhập tên tour!", "Sửa thông tin tên tour", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                tourHienTai.setTenTour(tenTourMoi);
                tourHienTai.updateTour(tourHienTai);

                buttonSuaTenTour.setEnabled(true);
                dataTenTour.setEnabled(false);
                dataTenTour.setEditable(false);
                buttonSaveTenTour.setEnabled(false);
                JOptionPane.showMessageDialog(null, "Lưu tên tour mới thành công!", "Sửa thông tin tên tour", JOptionPane.PLAIN_MESSAGE);
            } catch (Exception ex) {
                Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_buttonSaveTenTourMousePressed

    private void buttonSuaTenTourActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSuaTenTourActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonSuaTenTourActionPerformed

    private void buttonThemLichTrinhMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonThemLichTrinhMousePressed
        // TODO add your handling code here:
        labelLichTrinh.setVisible(true);
        textFieldThuTuLichTrinh.setVisible(true);
        textFieldThuTuLichTrinh.setEditable(false);
        int sl = list_thamQuan.size() + 1;
        textFieldThuTuLichTrinh.setText(String.valueOf(sl));
        comboLichTrinh.setVisible(true);
        buttonSaveLichTrinh.setVisible(true);
        buttonCancelLichTrinh.setVisible(true);
        buttonThemLichTrinh.setEnabled(false);
    }//GEN-LAST:event_buttonThemLichTrinhMousePressed

    private void buttonSaveLichTrinhMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonSaveLichTrinhMousePressed
        // TODO add your handling code here:
//        if (buttonThemLichTrinh.isEnabled()) {
//            return;
//        }
        if (textFieldThuTuLichTrinh.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Vui lòng nhập số thứ tự!", "Nhập số thứ tự", JOptionPane.ERROR_MESSAGE);
            return;
        }
        try {
            int thuTuCanThem = Integer.parseInt(textFieldThuTuLichTrinh.getText());
            if (thuTuCanThem < 1) {
                JOptionPane.showMessageDialog(null, "Vui lòng nhập số thứ tự hợp lệ (từ 1 trở lên)!", "Nhập số thứ tự", JOptionPane.ERROR_MESSAGE);
                textFieldThuTuLichTrinh.setText("");
                return;
            }

            boolean hopLe = true;
            for (ThamQuanModel thamQuanModel : list_thamQuan) {
                if (thamQuanModel.getThutu() == thuTuCanThem) {
                    hopLe = false;
                    break;
                }
            }

            if (hopLe) {
                String tenDiaDiemCanThem = (String) comboLichTrinh.getSelectedItem();

                try {
                    // Tìm mã địa điểm cần thêm trong list DiaDiem
                    String maDiaDiemCanThem = "";
                    for (DiaDiemModel model : list_diaDiem) {
                        if (model.getTenDiaDiem().equalsIgnoreCase(tenDiaDiemCanThem)) {
                            maDiaDiemCanThem = model.getMaDiaDiem();
                            break;
                        }
                    }
                    ThamQuanModel newTQ = new ThamQuanModel(tourHienTai.getMaTour(), maDiaDiemCanThem, tenDiaDiemCanThem, thuTuCanThem);

                    // Lưu vào DB
                    newTQ.insertThamQuan(newTQ);
                    list_thamQuan.add(newTQ);
                    Collections.sort(list_thamQuan, ThamQuanModel.sttComparator);
                    thamQuanTableModel.addRow(new Object[]{newTQ.getThutu(), newTQ.getTenDiaDiem()});

                    JOptionPane.showMessageDialog(null, "Lưu địa điểm mới vào lịch trình thành công!", "Thêm địa điểm mới vào Tour hiện tại", JOptionPane.PLAIN_MESSAGE);

                    // chuyển sang trạng thái ẩn
                    buttonThemLichTrinh.setEnabled(true);
                    labelLichTrinh.setVisible(false);
                    textFieldThuTuLichTrinh.setVisible(false);
                    textFieldThuTuLichTrinh.setText("");
                    comboLichTrinh.setVisible(false);
                    buttonSaveLichTrinh.setVisible(false);
                    buttonCancelLichTrinh.setVisible(false);
                } catch (Exception ex) {
                    Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Số thứ tự vừa nhập đã tồn tại!", "Nhập số thứ tự", JOptionPane.ERROR_MESSAGE);
            }

        } catch (NumberFormatException ex) {
            System.out.println("Phải nhập số nguyên");
        }
    }//GEN-LAST:event_buttonSaveLichTrinhMousePressed

    private void buttonDeleteLichTrinhMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_buttonDeleteLichTrinhMousePressed
        // TODO add your handling code here:
        int reply = JOptionPane.showConfirmDialog(null, "Bạn có chắc muốn xóa địa điểm này không?", "Xóa địa điểm", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            int index = tableLichTrinh.getSelectedRow();

            if (index != -1) {
                try {
                    ThamQuanModel thamQuanSelected = list_thamQuan.get(index);
                    thamQuanSelected.deleteThamQuan(thamQuanSelected);
                    JOptionPane.showMessageDialog(null, "Xóa địa điểm được chọn thành công!", "Xóa địa điểm", JOptionPane.PLAIN_MESSAGE);
                    thamQuanTableModel.removeRow(index);
                } catch (Exception ex) {
                    Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
                }
                list_thamQuan.remove(index);
                Collections.sort(list_thamQuan, ThamQuanModel.sttComparator);
                // Nếu xóa số ở giữa thì đặt lại STT
                int soLuong = list_thamQuan.size();
                int stt = index + 1;
                for (int i = index; i < soLuong; i++) {
                    list_thamQuan.get(i).setThutu(stt++);
                }

                // Update STT cho DB
                ArrayList<ThamQuanModel> list_temp = new ArrayList<>(list_thamQuan);
                list_temp.forEach(tmd -> {
                    try {
//                    System.out.println(tmd.getMaTour() + "/ " + tmd.getMaDiaDiem() + "/ " + tmd.getThutu());
                        tmd.updateThamQuan(tmd);
                    } catch (Exception ex) {
                        Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });

                list_thamQuan.clear();
                list_thamQuan = new ArrayList<>(list_temp);

                // Reset table
                tableLichTrinh.clearSelection();
                thamQuanTableModel.setRowCount(0);
                tableLichTrinh.removeAll();
                tableLichTrinh.revalidate();
                for (ThamQuanModel thamQuanModel : list_thamQuan) {
                    thamQuanTableModel.addRow(new Object[]{thamQuanModel.getThutu(), thamQuanModel.getTenDiaDiem()});
                }
            }
        }
    }//GEN-LAST:event_buttonDeleteLichTrinhMousePressed

    private void comboLichTrinhActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboLichTrinhActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboLichTrinhActionPerformed

    private void btnThemGiaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnThemGiaMousePressed
        frameThemSua_Gia jFrame = new frameThemSua_Gia(tourHienTai, frameThemSua_Gia.ThemSuaMode.ADD, null);
        jFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent windowEvent) {
                giaTableModel.setRowCount(0);
                refreshTableGia();
            }
        });

        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btnThemGiaMousePressed


    private void btnSuaGiaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSuaGiaMousePressed
        GiaTourModel giaTourModel = list_gia.get(indexGiaSelected);
        frameThemSua_Gia jFrame = new frameThemSua_Gia(tourHienTai, frameThemSua_Gia.ThemSuaMode.UPDATE, giaTourModel);
        jFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosed(java.awt.event.WindowEvent windowEvent) {
                giaTableModel.setRowCount(0);
                refreshTableGia();
            }
        });

        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btnSuaGiaMousePressed

    private void tableGiaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableGiaMousePressed
        
        indexGiaSelected = tableGia.getSelectedRow();
        if (indexGiaSelected != -1) {
            btnSuaGia.setEnabled(true);            
        }
    }//GEN-LAST:event_tableGiaMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSuaGia;
    private javax.swing.JButton btnThemGia;
    private javax.swing.JButton buttonCancelLichTrinh;
    private javax.swing.JButton buttonChangeDacDiem;
    private javax.swing.JButton buttonDeleteLichTrinh;
    private javax.swing.JButton buttonSaveDacDiem;
    private javax.swing.JButton buttonSaveLH;
    private javax.swing.JButton buttonSaveLichTrinh;
    private javax.swing.JButton buttonSaveTenTour;
    private javax.swing.JButton buttonSuaTenTour;
    private javax.swing.JButton buttonThemLichTrinh;
    private javax.swing.JComboBox<String> comboLichTrinh;
    private javax.swing.JComboBox<String> comboLoaiHinh;
    private javax.swing.JLabel dataMaTour;
    private javax.swing.JTextField dataTenTour;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextArea jTextAreaDacDiem;
    private javax.swing.JLabel labelDacDiemTour;
    private javax.swing.JLabel labelDiaDiem;
    private javax.swing.JLabel labelGiaTour;
    private javax.swing.JLabel labelLichTrinh;
    private javax.swing.JLabel labelMaTour;
    private javax.swing.JLabel labelTenLoaiHinh;
    private javax.swing.JLabel labelTenTour;
    private rojerusan.RSTableMetro tableGia;
    private rojerusan.RSTableMetro tableLichTrinh;
    private javax.swing.JTextField textFieldThuTuLichTrinh;
    // End of variables declaration//GEN-END:variables
}
