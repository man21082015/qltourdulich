package Form;

import DAL.ChiPhiDAL;
import static Form.formQUANLY_fixVer.workPanel;
import Model.ChiPhiModel;
import Model.DoanDuLichModel;
import Model.LoaiChiPhiModel;
import Other.ChuyenPanel;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author man21
 */
public class panelQLDoan_ChiPhi extends javax.swing.JPanel {

    private DoanDuLichModel doan = new DoanDuLichModel();
    private ChiPhiModel chiPhi = new ChiPhiModel();
    private final LoaiChiPhiModel loadChiPhi = new LoaiChiPhiModel();
    ArrayList<ChiPhiModel> listCP = new ArrayList<>();
    ArrayList<LoaiChiPhiModel> listLoaiCP = new ArrayList<>();
    private DefaultTableModel chiPhiTableModel;
    private int selectedIndex = -1;
    boolean firstClick = false;

    public panelQLDoan_ChiPhi(DoanDuLichModel doan) {
        initComponents();
        this.doan = doan;
        initComboBoxLoaiCP();
        showChiPhi();
        System.out.println("Done");
    }

    private void showChiPhi() {
        try {
            listCP = chiPhi.getChiPhiByMaDoan(doan.getMaDoan());
            chiPhiTableModel = (DefaultTableModel) tableChiPhi.getModel();
            chiPhiTableModel.setRowCount(0);
            if (listCP != null) {
                for (ChiPhiModel cp : listCP) {
                    chiPhiTableModel.addRow(new Object[]{cp.getLoaiChiPhi().getTenLoaiChiPhi(), cp.getSoTien()});
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(panelQLDoan_KhachHang.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initComboBoxLoaiCP() {
        try {
            comboLoaiCP.removeAllItems();
            listLoaiCP = loadChiPhi.getAllLoaiCP();
            for (LoaiChiPhiModel loaiCP : listLoaiCP) {
                comboLoaiCP.addItem(loaiCP.getTenLoaiChiPhi());
            }
            comboLoaiCP.setSelectedIndex(0);
        } catch (Exception ex) {
            Logger.getLogger(panelQLChiTietTour.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean isDuplicateLoaiCP(String tenLoaiCP) {
        ChiPhiModel object = listCP.stream()
                .filter(element -> tenLoaiCP.equals(element.getLoaiChiPhi().getTenLoaiChiPhi()))
                .findAny()
                .orElse(null);
        if (object != null) {
            return true;
        } else {
            return false;
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        estiloTablaHeader1 = new rojerusan.necesario.EstiloTablaHeader();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableChiPhi = new rojerusan.RSTableMetro();
        jLabel1 = new javax.swing.JLabel();
        createButton = new javax.swing.JButton();
        comboLoaiCP = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        soTienJLabel = new javax.swing.JTextField();
        updateButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(1304, 840));

        tableChiPhi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Loại Chi Phí", "Số Tiền"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableChiPhi.setRowHeight(32);
        tableChiPhi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tableChiPhiMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tableChiPhi);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("DANH SÁCH CHI PHÍ CỦA ĐOÀN DU LỊCH");

        createButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        createButton.setText("Thêm");
        createButton.setPreferredSize(new java.awt.Dimension(150, 50));
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });

        comboLoaiCP.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        comboLoaiCP.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboLoaiCP.setEnabled(false);
        comboLoaiCP.setPreferredSize(new java.awt.Dimension(250, 32));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel3.setText("Số tiền:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel4.setText("Tên loại chi phí:");

        soTienJLabel.setEnabled(false);
        soTienJLabel.setPreferredSize(new java.awt.Dimension(250, 32));

        updateButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        updateButton.setText("Sửa");
        updateButton.setEnabled(false);
        updateButton.setPreferredSize(new java.awt.Dimension(150, 50));
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        deleteButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        deleteButton.setText("Xóa");
        deleteButton.setEnabled(false);
        deleteButton.setPreferredSize(new java.awt.Dimension(150, 50));
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        backButton.setBackground(new java.awt.Color(255, 255, 255));
        backButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/button_back.png"))); // NOI18N
        backButton.setMaximumSize(new java.awt.Dimension(50, 50));
        backButton.setMinimumSize(new java.awt.Dimension(50, 50));
        backButton.setPreferredSize(new java.awt.Dimension(50, 50));
        backButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(257, 257, 257))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(createButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(updateButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(soTienJLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboLoaiCP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 744, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(backButton, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(comboLoaiCP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(soTienJLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 613, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(createButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(updateButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 752, Short.MAX_VALUE))
                .addGap(18, 18, 18))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed

        if (firstClick == false) {
            comboLoaiCP.setEnabled(true);
            soTienJLabel.setEnabled(true);
            firstClick = true;
        } else {
            boolean isDuplicated = false;
            String tenLoaiCP = (String) comboLoaiCP.getSelectedItem();
            String soTien = soTienJLabel.getText();

            // Lấy mã loại chi phí từ combo box
            String maLoaiCP = listLoaiCP.stream()
                    .filter(element -> tenLoaiCP.equals(element.getTenLoaiChiPhi()))
                    .findAny()
                    .orElse(null).getMaLoaiChiPhi();

            // Hàm kiểm tra phần tử trùng
            isDuplicated = isDuplicateLoaiCP(tenLoaiCP);

            if (soTien.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Bạn cần nhập chi phí cho " + tenLoaiCP, "Thông báo", JOptionPane.PLAIN_MESSAGE);
            } else if (isDuplicated) {
                JOptionPane.showMessageDialog(null, tenLoaiCP + " đã tồn tại", "Thông báo", JOptionPane.PLAIN_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, tenLoaiCP + " thêm thành công", "Thông báo", JOptionPane.PLAIN_MESSAGE);
                
                ChiPhiDAL chiPhiDAL = new ChiPhiDAL();
                int total = 0;
                try {
                    ArrayList<ChiPhiModel> allCP = chiPhiDAL.getAllCP(null, null);
                    total = allCP.size()+1;
                } catch (Exception ex) {
                    Logger.getLogger(panelQLDoan_ChiPhi.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                String maChiPhi = total < 10 ? "CP0" + total : "CP" + total;
                ChiPhiModel item = new ChiPhiModel(maChiPhi, doan.getMaDoan(), maLoaiCP, Integer.parseInt(soTien));
                item.setLoaiHinh(new LoaiChiPhiModel(maLoaiCP, tenLoaiCP));
                listCP.add(item);
                chiPhiTableModel.addRow(new Object[]{item.getLoaiChiPhi().getTenLoaiChiPhi(), item.getSoTien()});
                chiPhi.insert(item);
                System.out.println("mã loại CP: " + maLoaiCP + " số Tiền: " + soTien);

                firstClick = false;
                comboLoaiCP.setEnabled(false);
                soTienJLabel.setEnabled(false);
            }

        }


    }//GEN-LAST:event_createButtonActionPerformed

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed

        boolean isDuplicated = false;
        String tenLoaiCP = (String) comboLoaiCP.getSelectedItem();
        String soTien = soTienJLabel.getText();

        // Lấy mã loại chi phí từ combo box
        String maLoaiCP = listLoaiCP.stream()
                .filter(element -> tenLoaiCP.equals(element.getTenLoaiChiPhi()))
                .findAny()
                .orElse(null).getMaLoaiChiPhi();

        // Hàm kiểm tra phần tử trùng
        isDuplicated = isDuplicateLoaiCP(tenLoaiCP);

        if (soTien.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Bạn cần nhập chi phí cho " + tenLoaiCP, "Thông báo", JOptionPane.PLAIN_MESSAGE);
        } else if (isDuplicated) {
            JOptionPane.showMessageDialog(null, tenLoaiCP + " đã tồn tại", "Thông báo", JOptionPane.PLAIN_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "Cập nhật thành công", "Thông báo", JOptionPane.PLAIN_MESSAGE);
            ChiPhiModel item = new ChiPhiModel(chiPhi.getMaChiPhi(), doan.getMaDoan(), maLoaiCP, Integer.parseInt(soTien));
            item.setLoaiHinh(new LoaiChiPhiModel(maLoaiCP, tenLoaiCP));

            listCP.get(selectedIndex).setMaLoaiChiPhi(item.getMaLoaiChiPhi());
            listCP.get(selectedIndex).setSoTien(item.getSoTien());
            listCP.get(selectedIndex).setLoaiHinh(item.getLoaiChiPhi());

            chiPhiTableModel.removeRow(selectedIndex);
            chiPhiTableModel.insertRow(selectedIndex, new Object[]{item.getLoaiChiPhi().getTenLoaiChiPhi(), item.getSoTien()});

            chiPhi.update(item);

        }

    }//GEN-LAST:event_updateButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        String[] options = new String[]{"Có", "Không"};
        int choice = JOptionPane.showOptionDialog(null, "Bạn có muốn xóa loại chi phí này không?", "Xác nhận xóa",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[0]);
        if (choice == 0) {
            String maCP = listCP.get(selectedIndex).getMaChiPhi();
            chiPhi.delete(maCP);
            JOptionPane.showMessageDialog(null, "Xóa thành công", "Thông báo", JOptionPane.PLAIN_MESSAGE);
            chiPhiTableModel.removeRow(selectedIndex);            
            listCP.remove(selectedIndex);                                    
        } else {
            return;
        }

    }//GEN-LAST:event_deleteButtonActionPerformed

    private void backButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtonActionPerformed
        // TODO add your handling code here:
       new ChuyenPanel(workPanel, new panelQL_Doan());
        
    }//GEN-LAST:event_backButtonActionPerformed

    private void tableChiPhiMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableChiPhiMousePressed
        selectedIndex = tableChiPhi.getSelectedRow();
        comboLoaiCP.setEnabled(true);
        soTienJLabel.setEnabled(true);
        deleteButton.setEnabled(true);
        updateButton.setEnabled(true);
        if (!tableChiPhi.isEnabled()) {
            return;
        }
        if (selectedIndex != -1) {
            chiPhi = listCP.get(selectedIndex);
            soTienJLabel.setText(String.valueOf(chiPhi.getSoTien()));
            comboLoaiCP.setSelectedItem(chiPhi.getLoaiChiPhi().getTenLoaiChiPhi());
        }

    }//GEN-LAST:event_tableChiPhiMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JComboBox<String> comboLoaiCP;
    private javax.swing.JButton createButton;
    private javax.swing.JButton deleteButton;
    private rojerusan.necesario.EstiloTablaHeader estiloTablaHeader1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField soTienJLabel;
    private rojerusan.RSTableMetro tableChiPhi;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables
}
