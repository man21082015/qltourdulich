/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form;

import static Form.formQUANLY_fixVer.workPanel;
import Model.NhanVienModel;
import Model.PhanBoNVDoanModel;
import Other.ChuyenPanel;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author PHUC
 */
public class panelQLDoan_NhanVien extends javax.swing.JPanel {
    private NhanVienModel nhanvien = new NhanVienModel();
    private PhanBoNVDoanModel phancongNhanvien = new PhanBoNVDoanModel();
    private ArrayList<NhanVienModel> nhanvienList = new ArrayList<>();
    private ArrayList<PhanBoNVDoanModel> nhanvienPCList = new ArrayList<>(); 
    ArrayList<NhanVienModel> listNhanVienTemp = new ArrayList<>(); 
    private final DefaultTableModel nhanvienTableModel;
    private final DefaultTableModel nhanvienPCTableModel;
    private NhanVienModel nhanvienPhanCong;
    private String Doan;
    
    
    /**
     * Creates new form panelQLDoan_NhanVien
     */
    public panelQLDoan_NhanVien(String doan, String tour) throws Exception {
        initComponents();
        Doan = doan;
        labelTitle.setText("DANH SÁCH NHÂN VIÊN TOUR "+tour+" - ĐOÀN "+doan);
        nhanvienList = nhanvien.getAllNhanVien();
        nhanvienTableModel = (DefaultTableModel) tableAllNhanVien.getModel();
        for (NhanVienModel nv : nhanvienList) {
                System.out.println("Nhân viên " + nv.getTenNhanVien());
                nhanvienTableModel.addRow(new Object[]{nv.getMaNhanVien(), nv.getTenNhanVien(), nv.getGioiTinh(), nv.getSDT(), nv.getDiaChi()});
            }
        nhanvienPCList = phancongNhanvien.getNhanVienByMaDoan(Doan);
        nhanvienPCTableModel = (DefaultTableModel) tablePCNhanVien.getModel();
        for (PhanBoNVDoanModel nv : nhanvienPCList) {
                nhanvienPCTableModel.addRow(new Object[]{nv.getMaNhanVien(),nv.getTenNhanVien(),nv.getNhiemVu()});
            }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelTitle = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableAllNhanVien = new rojerusan.RSTableMetro();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablePCNhanVien = new rojerusan.RSTableMetro();
        jLabel2 = new javax.swing.JLabel();
        textSearch = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        lableMaTenNV = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        cbNhiemVu = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(255, 255, 255));

        labelTitle.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        labelTitle.setText("DANH SÁCH NHÂN VIÊN TOUR T01 - ĐOÀN MH17A78");

        tableAllNhanVien.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã NV", "Tên NV", "Giới Tính", "SDT", "Địa Chỉ"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableAllNhanVien.setColorFilasBackgound2(new java.awt.Color(255, 255, 255));
        tableAllNhanVien.setRowHeight(40);
        tableAllNhanVien.setSelectionBackground(new java.awt.Color(0, 112, 192));
        tableAllNhanVien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableAllNhanVienMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableAllNhanVien);
        if (tableAllNhanVien.getColumnModel().getColumnCount() > 0) {
            tableAllNhanVien.getColumnModel().getColumn(0).setMaxWidth(70);
            tableAllNhanVien.getColumnModel().getColumn(2).setMinWidth(100);
            tableAllNhanVien.getColumnModel().getColumn(2).setMaxWidth(120);
            tableAllNhanVien.getColumnModel().getColumn(3).setMinWidth(120);
            tableAllNhanVien.getColumnModel().getColumn(3).setMaxWidth(120);
        }

        tablePCNhanVien.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mã NV", "Tên NV", "Nhiệm vụ"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablePCNhanVien.setColorFilasBackgound2(new java.awt.Color(255, 255, 255));
        tablePCNhanVien.setRowHeight(40);
        tablePCNhanVien.setSelectionBackground(new java.awt.Color(0, 112, 192));
        jScrollPane2.setViewportView(tablePCNhanVien);
        if (tablePCNhanVien.getColumnModel().getColumnCount() > 0) {
            tablePCNhanVien.getColumnModel().getColumn(0).setMaxWidth(70);
            tablePCNhanVien.getColumnModel().getColumn(2).setMinWidth(100);
            tablePCNhanVien.getColumnModel().getColumn(2).setMaxWidth(180);
        }

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("ĐƯỢC PHÂN CÔNG");

        textSearch.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N

        jButton1.setBackground(new java.awt.Color(51, 102, 0));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton1.setText("Tìm kiếm");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton1MousePressed(evt);
            }
        });
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lableMaTenNV.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lableMaTenNV.setMaximumSize(new java.awt.Dimension(250, 50));
        lableMaTenNV.setMinimumSize(new java.awt.Dimension(250, 50));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Nhiệm vụ:");

        jButton2.setBackground(new java.awt.Color(0, 51, 153));
        jButton2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setText("Phân công nhân viên");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setText("DANH SÁCH NHÂN VIÊN");

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/button_back.png"))); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        cbNhiemVu.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cbNhiemVu.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Trưởng đoàn", "Phó đoàn" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(textSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 487, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 719, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
                                    .addComponent(lableMaTenNV, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(cbNhiemVu, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(13, 13, 13)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(69, 69, 69))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(labelTitle)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton4)
                    .addComponent(labelTitle))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 631, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(textSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton1))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 631, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(128, 128, 128)
                        .addComponent(lableMaTenNV, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbNhiemVu, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableAllNhanVienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableAllNhanVienMouseClicked
        // TODO add your handling code here:
        int i = tableAllNhanVien.getSelectedRow();
        nhanvienPhanCong = nhanvienList.get(i);
        lableMaTenNV.setText(nhanvienPhanCong.getMaNhanVien()+ " - "+nhanvienPhanCong.getTenNhanVien());
    }//GEN-LAST:event_tableAllNhanVienMouseClicked

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
       if(nhanvienPhanCong == null){
           JOptionPane.showMessageDialog(null, "Bạn chưa chọn nhân viên", "THÔNG BÁO", JOptionPane.PLAIN_MESSAGE);
       }
       else{
               PhanBoNVDoanModel nv = new PhanBoNVDoanModel(nhanvienPhanCong.getMaNhanVien(), Doan, cbNhiemVu.getSelectedItem().toString());
               if(checkNhanVien(nv.getMaNhanVien()) == true){
                    nhanvienPCTableModel.addRow(new Object[]{nv.getMaNhanVien(), nhanvienPhanCong.getTenNhanVien(), nv.getNhiemVu()});
                    nhanvienPCList.add(nv);
                   try {
                       nv.phancong();
                   } catch (Exception ex) {
                       Logger.getLogger(panelQLDoan_NhanVien.class.getName()).log(Level.SEVERE, null, ex);
                   }
                    System.out.println(nhanvienPCList.size());
               }
               else{
                   JOptionPane.showMessageDialog(null, "Nhân viên đã tồn tại", "THÔNG BÁO", JOptionPane.PLAIN_MESSAGE);
               }
       }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MousePressed
        // TODO add your handling code here:
        String textToSearch = textSearch.getText();
        if (textToSearch.length() > 0) {
            listNhanVienTemp.clear();
            nhanvienList.forEach((nv) -> {
                boolean check = false;
                String ma = nv.getMaNhanVien();
                String name = nv.getTenNhanVien();
                String gender = nv.getGioiTinh();
                String phone = nv.getSDT();
                String address = nv.getDiaChi();
                if (searchFilter(ma, textToSearch)) {
                    check = true;
                }
                if (searchFilter(name, textToSearch)) {
                    check = true;
                }
                if (searchFilter(gender, textToSearch)) {
                    check = true;
                }
                if (searchFilter(phone, textToSearch)) {
                    check = true;
                }
                if (searchFilter(address, textToSearch)) {
                    check = true;
                }
                if (check) {
                    listNhanVienTemp.add(nv);
                }
            });
            nhanvienTableModel.setRowCount(0);
             for (NhanVienModel nv : listNhanVienTemp) {
                System.out.println("Nhân viên " + nv.getTenNhanVien());
                nhanvienTableModel.addRow(new Object[]{nv.getMaNhanVien(), nv.getTenNhanVien(), nv.getGioiTinh(), nv.getSDT(), nv.getDiaChi()});
            }
        }
        else{
            nhanvienTableModel.setRowCount(0);
            for (NhanVienModel nv : nhanvienList) {
                System.out.println("Nhân viên " + nv.getTenNhanVien());
                nhanvienTableModel.addRow(new Object[]{nv.getMaNhanVien(), nv.getTenNhanVien(), nv.getGioiTinh(), nv.getSDT(), nv.getDiaChi()});
            }
        }
    }//GEN-LAST:event_jButton1MousePressed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        new ChuyenPanel(workPanel, new panelQL_Doan());
    }//GEN-LAST:event_jButton4ActionPerformed
    private boolean checkNhanVien(String manv){
        for(PhanBoNVDoanModel nv : nhanvienPCList){
            if(nv.getMaNhanVien().equals(manv) == true){
                return false;     
            }
        }
        return true;
    }
    private boolean searchFilter(String str, String str2) {
        if (str.toLowerCase().contains(str2.toLowerCase())) {
            return true;
        }
        return false;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbNhiemVu;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelTitle;
    private javax.swing.JLabel lableMaTenNV;
    private rojerusan.RSTableMetro tableAllNhanVien;
    private rojerusan.RSTableMetro tablePCNhanVien;
    private javax.swing.JTextField textSearch;
    // End of variables declaration//GEN-END:variables
}
