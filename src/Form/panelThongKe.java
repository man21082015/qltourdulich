/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Form;

import static Chart.BarChart.createChart;
import DAL.ChiTietDoanDAL;
import static Form.formQUANLY_fixVer.workPanel;
import Model.ChiPhiModel;
import Model.ChiTietDoanModel;
import Model.DoanDuLichModel;
import Model.DoanhThuDoanModel;
import Model.GiaTourModel;
import Model.ThongKeDoanhThuModel;
import Other.ChuyenPanel;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartPanel;

/**
 *
 * @author PHUC
 */
public class panelThongKe extends javax.swing.JPanel {
    DoanDuLichModel doandulich = new DoanDuLichModel();
    private ArrayList<DoanDuLichModel> doandulichList = new ArrayList<>();
    private final ArrayList<ThongKeDoanhThuModel> doanhThuList = new ArrayList<>();
    DecimalFormat currency = new DecimalFormat ("#,###,###,### VND");
    private DefaultTableModel doanhThuTableModel;
    private DefaultTableModel doanTableModel;
    ChiPhiModel chiPhiModel = new ChiPhiModel();
    ChiTietDoanModel chiTietDoanModel = new ChiTietDoanModel();
    
    ArrayList<DoanhThuDoanModel> listDoanhThuDoan;
    
    /**
     * Creates new form panelThongKe
     */
    public panelThongKe() throws Exception {
        initComponents();
        doandulichList = doandulich.getAllDoanDuLich();
        String thang = doandulichList.get(0).getNgayKhoiHanh().substring(0,7);
        ThongKeDoanhThuModel doanhthu = new ThongKeDoanhThuModel(thang);        
        double tongChiPhi = getChiPhi(doandulichList.get(0).getMaDoan());
        int sove = chiTietDoanModel.getChiTietDoanByMaDoan(doandulichList.get(0).getMaDoan()).size();
        int tongSove = chiTietDoanModel.getChiTietDoanByMaDoan(doandulichList.get(0).getMaDoan()).size();
        double tongthu = getGiaVe(doandulichList.get(0).getMaTour(), doandulichList.get(0).getNgayKhoiHanh())*sove;
        for(int i = 1; i < doandulichList.size(); i++){
            if(doandulichList.get(i).getNgayKhoiHanh().substring(0,7).equals(thang)){                
                tongChiPhi += getChiPhi(doandulichList.get(i).getMaDoan());
                sove = chiTietDoanModel.getChiTietDoanByMaDoan(doandulichList.get(i).getMaDoan()).size();
                tongSove += chiTietDoanModel.getChiTietDoanByMaDoan(doandulichList.get(i).getMaDoan()).size();
                tongthu += getGiaVe(doandulichList.get(i).getMaTour(), doandulichList.get(i).getNgayKhoiHanh())*sove;
                doanhthu.setDoanhThu(tongthu);
                doanhthu.setChiPhi(tongChiPhi);
                doanhthu.setLoiNhuan(tongthu-tongChiPhi);
                doanhthu.setTongSoVe(tongSove);
            }
            else{
                doanhThuList.add(doanhthu);
                tongthu = 0;
                tongChiPhi = 0;
                sove= 0;
                tongSove = 0;
                thang = doandulichList.get(i).getNgayKhoiHanh().substring(0,7);
                doanhthu = new ThongKeDoanhThuModel(thang);                
                tongChiPhi += getChiPhi(doandulichList.get(i).getMaDoan());
                sove = chiTietDoanModel.getChiTietDoanByMaDoan(doandulichList.get(i).getMaDoan()).size();
                tongSove += chiTietDoanModel.getChiTietDoanByMaDoan(doandulichList.get(i).getMaDoan()).size();
                tongthu += getGiaVe(doandulichList.get(i).getMaTour(), doandulichList.get(i).getNgayKhoiHanh())*sove;
                doanhthu.setDoanhThu(tongthu);
                doanhthu.setChiPhi(tongChiPhi);
                doanhthu.setLoiNhuan(tongthu-tongChiPhi);
                doanhthu.setTongSoVe(tongSove);
            }
        }
        doanhThuTableModel = (DefaultTableModel) tableDoanhThu.getModel();
        for(int i=0;i<doanhThuList.size();i++){
            doanhThuTableModel.addRow(new Object[]{doanhThuList.get(i).getThang(),currency.format(doanhThuList.get(i).getDoanhThu()),currency.format(doanhThuList.get(i).getChiPhi()),currency.format(doanhThuList.get(i).getLoiNhuan()),doanhThuList.get(i).getTongSoVe()});
        }
        doanTableModel = (DefaultTableModel) tableDoan.getModel();
        
        listDoanhThuDoan = new ArrayList<>();
        for(int i = 0; i < doandulichList.size(); i++){
            System.out.println(getGiaVe(doandulichList.get(i).getMaTour(), doandulichList.get(i).getNgayKhoiHanh()) + "-"+i);
            DoanDuLichModel model = doandulichList.get(i);
            listDoanhThuDoan.add(new DoanhThuDoanModel(model, getChiPhi(model.getMaDoan()), 
                    chiTietDoanModel.getChiTietDoanByMaDoan(model.getMaDoan()).size(), getGiaVe(model.getMaTour(), model.getNgayKhoiHanh())));          
        }

    }
    public int getChiPhi(String madoan) throws Exception{
        int tongChiPhi = 0;
        ArrayList<ChiPhiModel> listChiPhi = chiPhiModel.getChiPhiByMaDoan(madoan);
        if (listChiPhi == null) listChiPhi = new ArrayList<>();
        for(int i = 0; i<listChiPhi.size();i++){
            tongChiPhi += listChiPhi.get(i).getSoTien();
        }
        return tongChiPhi;
    }
    public int getGiaVe(String matour, String ngayxuatphat) throws Exception{   
        ArrayList<GiaTourModel> listGia = new GiaTourModel().getAllGia();
        for(GiaTourModel gt : listGia){
            if(checkDayBetween(ngayxuatphat, gt.getThoiGianBD(), gt.getThoiGianKT()) && matour.equals(gt.getMaTour())){
                return gt.getGiaTien();
            }
        }
        return 0;
    }
    public boolean checkDayBetween(String ngayKhoiHanh, LocalDate localDateTgBatDau, LocalDate localDateTgKetThuc){
        LocalDate localDateNgayKhoiHanh = LocalDate.parse(ngayKhoiHanh);
        if(localDateNgayKhoiHanh.isAfter(localDateTgBatDau) && localDateNgayKhoiHanh.isBefore(localDateTgKetThuc)){
            return true;
        }
        return false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableDoanhThu = new rojerusan.RSTableMetro();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableDoan = new rojerusan.RSTableMetro();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("Thống kê");

        tableDoanhThu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tháng", "Doanh Thu", "Chi Phí", "Lợi nhuận", "Số vé bán được"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableDoanhThu.setColorFilasBackgound2(new java.awt.Color(255, 255, 255));
        tableDoanhThu.setRowHeight(24);
        tableDoanhThu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableDoanhThuMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableDoanhThu);
        if (tableDoanhThu.getColumnModel().getColumnCount() > 0) {
            tableDoanhThu.getColumnModel().getColumn(0).setMinWidth(120);
            tableDoanhThu.getColumnModel().getColumn(0).setMaxWidth(120);
            tableDoanhThu.getColumnModel().getColumn(1).setResizable(false);
            tableDoanhThu.getColumnModel().getColumn(2).setResizable(false);
            tableDoanhThu.getColumnModel().getColumn(3).setResizable(false);
            tableDoanhThu.getColumnModel().getColumn(4).setResizable(false);
        }

        tableDoan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã Đoàn", "Mã Tour", "Ngày khởi hành", "Ngày kết thúc", "Số vé bán được", "Giá vé", "Doanh thu", "Chi phí ", "Lợi nhuận"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableDoan.setColorFilasBackgound2(new java.awt.Color(255, 255, 255));
        tableDoan.setRowHeight(24);
        jScrollPane2.setViewportView(tableDoan);
        if (tableDoan.getColumnModel().getColumnCount() > 0) {
            tableDoan.getColumnModel().getColumn(0).setMinWidth(120);
            tableDoan.getColumnModel().getColumn(0).setMaxWidth(120);
            tableDoan.getColumnModel().getColumn(1).setMinWidth(120);
            tableDoan.getColumnModel().getColumn(1).setMaxWidth(120);
        }

        jButton1.setBackground(new java.awt.Color(51, 153, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Xem biểu đồ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1292, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 680, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(64, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void tableDoanhThuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableDoanhThuMouseClicked
        // TODO add your handling code here:
        int selectedRow = tableDoanhThu.getSelectedRow();
        while (doanTableModel.getRowCount() > 0) {
               doanTableModel.removeRow(0);
        }
        for(int i = 0; i < listDoanhThuDoan.size(); i++){
            if(doanhThuList.get(selectedRow).getThang().equals(listDoanhThuDoan.get(i).getDoanDuLichModel().getNgayKhoiHanh().substring(0, 7))){
                DoanhThuDoanModel doanhThuDoan = listDoanhThuDoan.get(i);
                doanTableModel.addRow(new Object[]{doanhThuDoan.getDoanDuLichModel().getMaDoan(),
                    doanhThuDoan.getDoanDuLichModel().getMaTour(),
                    doanhThuDoan.getDoanDuLichModel().getNgayKhoiHanh(),
                    doanhThuDoan.getDoanDuLichModel().getNgayKetThuc(),
                    doanhThuDoan.getTongSoVe(),
                    currency.format(doanhThuDoan.getGiaVe()),
                    currency.format(doanhThuDoan.getGiaVe()*doanhThuDoan.getTongSoVe()),
                    currency.format(doanhThuDoan.getChiPhi()),
                    currency.format(doanhThuDoan.getGiaVe()*doanhThuDoan.getTongSoVe()-doanhThuDoan.getChiPhi())});
            }
        }
    }//GEN-LAST:event_tableDoanhThuMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        ChartPanel chartPanel = new ChartPanel(createChart(doanhThuList));
        chartPanel.setPreferredSize(new java.awt.Dimension(1560, 1367));
        JFrame frame = new JFrame();
        frame.add(chartPanel);
        frame.setTitle("Biểu đồ lợi nhuận");
        frame.setSize(600, 400);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private rojerusan.RSTableMetro tableDoan;
    private rojerusan.RSTableMetro tableDoanhThu;
    // End of variables declaration//GEN-END:variables
}
