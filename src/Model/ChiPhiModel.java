package Model;

import DAL.ChiPhiDAL;
import DAL.LoaiChiPhiDAL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letoan
 */
public class ChiPhiModel {

    private String MaChiPhi;
    private String MaDoan;
    private String MaLoaiChiPhi;
    private int SoTien;
    private LoaiChiPhiModel loaiChiPhi;

    public ChiPhiModel() {
    }

    public ChiPhiModel(String MaChiPhi, String MaDoan, String MaLoaiChiPhi, int SoTien) {
        this.MaChiPhi = MaChiPhi;
        this.MaDoan = MaDoan;
        this.MaLoaiChiPhi = MaLoaiChiPhi;
        this.SoTien = SoTien;
    }

    public String getMaChiPhi() {
        return MaChiPhi;
    }

    public void setMaChiPhi(String MaChiPhi) {
        this.MaChiPhi = MaChiPhi;
    }

    public String getMaDoan() {
        return MaDoan;
    }

    public void setMaDoan(String MaDoan) {
        this.MaDoan = MaDoan;
    }

    public String getMaLoaiChiPhi() {
        return MaLoaiChiPhi;
    }

    public void setMaLoaiChiPhi(String MaLoaiChiPhi) {
        this.MaLoaiChiPhi = MaLoaiChiPhi;
    }

    public int getSoTien() {
        return SoTien;
    }

    public void setSoTien(int SoTien) {
        this.SoTien = SoTien;
    }

    public LoaiChiPhiModel getLoaiChiPhi() {
        return loaiChiPhi;
    }

    public void setLoaiHinh(LoaiChiPhiModel loaiChiPhi) {
        this.loaiChiPhi = loaiChiPhi;
    }

    public ArrayList<ChiPhiModel> getChiPhiByMaDoan(String maDoan) throws Exception {
        ChiPhiDAL chiPhiDAL = new ChiPhiDAL();
        LoaiChiPhiDAL loaiCPDAL = new LoaiChiPhiDAL();
        ArrayList<ChiPhiModel> resultList = new ArrayList<>();
        for (ChiPhiModel object : chiPhiDAL.allChiPhi) {
            if (object.MaDoan.equals(maDoan)) {                
                object.loaiChiPhi = loaiCPDAL.getLoaiCPByMaCP(object.MaLoaiChiPhi);
                resultList.add(object);
            }
        }
        return resultList;
    }
    public void insert(ChiPhiModel chiPhi){
        ChiPhiDAL chiPhiDAL = new ChiPhiDAL();
        try {
            chiPhiDAL.insert(chiPhi);
        } catch (Exception ex) {
            Logger.getLogger(ChiPhiModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void update(ChiPhiModel chiPhi){
        ChiPhiDAL chiPhiDAL = new ChiPhiDAL();
        try {
            chiPhiDAL.update(chiPhi);
        } catch (Exception ex) {
            Logger.getLogger(ChiPhiModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void delete(String maCP){
        ChiPhiDAL chiPhiDAL = new ChiPhiDAL();
        try {
            chiPhiDAL.delete(maCP);
        } catch (Exception ex) {
            Logger.getLogger(ChiPhiModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
