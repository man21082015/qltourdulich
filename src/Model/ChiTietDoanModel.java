package Model;

import DAL.ChiTietDoanDAL;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class ChiTietDoanModel {

    private String MaDoan;
    private String MaKhachHang;

    public ChiTietDoanModel() {
    }

    public ChiTietDoanModel(String MaDoan, String MaKhachHang) {
        this.MaDoan = MaDoan;
        this.MaKhachHang = MaKhachHang;
    }

    public String getMaDoan() {
        return MaDoan;
    }

    public void setMaDoan(String MaDoan) {
        this.MaDoan = MaDoan;
    }

    public String getMaKhachHang() {
        return MaKhachHang;
    }

    public void setMaKhachHang(String MaKhachHang) {
        this.MaKhachHang = MaKhachHang;
    }

    public ArrayList<ChiTietDoanModel> getChiTietDoanByMaDoan(String maDoan) throws Exception {
        ChiTietDoanDAL chiTietDoanDAL = new ChiTietDoanDAL();
        return chiTietDoanDAL.getChiTietDoanByMaDoan(maDoan);
    }

    public boolean isExist(ChiTietDoanModel ctd) throws Exception {
        ArrayList<ChiTietDoanModel> ctdList = getChiTietDoanByMaDoan(ctd.getMaDoan());

        // hàm tìm kiếm 1 object dùng stream        
        ChiTietDoanModel object = ctdList.stream()
                .filter(element -> ctd.getMaKhachHang().equals(element.getMaKhachHang()))
                .findAny()
                .orElse(null);

        if (object != null) {
            return true;
        } else {

            return false;
        }
    }

    public boolean addKhachHangIntoDoan(String maDoan, String maKH) throws Exception {
        ArrayList<ChiTietDoanModel> ctdList = getChiTietDoanByMaDoan(maDoan);

        // hàm tìm kiếm 1 object dùng stream        
        ChiTietDoanModel object = ctdList.stream()
                .filter(element -> maKH.equals(element.getMaKhachHang()))
                .findAny()
                .orElse(null);

        if (object != null) {
            return false;
        } else {
            ChiTietDoanModel ctd = new ChiTietDoanModel(maDoan, maKH);
            new ChiTietDoanDAL().insertChiTietDoan(ctd);
            return true;
        }

    }

    public void deleteKhachHangFromDoan(String maKH) throws Exception {
        new ChiTietDoanDAL().deleteChiTietDoan(maKH);
    }
    
    public void deleteKhachHangFromDoan(String maKH, String maDoan) throws Exception {
        new ChiTietDoanDAL().deleteChiTietDoan(maKH, maDoan);
    }

}
