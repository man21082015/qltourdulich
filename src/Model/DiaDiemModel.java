package Model;

import DAL.DiaDiemDAL;

/**
 *
 * @author letoan
 */
public class DiaDiemModel {
    private String MaDiaDiem;
    private String TenDiaDiem;
    
    public DiaDiemModel(){
    }

    public DiaDiemModel(String MaDiaDiem, String TenDiaDiem) {
        this.MaDiaDiem = MaDiaDiem;
        this.TenDiaDiem = TenDiaDiem;
    }

    public String getMaDiaDiem() {
        return MaDiaDiem;
    }

    public void setMaDiaDiem(String MaDiaDiem) {
        this.MaDiaDiem = MaDiaDiem;
    }

    public String getTenDiaDiem() {
        return TenDiaDiem;
    }

    public void setTenDiaDiem(String TenDiaDiem) {
        this.TenDiaDiem = TenDiaDiem;
    }
    
    public String getTenDiaDiemByMa(String maDD) throws Exception{
        DiaDiemDAL diaDiemDAL = new DiaDiemDAL();
        return diaDiemDAL.getTenDiaDiemByMa(maDD);
    }
    
    public String getMaDiaDiemtheoTen(String tenDD) throws Exception{
        DiaDiemDAL diaDiemDAL = new DiaDiemDAL();
        return diaDiemDAL.getMaDiaDiemByTen(tenDD);
    }
}
