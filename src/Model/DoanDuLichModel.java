package Model;

import DAL.DoanDuLichDAL;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class DoanDuLichModel {
    private String MaDoan;
    private String MaTour;
    private String NgayKhoiHanh;
    private String NgayKetThuc;
    private double DoanhThu;
    private String HanhTrinh;
    private String KhachSan;
    
    public DoanDuLichModel(){
    }

    public DoanDuLichModel(String MaDoan, String MaTour, String NgayKhoiHanh, String NgayKetThuc, double DoanhThu, String HanhTrinh, String KhachSan) {
        this.MaDoan = MaDoan;
        this.MaTour = MaTour;
        this.NgayKhoiHanh = NgayKhoiHanh;
        this.NgayKetThuc = NgayKetThuc;
        this.DoanhThu = DoanhThu;
        this.HanhTrinh = HanhTrinh;
        this.KhachSan = KhachSan;
    }

    public String getMaDoan() {
        return MaDoan;
    }

    public void setMaDoan(String MaDoan) {
        this.MaDoan = MaDoan;
    }

    public String getMaTour() {
        return MaTour;
    }

    public void setMaTour(String MaTour) {
        this.MaTour = MaTour;
    }

    public String getNgayKhoiHanh() {
        return NgayKhoiHanh;
    }

    public void setNgayKhoiHanh(String NgayKhoiHanh) {
        this.NgayKhoiHanh = NgayKhoiHanh;
    }

    public String getNgayKetThuc() {
        return NgayKetThuc;
    }

    public void setNgayKetThuc(String NgayKetThuc) {
        this.NgayKetThuc = NgayKetThuc;
    }

    public double getDoanhThu() {
        return DoanhThu;
    }

    public void setDoanhThu(double DoanhThu) {
        this.DoanhThu = DoanhThu;
    }

    public String getHanhTrinh() {
        return HanhTrinh;
    }

    public void setHanhTrinh(String HanhTrinh) {
        this.HanhTrinh = HanhTrinh;
    }

    public String getKhachSan() {
        return KhachSan;
    }

    public void setKhachSan(String KhachSan) {
        this.KhachSan = KhachSan;
    }
    
    public ArrayList<DoanDuLichModel> getAllDoanDuLich() throws Exception {
        DoanDuLichDAL doandulichDAL = new DoanDuLichDAL();
        ArrayList<DoanDuLichModel> listDoanDuLich = doandulichDAL.getAllDoanDuLich(null, null);
        return listDoanDuLich;
    }
    
     public void updateDoanDuLich(DoanDuLichModel doandulich) throws Exception{
        DoanDuLichDAL doandulichDAL = new DoanDuLichDAL();
        doandulichDAL.update(doandulich);
    }
    
}
