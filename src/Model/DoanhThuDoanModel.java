/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author PHUC
 */
public class DoanhThuDoanModel {
    DoanDuLichModel doanDuLichModel;
    int GiaVe;
    int ChiPhi;
    int TongSoVe;
    public DoanhThuDoanModel() {
    }

    public DoanhThuDoanModel(DoanDuLichModel doanDuLichModel, int ChiPhi, int TongSoVe, int GiaVe) {
        this.doanDuLichModel = doanDuLichModel;
        this.ChiPhi = ChiPhi;
        this.TongSoVe = TongSoVe;
        this.GiaVe = GiaVe;
    }

    public int getGiaVe() {
        return GiaVe;
    }

    public void setGiaVe(int GiaVe) {
        this.GiaVe = GiaVe;
    }

    public int getTongSoVe() {
        return TongSoVe;
    }

    public void setTongSoVe(int TongSoVe) {
        this.TongSoVe = TongSoVe;
    }



    public DoanDuLichModel getDoanDuLichModel() {
        return doanDuLichModel;
    }

    public void setDoanDuLichModel(DoanDuLichModel doanDuLichModel) {
        this.doanDuLichModel = doanDuLichModel;
    }

    public int getChiPhi() {
        return ChiPhi;
    }

    public void setChiPhi(int ChiPhi) {
        this.ChiPhi = ChiPhi;
    }
    
    
}
