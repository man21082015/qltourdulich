package Model;

import DAL.GiaTourDAL;
import DAL.KhachHangDAL;
import DAL.LoaiHinhDAL;
import DAL.TourDAL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letoan
 */
public class GiaTourModel {

    private String MaGiaTour;
    private String MaTour;
    private LocalDate ThoiGianBD;
    private LocalDate ThoiGianKT;
    private int GiaTien;

    public GiaTourModel() {
    }

    public GiaTourModel(String MaGiaTour, String MaTour, LocalDate ThoiGianBD, LocalDate ThoiGianKT, int GiaTien) {
        this.MaGiaTour = MaGiaTour;
        this.MaTour = MaTour;
        this.ThoiGianBD = ThoiGianBD;
        this.ThoiGianKT = ThoiGianKT;
        this.GiaTien = GiaTien;
    }

    public String getMaGiaTour() {
        return MaGiaTour;
    }

    public void setMaGiaTour(String MaGiaTour) {
        this.MaGiaTour = MaGiaTour;
    }

    public String getMaTour() {
        return MaTour;
    }

    public void setMaTour(String MaTour) {
        this.MaTour = MaTour;
    }

    public LocalDate getThoiGianBD() {
        return ThoiGianBD;
    }

    public void setThoiGianBD(LocalDate ThoiGianBD) {
        this.ThoiGianBD = ThoiGianBD;
    }

    public LocalDate getThoiGianKT() {
        return ThoiGianKT;
    }

    public void setThoiGianKT(LocalDate ThoiGianKT) {
        this.ThoiGianKT = ThoiGianKT;
    }

    public int getGiaTien() {
        return GiaTien;
    }

    public void setGiaTien(int GiaTien) {
        this.GiaTien = GiaTien;
    }

    public ArrayList<GiaTourModel> getAllGia() throws Exception {
        GiaTourDAL giaDAL = new GiaTourDAL();
        return giaDAL.getGiaTours(null, null);
    }

    public ArrayList<GiaTourModel> getGiaByMaTour(String maTour) throws Exception {
        GiaTourDAL giaDAL = new GiaTourDAL();
        return giaDAL.getListGiaByMaTour(maTour);
    }

    public boolean addGia(GiaTourModel gia) {
        try {
            new GiaTourDAL().insert(gia);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(GiaTourModel.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean updateGia(GiaTourModel gia) {
        try {
            new GiaTourDAL().update(gia);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(GiaTourModel.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

}
