package Model;

import DAL.KhachHangDAL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letoan
 */
public class KhachHangModel {

    private String MaKhachHang;
    private String HoTen;
    private String SoCMND;
    private String DiaChi;
    private String GioiTinh;
    private String SDT;
    private String QuocTich;

    public KhachHangModel() {
    }

    public KhachHangModel(String MaKhachHang, String HoTen, String SoCMND, String DiaChi, String GioiTinh, String SDT, String QuocTich) {
        this.MaKhachHang = MaKhachHang;
        this.HoTen = HoTen;
        this.SoCMND = SoCMND;
        this.DiaChi = DiaChi;
        this.GioiTinh = GioiTinh;
        this.SDT = SDT;
        this.QuocTich = QuocTich;
    }

    public String getMaKhachHang() {
        return MaKhachHang;
    }

    public void setMaKhachHang(String MaKhachHang) {
        this.MaKhachHang = MaKhachHang;
    }

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public String getSoCMND() {
        return SoCMND;
    }

    public void setSoCMND(String SoCMND) {
        this.SoCMND = SoCMND;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public String getGioiTinh() {
        return GioiTinh;
    }

    public void setGioiTinh(String GioiTinh) {
        this.GioiTinh = GioiTinh;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getQuocTich() {
        return QuocTich;
    }

    public void setQuocTich(String QuocTich) {
        this.QuocTich = QuocTich;
    }

    public ArrayList<KhachHangModel> getAllKhachHang() throws Exception {
        //KhachHangDAL khachHangDAL = new KhachHangDAL();
        return new KhachHangDAL().getAllKhachHang(null, null);
    }

    public KhachHangModel getKhachHang(String maKH) throws Exception {
        KhachHangDAL khachHangDAL = new KhachHangDAL();
        return khachHangDAL.allKhachHang.stream()
                .filter(element -> maKH.equals(element.getMaKhachHang()))
                .findAny()
                .orElse(null);
    }

    public ArrayList<KhachHangModel> findKhachHangByHoTen(ArrayList<KhachHangModel> list, String keyWord) throws Exception {
        ArrayList<KhachHangModel> result = new ArrayList<>();
        for (KhachHangModel kh : list) {
            if (kh.getHoTen().contains(keyWord)) {
                result.add(kh);
            }
        }
        return result;
    }

    public boolean isExist(KhachHangModel khachHang) throws Exception {
        ArrayList<KhachHangModel> khList = getAllKhachHang();
        // hàm tìm kiếm 1 object dùng stream
        KhachHangModel object = khList.stream()
                .filter(element -> khachHang.getMaKhachHang().equals(element.getMaKhachHang()))
                .findAny()
                .orElse(null);

        if (object != null) {
            return true;
        } else {            
            return false;
        }
    }

    public boolean addKhachHang(KhachHangModel khachHang)  {        
        try {
            if (isExist(khachHang)) {
                return false;
            } else {
                new KhachHangDAL().insert(khachHang);
                return true;
            }
        } catch (Exception ex) {
            Logger.getLogger(KhachHangModel.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean updateKhachHang(KhachHangModel khachHang)  {        
        try {
            new KhachHangDAL().update(khachHang);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(KhachHangModel.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean deleteKhachHang(String maKH)  {        
        try {
            new KhachHangDAL().delete(maKH);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(KhachHangModel.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
