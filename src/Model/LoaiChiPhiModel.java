package Model;

import DAL.LoaiChiPhiDAL;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class LoaiChiPhiModel {
    private String MaLoaiChiPhi;
    private String TenLoaiChiPhi;
    
    public LoaiChiPhiModel(){
    }

    public LoaiChiPhiModel(String MaLoaiChiPhi, String TenLoaiChiPhi) {
        this.MaLoaiChiPhi = MaLoaiChiPhi;
        this.TenLoaiChiPhi = TenLoaiChiPhi;
    }

    public String getMaLoaiChiPhi() {
        return MaLoaiChiPhi;
    }

    public void setMaLoaiChiPhi(String MaLoaiChiPhi) {
        this.MaLoaiChiPhi = MaLoaiChiPhi;
    }

    public String getTenLoaiChiPhi() {
        return TenLoaiChiPhi;
    }

    public void setTenLoaiChiPhi(String TenLoaiChiPhi) {
        this.TenLoaiChiPhi = TenLoaiChiPhi;
    }
    
    
    public ArrayList<LoaiChiPhiModel> getAllLoaiCP() {
        LoaiChiPhiDAL loaiCP = new LoaiChiPhiDAL();
        return loaiCP.allLoaiCP;
    }
       
}
