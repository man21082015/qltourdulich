package Model;

import DAL.LoaiHinhDAL;
import DAL.TourDAL;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class LoaiHinhModel {

    private String MaLoaiHinh;
    private String TenLoaiHinh;

    public LoaiHinhModel() {
    }

    public LoaiHinhModel(String MaLoaiHinh, String TenLoaiHinh) {
        this.MaLoaiHinh = MaLoaiHinh;
        this.TenLoaiHinh = TenLoaiHinh;
    }

    public String getMaLoaiHinh() {
        return MaLoaiHinh;
    }

    public void setMaLoaiHinh(String MaLoaiHinh) {
        this.MaLoaiHinh = MaLoaiHinh;
    }

    public String getTenLoaiHinh() {
        return TenLoaiHinh;
    }

    public void setTenLoaiHinh(String TenLoaiHinh) {
        this.TenLoaiHinh = TenLoaiHinh;
    }

    public ArrayList<LoaiHinhModel> getAllLoaiHinh() throws Exception {
        LoaiHinhDAL loaihinhDAL = new LoaiHinhDAL();
        return loaihinhDAL.getAllLoaiHinh(null, null);
    }
    
    

}
