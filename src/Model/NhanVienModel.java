package Model;

import DAL.NhanVienDAL;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class NhanVienModel {

    private String MaNhanVien;
    private String TenNhanVien;
    private LocalDate NgaySinh;
    private String GioiTinh;
    private String SDT;
    private String DiaChi;
    private String Luong;
    private String TrangThai;    

    public NhanVienModel() {
    }

    public NhanVienModel(String MaNhanVien, String TenNhanVien) {
        this.MaNhanVien = MaNhanVien;
        this.TenNhanVien = TenNhanVien;
    }

    public ArrayList<NhanVienModel> getAllNhanVien() throws Exception {
        NhanVienDAL nhanVienDAL = new NhanVienDAL();
        return nhanVienDAL.getAllNhanVien(null, null);
    }
    
    public void insert(NhanVienModel nv) throws Exception{
        NhanVienDAL nhanVienDAL = new NhanVienDAL();   
        nhanVienDAL.Insert(nv);
    }
    
    public void update(NhanVienModel nv) throws Exception{
        NhanVienDAL nhanVienDAL = new NhanVienDAL();   
        nhanVienDAL.Update(nv);
    }

    public NhanVienModel(String MaNhanVien, String TenNhanVien, LocalDate NgaySinh, String GioiTinh, String SDT, String DiaChi, String Luong, String TrangThai) {
        this.MaNhanVien = MaNhanVien;
        this.TenNhanVien = TenNhanVien;
        this.NgaySinh = NgaySinh;
        this.GioiTinh = GioiTinh;
        this.SDT = SDT;
        this.DiaChi = DiaChi;
        this.Luong = Luong;
        this.TrangThai = TrangThai;
    }

    public LocalDate getNgaySinh() {
        return NgaySinh;
    }

    public void setNgaySinh(LocalDate NgaySinh) {
        this.NgaySinh = NgaySinh;
    }

    public String getGioiTinh() {
        return GioiTinh;
    }

    public void setGioiTinh(String GioiTinh) {
        this.GioiTinh = GioiTinh;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public void setDiaChi(String DiaChi) {
        this.DiaChi = DiaChi;
    }

    public String getLuong() {
        return Luong;
    }

    public void setLuong(String Luong) {
        this.Luong = Luong;
    }

    public String getTrangThai() {
        return TrangThai;
    }

    public void setTrangThai(String TrangThai) {
        this.TrangThai = TrangThai;
    }

    public String getMaNhanVien() {
        return MaNhanVien;
    }

    public void setMaNhanVien(String MaNhanVien) {
        this.MaNhanVien = MaNhanVien;
    }

    public String getTenNhanVien() {
        return TenNhanVien;
    }

    public void setTenNhanVien(String TenNhanVien) {
        this.TenNhanVien = TenNhanVien;
    }
    
}
