package Model;

import DAL.NhanVienDAL;
import DAL.PhanBoNVDoanDAL;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class PhanBoNVDoanModel {
    private String MaNhanVien;
    private String TenNhanVien;
    private String MaDoan;
    private String NhiemVu;
    
    public PhanBoNVDoanModel(){
    }

    public PhanBoNVDoanModel(String MaNhanVien, String MaDoan, String NhiemVu) {
        this.MaNhanVien = MaNhanVien;
        this.MaDoan = MaDoan;
        this.NhiemVu = NhiemVu;
    }

    public PhanBoNVDoanModel(String MaNhanVien, String TenNhanVien, String MaDoan, String NhiemVu) {
        this.MaNhanVien = MaNhanVien;
        this.TenNhanVien = TenNhanVien;
        this.MaDoan = MaDoan;
        this.NhiemVu = NhiemVu;
    }

    public String getTenNhanVien() {
        return TenNhanVien;
    }

    public void setTenNhanVien(String TenNhanVien) {
        this.TenNhanVien = TenNhanVien;
    }
    
    public ArrayList<PhanBoNVDoanModel> getNhanVienByMaDoan(String madoan) throws Exception {
        PhanBoNVDoanDAL pbnhanVienDAL = new PhanBoNVDoanDAL();
        NhanVienDAL nhanvienDAL = new NhanVienDAL();
        ArrayList<PhanBoNVDoanModel> list = pbnhanVienDAL.getNhanVienByMaDoan(madoan);
        for(int i= 0; i<list.size();i++){
            NhanVienModel nhanVienModel = nhanvienDAL.getNhanVienByMaNV(list.get(i).getMaNhanVien());
            list.get(i).setTenNhanVien(nhanVienModel.getTenNhanVien());
        }
        return list;
    }
    public void phancong() throws Exception{
        PhanBoNVDoanDAL pbnhanVienDAL = new PhanBoNVDoanDAL();   
        pbnhanVienDAL.phanCongNhanVien(this);
    }

    public String getMaNhanVien() {
        return MaNhanVien;
    }

    public void setMaNhanVien(String MaNhanVien) {
        this.MaNhanVien = MaNhanVien;
    }

    public String getMaDoan() {
        return MaDoan;
    }

    public void setMaDoan(String MaDoan) {
        this.MaDoan = MaDoan;
    }

    public String getNhiemVu() {
        return NhiemVu;
    }

    public void setNhiemVu(String NhiemVu) {
        this.NhiemVu = NhiemVu;
    }
    
            
    
}
