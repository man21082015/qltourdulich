package Model;

import DAL.ThamQuanDAL;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author letoan
 */
public class ThamQuanModel {

    private String MaTour;
    private String MaDiaDiem;
    private String TenDiaDiem;
    private int thutu;

    public ThamQuanModel() {
    }

    public ThamQuanModel(String MaTour, String MaDiaDiem, String TenDiaDiem, int thutu) {
        this.MaTour = MaTour;
        this.MaDiaDiem = MaDiaDiem;
        this.TenDiaDiem = TenDiaDiem;
        this.thutu = thutu;
    }

    public ThamQuanModel(String MaTour, String MaDiaDiem, int thutu) {
        this.MaTour = MaTour;
        this.MaDiaDiem = MaDiaDiem;
        this.thutu = thutu;
    }

    public String getMaTour() {
        return MaTour;
    }

    public void setMaTour(String MaTour) {
        this.MaTour = MaTour;
    }

    public String getMaDiaDiem() {
        return MaDiaDiem;
    }

    public void setMaDiaDiem(String MaDiaDiem) {
        this.MaDiaDiem = MaDiaDiem;
    }

    public String getTenDiaDiem() {
        return TenDiaDiem;
    }

    public void setTenDiaDiem(String TenDiaDiem) {
        this.TenDiaDiem = TenDiaDiem;
    }

    public int getThutu() {
        return thutu;
    }

    public void setThutu(int thutu) {
        this.thutu = thutu;
    }

    public static Comparator<ThamQuanModel> sttComparator = (ThamQuanModel t1, ThamQuanModel t2) -> {
        int stt1
                = t1.getThutu();
        int stt2
                = t2.getThutu();
        
        // ascending order
        return stt1 - stt2;
        
        // descending order
//            return stt2 - stt1;
    };

    public ArrayList<ThamQuanModel> getThamQuanByMaTour(String maTour) throws Exception {
        ThamQuanDAL thamQuanDAL = new ThamQuanDAL();
//        this.TenDiaDiem = new DiaDiemModel().getTenDiaDiemByMa(MaDiaDiem);
        ArrayList<ThamQuanModel> listThamQuan = thamQuanDAL.getThamQuanByMaTour(maTour);
        if(listThamQuan != null){
            for (ThamQuanModel thamQuan : listThamQuan) {
            thamQuan.setTenDiaDiem(new DiaDiemModel().getTenDiaDiemByMa(thamQuan.getMaDiaDiem()));
        }
        return listThamQuan;
        }
        return new ArrayList<>();
        
    }

    public void insertThamQuan(ThamQuanModel thamQuan) throws Exception {
        ThamQuanDAL thamQuanDAL = new ThamQuanDAL();
        thamQuanDAL.Insert(thamQuan);
    }
    
    public void updateThamQuan(ThamQuanModel  thamQuan) throws Exception{
        ThamQuanDAL thamQuanDAL = new ThamQuanDAL();
        thamQuanDAL.Update(thamQuan);
    }

    public void deleteThamQuan(ThamQuanModel thamQuan) throws Exception {
        ThamQuanDAL thamQuanDAL = new ThamQuanDAL();
        thamQuanDAL.Delete(thamQuan);
    }

}
