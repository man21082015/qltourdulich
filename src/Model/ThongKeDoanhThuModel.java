/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author PHUC
 */
public class ThongKeDoanhThuModel {
    String Thang;
    double DoanhThu;
    double ChiPhi;
    double LoiNhuan;
    int TongSoVe;

    public ThongKeDoanhThuModel(String Thang) {
        this.Thang = Thang;
    }

    public ThongKeDoanhThuModel(String Thang, double DoanhThu, double ChiPhi, double LoiNhuan, int TongSoVe) {
        this.Thang = Thang;
        this.DoanhThu = DoanhThu;
        this.ChiPhi = ChiPhi;
        this.LoiNhuan = LoiNhuan;
        this.TongSoVe = TongSoVe;
    }

    public int getTongSoVe() {
        return TongSoVe;
    }

    public void setTongSoVe(int TongSoVe) {
        this.TongSoVe = TongSoVe;
    }

    public String getThang() {
        return Thang;
    }

    public void setThang(String Thang) {
        this.Thang = Thang;
    }

    public double getDoanhThu() {
        return DoanhThu;
    }

    public void setDoanhThu(double DoanhThu) {
        this.DoanhThu = DoanhThu;
    }

    public double getChiPhi() {
        return ChiPhi;
    }

    public void setChiPhi(double ChiPhi) {
        this.ChiPhi = ChiPhi;
    }

    public double getLoiNhuan() {
        return LoiNhuan;
    }

    public void setLoiNhuan(double LoiNhuan) {
        this.LoiNhuan = LoiNhuan;
    }
}
