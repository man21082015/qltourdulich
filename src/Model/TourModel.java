package Model;

import DAL.LoaiHinhDAL;
import DAL.TourDAL;
import java.util.ArrayList;

/**
 *
 * @author letoan
 */
public class TourModel {

    private String MaTour;
    private String MaLoaiHinh;
    private String TenTour;
    private String DacDiem;
    private LoaiHinhModel LoaiHinh;

    public TourModel() {

    }

    public TourModel(String MaTour, String MaLoaiHinh, String TenTour, String DacDiem ) {
        this.MaTour = MaTour;
        this.MaLoaiHinh = MaLoaiHinh;
        this.TenTour = TenTour;
        this.DacDiem = DacDiem;
    }

    public String getMaTour() {
        return MaTour;
    }

    public void setMaTour(String MaTour) {
        this.MaTour = MaTour;
    }

    public String getMaLoaiHinh() {
        return MaLoaiHinh;
    }

    public void setMaLoaiHinh(String MaLoaiHinh) {
        this.MaLoaiHinh = MaLoaiHinh;
    }

    public String getTenTour() {
        return TenTour;
    }

    public void setTenTour(String TenTour) {
        this.TenTour = TenTour;
    }

    public String getDacDiem() {
        return DacDiem;
    }

    public void setDacDiem(String DacDiem) {
        this.DacDiem = DacDiem;
    }

    public LoaiHinhModel getLoaiHinh() {
        return LoaiHinh;
    }
    
    public void setLoaiHinh(LoaiHinhModel LoaiHinh) {
        this.LoaiHinh = LoaiHinh;
    }

    public ArrayList<TourModel> getAllTours() throws Exception {
        TourDAL tourDAL = new TourDAL();
        LoaiHinhDAL loaiHinhDAL = new LoaiHinhDAL();   
        
        ArrayList<TourModel> listTour = tourDAL.getAllTours(null, null);
        
        
        for (TourModel tourModel : listTour) {            
            tourModel.LoaiHinh = loaiHinhDAL.getLoaiHinhByMaLH(tourModel.MaLoaiHinh);
        }

        return listTour;
    }
    
    public void updateTour(TourModel tour) throws Exception{
        TourDAL tourDAL = new TourDAL();
        tourDAL.update(tour);
    }
    
}
