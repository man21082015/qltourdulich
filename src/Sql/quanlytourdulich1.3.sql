-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: quanlytourdulich
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chiphi`
--

DROP TABLE IF EXISTS `chiphi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chiphi` (
  `MaChiPhi` char(10) NOT NULL,
  `MaDoan` char(10) DEFAULT NULL,
  `MaLoaiChiPhi` char(10) DEFAULT NULL,
  `SoTien` int DEFAULT NULL,
  PRIMARY KEY (`MaChiPhi`),
  KEY `fk_CP_MaDoan` (`MaDoan`),
  KEY `fk_CP_MaLoaiChiPhi` (`MaLoaiChiPhi`),
  CONSTRAINT `fk_CP_MaDoan` FOREIGN KEY (`MaDoan`) REFERENCES `doandulich` (`MaDoan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CP_MaLoaiChiPhi` FOREIGN KEY (`MaLoaiChiPhi`) REFERENCES `loaichiphi` (`MaLoaiChiPhi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chiphi`
--

LOCK TABLES `chiphi` WRITE;
/*!40000 ALTER TABLE `chiphi` DISABLE KEYS */;
/*!40000 ALTER TABLE `chiphi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitietdoan`
--

DROP TABLE IF EXISTS `chitietdoan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `chitietdoan` (
  `MaDoan` char(10) NOT NULL,
  `MaKhachHang` char(10) NOT NULL,
  PRIMARY KEY (`MaDoan`,`MaKhachHang`),
  KEY `fk_CTD_MaKhachHang` (`MaKhachHang`),
  CONSTRAINT `fk_CTD_MaDoan` FOREIGN KEY (`MaDoan`) REFERENCES `doandulich` (`MaDoan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CTD_MaKhachHang` FOREIGN KEY (`MaKhachHang`) REFERENCES `khachhang` (`MaKhachHang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietdoan`
--

LOCK TABLES `chitietdoan` WRITE;
/*!40000 ALTER TABLE `chitietdoan` DISABLE KEYS */;
INSERT INTO `chitietdoan` VALUES ('DOAN01','KH01'),('DOAN01','KH02'),('DOAN02','KH03'),('DOAN02','KH04'),('DOAN02','KH05'),('DOAN01','KH06'),('DOAN02','KH07'),('DOAN01','KH08'),('DOAN02','KH09'),('DOAN01','KH10'),('DOAN02','KH11');
/*!40000 ALTER TABLE `chitietdoan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diadiem`
--

DROP TABLE IF EXISTS `diadiem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `diadiem` (
  `MaDiaDiem` char(10) NOT NULL,
  `TenDiaDiem` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`MaDiaDiem`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diadiem`
--

LOCK TABLES `diadiem` WRITE;
/*!40000 ALTER TABLE `diadiem` DISABLE KEYS */;
INSERT INTO `diadiem` VALUES ('DD01','Cầu Sông Hàn'),('DD02','Bảo tàng tranh 3D'),('DD03','Phố cổ Hội An'),('DD04','Vịnh Hạ Long'),('DD05','Chợ đêm Hạ Long'),('DD06','Công viên hoa Hạ Long'),('DD07','Vườn dâu Cái Tàu'),('DD08','Hòn Đá Bạc'),('DD09','Rừng U Minh Hạ'),('DD10','Quảng trường Lâm Viên'),('DD11','Núi Lang Biang'),('DD12','Thung Lũng Tình Yêu'),('DD13','Chùa Thiên Mụ'),('DD14','Sông Hương'),('DD15','Vườn Quốc Gia Bạch Mã');
/*!40000 ALTER TABLE `diadiem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doandulich`
--

DROP TABLE IF EXISTS `doandulich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doandulich` (
  `MaDoan` char(10) NOT NULL,
  `MaTour` char(10) DEFAULT NULL,
  `NgayKhoiHanh` date DEFAULT NULL,
  `NgayKetThuc` date DEFAULT NULL,
  `DoanhThu` bigint DEFAULT NULL,
  `HanhTrinh` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `KhachSan` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`MaDoan`),
  KEY `fk_DDL_MaTour` (`MaTour`),
  CONSTRAINT `fk_DDL_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doandulich`
--

LOCK TABLES `doandulich` WRITE;
/*!40000 ALTER TABLE `doandulich` DISABLE KEYS */;
INSERT INTO `doandulich` VALUES ('DOAN01','T01','2021-10-20','2021-10-27',0,'#','#'),('DOAN02','T01','2021-10-20','2021-10-27',0,'#','#'),('DOAN03','T01','2021-10-20','2021-10-27',0,'#','#'),('DOAN04','T01','2021-10-20','2021-10-27',0,'#','#'),('DOAN05','T01','2021-10-20','2021-10-27',0,'#','#'),('DOAN06','T02','2021-11-01','2021-11-07',0,'#','#'),('DOAN07','T02','2021-11-01','2021-11-07',0,'#','#'),('DOAN08','T02','2021-11-01','2021-11-07',NULL,NULL,NULL),('DOAN09','T03','2021-11-01','2021-11-07',NULL,NULL,NULL),('DOAN10','T03','2021-11-01','2021-11-07',NULL,NULL,NULL),('DOAN11','T04','2021-11-01','2021-11-07',NULL,NULL,NULL),('DOAN12','T04','2021-11-01','2021-11-07',NULL,NULL,NULL),('DOAN13','T04','2021-11-01','2021-11-07',NULL,NULL,NULL),('DOAN14','T05','2021-11-01','2021-11-07',NULL,NULL,NULL),('DOAN15','T05','2021-11-01','2021-11-07',NULL,NULL,NULL);
/*!40000 ALTER TABLE `doandulich` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `giatour`
--

DROP TABLE IF EXISTS `giatour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `giatour` (
  `MaGiaTour` char(10) NOT NULL,
  `ThoiGianBD` date DEFAULT NULL,
  `ThoiGianKT` date DEFAULT NULL,
  `GiaTien` int DEFAULT NULL,
  `MaTour` char(10) DEFAULT NULL,
  PRIMARY KEY (`MaGiaTour`),
  KEY `fk_GT_MaTour` (`MaTour`),
  CONSTRAINT `fk_GT_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giatour`
--

LOCK TABLES `giatour` WRITE;
/*!40000 ALTER TABLE `giatour` DISABLE KEYS */;
INSERT INTO `giatour` VALUES ('GT01','2021-09-01','2022-01-01',5000000,'T01'),('GT02','2021-09-01','2022-01-01',6000000,'T02'),('GT03','2021-09-01','2022-01-01',7000000,'T03'),('GT04','2021-09-01','2022-01-01',8000000,'T04'),('GT05','2021-09-01','2022-01-01',10000000,'T05');
/*!40000 ALTER TABLE `giatour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khachhang`
--

DROP TABLE IF EXISTS `khachhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `khachhang` (
  `MaKhachHang` char(10) NOT NULL,
  `HoTen` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `SoCMND` char(13) DEFAULT NULL,
  `DiaChi` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `GioiTinh` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `SDT` char(12) DEFAULT NULL,
  `QuocTich` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`MaKhachHang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khachhang`
--

LOCK TABLES `khachhang` WRITE;
/*!40000 ALTER TABLE `khachhang` DISABLE KEYS */;
INSERT INTO `khachhang` VALUES ('KH0',NULL,NULL,NULL,NULL,NULL,NULL),('KH01',NULL,NULL,NULL,NULL,NULL,NULL),('KH02',NULL,NULL,NULL,NULL,NULL,NULL),('KH03',NULL,NULL,NULL,NULL,NULL,NULL),('KH04',NULL,NULL,NULL,NULL,NULL,NULL),('KH05',NULL,NULL,NULL,NULL,NULL,NULL),('KH06',NULL,NULL,NULL,NULL,NULL,NULL),('KH07',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `khachhang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loaichiphi`
--

DROP TABLE IF EXISTS `loaichiphi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loaichiphi` (
  `MaLoaiChiPhi` char(10) NOT NULL,
  `TenLoaiChiPhi` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`MaLoaiChiPhi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loaichiphi`
--

LOCK TABLES `loaichiphi` WRITE;
/*!40000 ALTER TABLE `loaichiphi` DISABLE KEYS */;
/*!40000 ALTER TABLE `loaichiphi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loaihinhdulich`
--

DROP TABLE IF EXISTS `loaihinhdulich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loaihinhdulich` (
  `MaLoaiHinh` char(10) NOT NULL,
  `TenLoaiHinh` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`MaLoaiHinh`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loaihinhdulich`
--

LOCK TABLES `loaihinhdulich` WRITE;
/*!40000 ALTER TABLE `loaihinhdulich` DISABLE KEYS */;
INSERT INTO `loaihinhdulich` VALUES ('LH01','Du lịch di động'),('LH02','Du lịch kết hợp nghề nghiệp'),('LH03','Du lịch xã hội và gia đình'),('LH04','Du lịch sinh thái'),('LH05','Du lịch văn hóa, lịch sử');
/*!40000 ALTER TABLE `loaihinhdulich` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nhanvien`
--

DROP TABLE IF EXISTS `nhanvien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nhanvien` (
  `MaNhanVien` char(10) NOT NULL,
  `TenNhanVien` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`MaNhanVien`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nhanvien`
--

LOCK TABLES `nhanvien` WRITE;
/*!40000 ALTER TABLE `nhanvien` DISABLE KEYS */;
/*!40000 ALTER TABLE `nhanvien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phanbonvdoan`
--

DROP TABLE IF EXISTS `phanbonvdoan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `phanbonvdoan` (
  `MaNhanvien` char(10) NOT NULL,
  `MaDoan` char(10) NOT NULL,
  `NhiemVu` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`MaNhanvien`,`MaDoan`),
  KEY `fk_PBNV_MaDoan` (`MaDoan`),
  CONSTRAINT `fk_PBNV_MaDoan` FOREIGN KEY (`MaDoan`) REFERENCES `doandulich` (`MaDoan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_PBNV_MaNhanVien` FOREIGN KEY (`MaNhanvien`) REFERENCES `nhanvien` (`MaNhanVien`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phanbonvdoan`
--

LOCK TABLES `phanbonvdoan` WRITE;
/*!40000 ALTER TABLE `phanbonvdoan` DISABLE KEYS */;
/*!40000 ALTER TABLE `phanbonvdoan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thamquan`
--

DROP TABLE IF EXISTS `thamquan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `thamquan` (
  `MaTour` char(10) NOT NULL,
  `MaDiaDiem` char(10) NOT NULL,
  `ThuTu` int DEFAULT NULL,
  PRIMARY KEY (`MaTour`,`MaDiaDiem`),
  KEY `fk_TQ_MaDiaDiem` (`MaDiaDiem`),
  CONSTRAINT `fk_TQ_MaDiaDiem` FOREIGN KEY (`MaDiaDiem`) REFERENCES `diadiem` (`MaDiaDiem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_TQ_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thamquan`
--

LOCK TABLES `thamquan` WRITE;
/*!40000 ALTER TABLE `thamquan` DISABLE KEYS */;
INSERT INTO `thamquan` VALUES ('T01','DD01',1),('T01','DD02',2),('T01','DD03',3),('T02','DD04',1),('T02','DD05',2),('T02','DD06',3),('T03','DD07',1),('T03','DD08',2),('T03','DD09',3),('T04','DD10',1),('T04','DD11',2),('T04','DD12',3),('T05','DD13',1),('T05','DD14',2),('T05','DD15',3);
/*!40000 ALTER TABLE `thamquan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tourdulich`
--

DROP TABLE IF EXISTS `tourdulich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tourdulich` (
  `MaTour` char(10) NOT NULL,
  `TenTour` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `DacDiem` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `MaLoaiHinh` char(10) DEFAULT NULL,
  PRIMARY KEY (`MaTour`),
  KEY `fk_TDL_MaLoaiHinh` (`MaLoaiHinh`),
  CONSTRAINT `fk_TDL_MaLoaiHinh` FOREIGN KEY (`MaLoaiHinh`) REFERENCES `loaihinhdulich` (`MaLoaiHinh`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tourdulich`
--

LOCK TABLES `tourdulich` WRITE;
/*!40000 ALTER TABLE `tourdulich` DISABLE KEYS */;
INSERT INTO `tourdulich` VALUES ('T01','Sài Gòn - Đà Nẵng','- Chiêm ngưỡng khung cảnh thiên nhiên ngoạn mục trên Cây Cầu Vàng.','LH01'),('T02','Sài Gòn -  Hạ Long','- Tham quan cửa khẩu Hoành Mô, “check-in” cột mốc 1317. ','LH02'),('T03','Sài Gòn - Cà Mau','Đến Đất Mũi, đoàn tham quan Công viên Văn Hóa Mũi Cà Mau - tọa lạc trong khu dự trữ sinh quyển thế giới Mũi Cà Mau được UNESCO công nhận vào tháng 5/2009.','LH03'),('T04','Sài Gòn - Đà Lạt','- Tham quan Khu Du Lịch Trang Trại Rau và Hoa (Làng hoa Vạn Thành), nằm trải rộng cả một thung lũng với bốn bề là rau và hoa đẹp tuyệt vời.','LH04'),('T05','Sài Gòn - Huế','- Hội An với Chùa Cầu Nhật Bản, Nhà Cổ hàng trăm năm tuổi, Hội Quán Phước Kiến & Xưởng thủ công mỹ nghệ.','LH05');
/*!40000 ALTER TABLE `tourdulich` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-22 16:46:36
