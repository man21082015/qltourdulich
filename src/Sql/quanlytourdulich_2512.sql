-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 25, 2021 at 07:53 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quanlytourdulich`
--

-- --------------------------------------------------------

--
-- Table structure for table `chiphi`
--

CREATE TABLE `chiphi` (
  `MaChiPhi` char(10) NOT NULL,
  `MaDoan` char(10) DEFAULT NULL,
  `MaLoaiChiPhi` char(10) DEFAULT NULL,
  `SoTien` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chiphi`
--

INSERT INTO `chiphi` (`MaChiPhi`, `MaDoan`, `MaLoaiChiPhi`, `SoTien`) VALUES
('CP01', 'DOAN01', 'LCP02', 5000000),
('CP02', 'DOAN01', 'LCP04', 2000000),
('CP03', 'DOAN01', 'LCP05', 3000000),
('CP04', 'DOAN02', 'LCP02', 4000000),
('CP05', 'DOAN02', 'LCP04', 3000000),
('CP06', 'DOAN02', 'LCP05', 1000000),
('CP07', 'DOAN03', 'LCP02', 6000000),
('CP08', 'DOAN03', 'LCP04', 2000000),
('CP09', 'DOAN03', 'LCP05', 2000000),
('CP10', 'DOAN04', 'LCP02', 3000000),
('CP11', 'DOAN04', 'LCP04', 2000000),
('CP12', 'DOAN04', 'LCP05', 3000000),
('CP13', 'DOAN05', 'LCP02', 2000000),
('CP14', 'DOAN05', 'LCP04', 2000000),
('CP15', 'DOAN05', 'LCP05', 5000000),
('CP16', 'DOAN06', 'LCP02', 4000000),
('CP17', 'DOAN06', 'LCP04', 2000000),
('CP18', 'DOAN06', 'LCP05', 3000000),
('CP19', 'DOAN07', 'LCP02', 5000000),
('CP20', 'DOAN07', 'LCP04', 2000000),
('CP21', 'DOAN07', 'LCP05', 3000000),
('CP22', 'DOAN08', 'LCP02', 4000000),
('CP23', 'DOAN08', 'LCP04', 3000000),
('CP24', 'DOAN08', 'LCP05', 1000000),
('CP25', 'DOAN09', 'LCP02', 6000000),
('CP26', 'DOAN09', 'LCP04', 2000000),
('CP27', 'DOAN09', 'LCP05', 2000000),
('CP28', 'DOAN10', 'LCP02', 3000000),
('CP29', 'DOAN10', 'LCP04', 2000000),
('CP30', 'DOAN10', 'LCP05', 3000000),
('CP31', 'DOAN11', 'LCP02', 2000000),
('CP32', 'DOAN11', 'LCP04', 2000000),
('CP33', 'DOAN11', 'LCP05', 5000000),
('CP34', 'DOAN12', 'LCP02', 4000000),
('CP35', 'DOAN12', 'LCP04', 2000000),
('CP36', 'DOAN12', 'LCP05', 3000000),
('CP37', 'DOAN13', 'LCP02', 5000000),
('CP38', 'DOAN13', 'LCP04', 2000000),
('CP39', 'DOAN13', 'LCP05', 3000000),
('CP40', 'DOAN14', 'LCP02', 4000000),
('CP41', 'DOAN14', 'LCP04', 3000000),
('CP42', 'DOAN14', 'LCP05', 1000000),
('CP43', 'DOAN15', 'LCP02', 6000000),
('CP44', 'DOAN15', 'LCP04', 2000000),
('CP45', 'DOAN15', 'LCP05', 2000000),
('CP46', 'DOAN16', 'LCP02', 3000000),
('CP47', 'DOAN16', 'LCP04', 2000000),
('CP48', 'DOAN16', 'LCP05', 3000000),
('CP49', 'DOAN17', 'LCP02', 2000000),
('CP50', 'DOAN17', 'LCP04', 2000000),
('CP51', 'DOAN17', 'LCP05', 5000000),
('CP52', 'DOAN18', 'LCP02', 4000000),
('CP53', 'DOAN18', 'LCP04', 2000000),
('CP54', 'DOAN18', 'LCP05', 3000000),
('CP55', 'DOAN19', 'LCP02', 5000000),
('CP56', 'DOAN19', 'LCP04', 2000000),
('CP57', 'DOAN19', 'LCP05', 3000000),
('CP58', 'DOAN20', 'LCP02', 4000000),
('CP59', 'DOAN20', 'LCP04', 3000000),
('CP60', 'DOAN20', 'LCP05', 1000000),
('CP61', 'DOAN21', 'LCP02', 6000000),
('CP62', 'DOAN21', 'LCP04', 2000000),
('CP63', 'DOAN21', 'LCP05', 2000000),
('CP64', 'DOAN22', 'LCP02', 3000000),
('CP65', 'DOAN22', 'LCP04', 2000000),
('CP66', 'DOAN22', 'LCP05', 3000000),
('CP67', 'DOAN23', 'LCP02', 2000000),
('CP68', 'DOAN23', 'LCP04', 2000000),
('CP69', 'DOAN23', 'LCP05', 5000000);

-- --------------------------------------------------------

--
-- Table structure for table `chitietdoan`
--

CREATE TABLE `chitietdoan` (
  `MaDoan` char(10) NOT NULL,
  `MaKhachHang` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chitietdoan`
--

INSERT INTO `chitietdoan` (`MaDoan`, `MaKhachHang`) VALUES
('DOAN01', 'KH001'),
('DOAN01', 'KH002'),
('DOAN01', 'KH003'),
('DOAN01', 'KH004'),
('DOAN01', 'KH005'),
('DOAN01', 'KH006'),
('DOAN01', 'KH024'),
('DOAN01', 'KH025'),
('DOAN02', 'KH007'),
('DOAN02', 'KH008'),
('DOAN02', 'KH009'),
('DOAN02', 'KH010'),
('DOAN02', 'KH011'),
('DOAN02', 'KH012'),
('DOAN03', 'KH013'),
('DOAN03', 'KH014'),
('DOAN03', 'KH015'),
('DOAN03', 'KH016'),
('DOAN03', 'KH017'),
('DOAN03', 'KH018'),
('DOAN04', 'KH019'),
('DOAN04', 'KH020'),
('DOAN04', 'KH021'),
('DOAN04', 'KH022'),
('DOAN04', 'KH023'),
('DOAN04', 'KH024'),
('DOAN05', 'KH025'),
('DOAN05', 'KH026'),
('DOAN05', 'KH027'),
('DOAN05', 'KH028'),
('DOAN05', 'KH029'),
('DOAN05', 'KH030'),
('DOAN06', 'KH031'),
('DOAN06', 'KH032'),
('DOAN06', 'KH033'),
('DOAN06', 'KH034'),
('DOAN06', 'KH035'),
('DOAN06', 'KH036'),
('DOAN07', 'KH037'),
('DOAN07', 'KH038'),
('DOAN07', 'KH039'),
('DOAN07', 'KH040'),
('DOAN07', 'KH041'),
('DOAN07', 'KH042'),
('DOAN08', 'KH043'),
('DOAN08', 'KH044'),
('DOAN08', 'KH045'),
('DOAN08', 'KH046'),
('DOAN08', 'KH047'),
('DOAN08', 'KH048'),
('DOAN09', 'KH049'),
('DOAN09', 'KH050'),
('DOAN09', 'KH051'),
('DOAN09', 'KH052'),
('DOAN09', 'KH053'),
('DOAN09', 'KH054'),
('DOAN10', 'KH055'),
('DOAN10', 'KH056'),
('DOAN10', 'KH057'),
('DOAN10', 'KH058'),
('DOAN10', 'KH059'),
('DOAN10', 'KH060'),
('DOAN11', 'KH061'),
('DOAN11', 'KH062'),
('DOAN11', 'KH063'),
('DOAN11', 'KH064'),
('DOAN11', 'KH065'),
('DOAN11', 'KH066'),
('DOAN12', 'KH067'),
('DOAN12', 'KH068'),
('DOAN12', 'KH069'),
('DOAN12', 'KH070'),
('DOAN12', 'KH071'),
('DOAN12', 'KH072'),
('DOAN13', 'KH073'),
('DOAN13', 'KH074'),
('DOAN13', 'KH075'),
('DOAN13', 'KH076'),
('DOAN13', 'KH077'),
('DOAN13', 'KH078'),
('DOAN14', 'KH079'),
('DOAN14', 'KH080'),
('DOAN14', 'KH081'),
('DOAN14', 'KH082'),
('DOAN14', 'KH083'),
('DOAN14', 'KH084'),
('DOAN15', 'KH085'),
('DOAN15', 'KH086'),
('DOAN15', 'KH087'),
('DOAN15', 'KH088'),
('DOAN15', 'KH089'),
('DOAN15', 'KH090'),
('DOAN16', 'KH091'),
('DOAN16', 'KH092'),
('DOAN16', 'KH093'),
('DOAN16', 'KH094'),
('DOAN16', 'KH095'),
('DOAN16', 'KH096'),
('DOAN17', 'KH097'),
('DOAN17', 'KH098'),
('DOAN17', 'KH099'),
('DOAN17', 'KH100'),
('DOAN17', 'KH101'),
('DOAN17', 'KH102'),
('DOAN18', 'KH103'),
('DOAN18', 'KH104'),
('DOAN18', 'KH105'),
('DOAN18', 'KH106'),
('DOAN18', 'KH107'),
('DOAN18', 'KH108'),
('DOAN19', 'KH109'),
('DOAN19', 'KH110'),
('DOAN19', 'KH111'),
('DOAN19', 'KH112'),
('DOAN19', 'KH113'),
('DOAN20', 'KH114'),
('DOAN20', 'KH115'),
('DOAN20', 'KH116'),
('DOAN20', 'KH117'),
('DOAN20', 'KH118'),
('DOAN20', 'KH119'),
('DOAN21', 'KH120'),
('DOAN21', 'KH121'),
('DOAN21', 'KH122'),
('DOAN21', 'KH123'),
('DOAN21', 'KH124'),
('DOAN21', 'KH125'),
('DOAN22', 'KH126'),
('DOAN22', 'KH127'),
('DOAN22', 'KH128'),
('DOAN22', 'KH129'),
('DOAN22', 'KH130'),
('DOAN22', 'KH131'),
('DOAN22', 'KH132'),
('DOAN23', 'KH133'),
('DOAN23', 'KH134'),
('DOAN23', 'KH135'),
('DOAN23', 'KH136'),
('DOAN23', 'KH137'),
('DOAN23', 'KH138');

-- --------------------------------------------------------

--
-- Table structure for table `diadiem`
--

CREATE TABLE `diadiem` (
  `MaDiaDiem` char(10) NOT NULL,
  `TenDiaDiem` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `diadiem`
--

INSERT INTO `diadiem` (`MaDiaDiem`, `TenDiaDiem`) VALUES
('DD01', 'Cầu Sông Hàn'),
('DD02', 'Bảo tàng tranh 3D'),
('DD03', 'Phố cổ Hội An'),
('DD04', 'Vịnh Hạ Long'),
('DD05', 'Chợ đêm Hạ Long'),
('DD06', 'Công viên hoa Hạ Long'),
('DD07', 'Vườn dâu Cái Tàu'),
('DD08', 'Hòn Đá Bạc'),
('DD09', 'Rừng U Minh Hạ'),
('DD10', 'Quảng trường Lâm Viên'),
('DD11', 'Núi Lang Biang'),
('DD12', 'Thung Lũng Tình Yêu'),
('DD13', 'Chùa Thiên Mụ'),
('DD14', 'Sông Hương'),
('DD15', 'Vườn Quốc Gia Bạch Mã'),
('DD16', 'Tháp Bà Ponagar'),
('DD17', 'Hòn Lao – Đảo Khỉ'),
('DD18', 'Đảo Vinpearland'),
('DD19', 'Khu di tích Địa đạo Bến Dược'),
('DD20', 'Đền Bến Dược'),
('DD21', 'Lâm Viên Cần Giờ'),
('DD22', 'Khu du lịch sinh thái biển Phương Nam'),
('DD23', 'Khu du lịch Dần Xây'),
('DD24', 'Khu du lịch Sinh Thái Vàm Sát'),
('DD25', 'Khu bảo tồn chim'),
('DD26', 'Tháp Tang Bồng'),
('DD27', 'Làng cổ Phước Lộc Thọ'),
('DD28', 'KDL Cánh Đồng Bất Tận'),
('DD29', 'Khu vực Rừng Tràm Gió'),
('DD30', 'Khu vực Đồng Tháp Mười'),
('DD31', 'Núi Bà Đen'),
('DD32', 'Thung lũng Ma Thiên Lãnh'),
('DD33', 'Chùa Hang'),
('DD34', 'Bến tàu du lịch Vĩnh Long'),
('DD35', 'Cù lao Minh'),
('DD36', 'Làng nghề'),
('DD37', 'Chợ Nổi Cái Bè'),
('DD38', 'Chùa Vĩnh Tràng'),
('DD39', 'Ngắm cảnh bốn cù lao Long'),
('DD40', 'Tham quan lò kẹo dừa'),
('DD41', 'Chợ nổi Cái Răng'),
('DD42', 'Viếng Thiền viện Trúc Lâm Phương Nam'),
('DD43', 'Làng Cỏ Ống'),
('DD44', 'Nghĩa địa đầu tiên tại Côn Đảo'),
('DD45', 'Dinh chúa đảo'),
('DD46', 'Trại tù Phú Hải'),
('DD47', 'Cầu tàu 914'),
('DD48', 'Viếng nghĩa trang Hàng Dương'),
('DD49', 'Khu Chuồng Cọp Pháp - Mỹ'),
('DD50', 'Cảng Bến Đầm'),
('DD51', 'Bãi Nhát'),
('DD52', 'Tham quan Dinh Cậu'),
('DD53', 'Chợ đêm Dương Đông'),
('DD54', 'Khám phá Hòn Thơm'),
('DD55', 'Đảo Hòn Dừa'),
('DD56', 'Khu du lịch Sun World'),
('DD57', 'Khu di tích lịch sử'),
('DD58', 'Khu Du Lịch Làng Cafe Trung Nguyên'),
('DD59', 'Khu Du Lịch Buôn Đôn'),
('DD60', 'Sông Serepok'),
('DD61', 'Khu lăng mộ vua săn voi Khun Yu Nốp'),
('DD62', 'CHÙA SẮC TỨ KHẢI ĐOAN'),
('DD63', 'BẢO TÀNG ĐAKLAK'),
('DD64', 'KHU DU LỊCH SINH THÁI KOTAM  '),
('DD65', 'khu du lịch sinh thái Ko Tam'),
('DD66', 'Làng Hoàng Trù'),
('DD67', 'Mộ 13 nữ thanh niên xung phong Truông Bồn'),
('DD68', 'Viếng đền thờ Bà Triệu'),
('DD69', 'Khu du lịch sinh thái Tràng An'),
('DD70', 'Tâm linh Chùa Tam Chúc'),
('DD71', 'Đá Chông – K9'),
('DD72', 'Quảng Trường Ba Đình'),
('DD73', 'Nhà Thờ Đá'),
('DD74', 'Hồ Sapa'),
('DD75', 'Tham quan bản Cát Cát'),
('DD76', 'Khu du lịch núi Hàm Rồng'),
('DD77', 'CHINH PHỤC FANSIPAN');

-- --------------------------------------------------------

--
-- Table structure for table `doandulich`
--

CREATE TABLE `doandulich` (
  `MaDoan` char(10) NOT NULL,
  `MaTour` char(10) DEFAULT NULL,
  `NgayKhoiHanh` date DEFAULT NULL,
  `NgayKetThuc` date DEFAULT NULL,
  `DoanhThu` bigint(20) DEFAULT NULL,
  `HanhTrinh` varchar(10000) CHARACTER SET utf8 DEFAULT NULL,
  `KhachSan` varchar(200) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doandulich`
--

INSERT INTO `doandulich` (`MaDoan`, `MaTour`, `NgayKhoiHanh`, `NgayKetThuc`, `DoanhThu`, `HanhTrinh`, `KhachSan`) VALUES
('DOAN01', 'T01', '2021-09-10', '2021-09-17', 2000000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Phúc Thành Luxury'),
('DOAN02', 'T01', '2021-09-10', '2021-09-17', 1500000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Yến Vy Hotel'),
('DOAN03', 'T01', '2021-09-10', '2021-09-17', 3000000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Robin Hotel'),
('DOAN04', 'T02', '2021-09-20', '2021-09-27', 4000000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Sen Boutique Hotel'),
('DOAN05', 'T03', '2021-10-01', '2021-10-07', 5000000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Mai Boutique Villa'),
('DOAN06', 'T04', '2021-10-01', '2021-10-07', 5500000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'The Confetti Hotel'),
('DOAN07', 'T04', '2021-10-01', '2021-10-07', 2500000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Draha Halong Hotel'),
('DOAN08', 'T05', '2021-10-20', '2021-10-27', 3200000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'The Marine Hotel'),
('DOAN09', 'T06', '2021-11-01', '2021-11-07', 6200000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Quốc Tế Hotel'),
('DOAN10', 'T07', '2021-11-01', '2021-11-07', 3300000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Thanh Trúc Hotel'),
('DOAN11', 'T08', '2021-11-01', '2021-11-07', 1800000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Kings Hotel'),
('DOAN12', 'T08', '2021-11-01', '2021-11-07', 5800000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Mỹ Hy Hotel'),
('DOAN13', 'T09', '2021-11-10', '2021-11-17', 4500000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Dalat Eco Hotel'),
('DOAN14', 'T10', '2021-11-10', '2021-11-17', 1200000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Nice Hotel'),
('DOAN15', 'T11', '2021-11-21', '2021-11-28', 4800000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Hotel La Perle'),
('DOAN16', 'T11', '2021-11-21', '2021-11-28', 4500000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Rex Hotel & Apartment'),
('DOAN17', 'T12', '2021-11-21', '2021-11-28', 8000000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Good Hotel'),
('DOAN18', 'T13', '2021-11-30', '2021-12-07', 7000000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Angela Hotel'),
('DOAN19', 'T13', '2021-11-30', '2021-12-07', 2200000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Messi Hotel'),
('DOAN20', 'T14', '2021-12-10', '2021-11-17', 7200000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Cát Phượng Hotel'),
('DOAN21', 'T15', '2021-12-10', '2021-12-17', 5600000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Minh Khuê Hotel'),
('DOAN22', 'T16', '2021-12-20', '2021-12-27', 2800000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Bamboo Hotel\r '),
('DOAN23', 'T17', '2021-12-20', '2021-12-27', 3800000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.', 'Thạch Anh Hotel');

-- --------------------------------------------------------

--
-- Table structure for table `giatour`
--

CREATE TABLE `giatour` (
  `MaGiaTour` char(10) NOT NULL,
  `ThoiGianBD` date DEFAULT NULL,
  `ThoiGianKT` date DEFAULT NULL,
  `GiaTien` int(11) DEFAULT NULL,
  `MaTour` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `giatour`
--

INSERT INTO `giatour` (`MaGiaTour`, `ThoiGianBD`, `ThoiGianKT`, `GiaTien`, `MaTour`) VALUES
('GT01', '2021-09-01', '2022-01-01', 5000000, 'T01'),
('GT02', '2021-09-01', '2022-01-01', 6000000, 'T02'),
('GT03', '2021-09-01', '2022-01-01', 7000000, 'T03'),
('GT04', '2021-09-01', '2022-01-01', 8000000, 'T04'),
('GT05', '2021-09-01', '2022-01-01', 10000000, 'T05'),
('GT06', '2021-09-01', '2022-01-01', 4000000, 'T06'),
('GT07', '2021-09-01', '2022-01-01', 650000, 'T07'),
('GT08', '2021-09-01', '2022-01-01', 1600000, 'T08'),
('GT09', '2021-09-01', '2022-01-01', 1279000, 'T09'),
('GT10', '2021-09-01', '2022-01-01', 2000000, 'T10'),
('GT11', '2021-09-01', '2022-01-01', 3000000, 'T11'),
('GT12', '2021-09-01', '2022-01-01', 1979000, 'T12'),
('GT13', '2021-09-01', '2022-01-01', 5700000, 'T13'),
('GT14', '2021-09-01', '2022-01-01', 5510000, 'T14'),
('GT15', '2021-09-01', '2022-01-01', 1550000, 'T15'),
('GT16', '2021-09-01', '2022-01-01', 8000000, 'T16'),
('GT17', '2021-09-01', '2022-01-01', 2350000, 'T17'),
('GT18', '2022-01-02', '2022-01-31', 6500000, 'T01');

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `MaKhachHang` char(10) NOT NULL,
  `HoTen` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `SoCMND` char(13) DEFAULT NULL,
  `DiaChi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `GioiTinh` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `SDT` char(12) DEFAULT NULL,
  `QuocTich` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MaKhachHang`, `HoTen`, `SoCMND`, `DiaChi`, `GioiTinh`, `SDT`, `QuocTich`) VALUES
('KH001', 'A Giơ', '233259791', '612 Khu phố 2, Phường Bình Trị Đông B, Q.Bình Tân', 'Nam', '0586037708', 'Lào'),
('KH002', 'Y Djok', '233259792', '278 Ngô Quyền, Q8 ', 'Nam', '0586073704', 'Lào'),
('KH003', 'Trần Quỳnh Nhi', '233259794', '180B Lê Văn Sỹ, Q10', 'Nữ', '0586512274', 'Việt Nam'),
('KH004', 'Nguyễn Thị Trường An', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH005', 'Nguyễn Thị Huyền', '233259812', 'B16 Song hành An phú – An Khánh Q.02 TPHCM', 'Nữ', '0909522702', 'Việt Nam'),
('KH006', 'Bùi Duy Khương', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH007', 'Bùi Hoàng Lệ Diễm', '233259814', '503 Cao ốc  An Phú P.An Phú Q.2 TPHCM', 'Nữ', '0903734355', 'Việt Nam'),
('KH008', 'Bùi Kim Anh', '233259802', '84 Đường 1 khu B , An Phú Q.02 TPHCM', 'Nữ', '0989991211', 'Việt Nam'),
('KH009', 'Bùi Lê Thùy Trâm', '233259798', 'lầu 8 , phòng 801, Cao ốc An Khánh, Khu A P.AN KHANH Q.2 TPHCM', 'Nữ', '0907225050', 'Việt Nam'),
('KH010', 'Bùi Ngọc Minh', '233259800', '128 đường 17B p. An Phú Q.02 TPHCM', 'Nam', '0903683668', 'Việt Nam'),
('KH011', 'Lương Anh Phụng', '233259801', 'Long Trị, Long Mỹ, Hậu Giang', 'Nữ', '0989209286', 'Việt Nam'),
('KH012', 'Hoàng Trung Chính', '233256888', '300/43 Nguyễn Văn Linh, Phường Bình Thuận, Quận 7', 'Nam', '0907248555', 'Việt Nam'),
('KH013', 'Đặng Huỳnh Thành Nhân', '233259802', '41/27 Hương Lộ 14, H/Tân, Q.TP', 'Nam', '0908285117', 'Việt Nam'),
('KH014', 'Phùng Thị Ánh Tuyết', '233259821', '307 cao ốc an khánh song hành tổ 42, An Phú Q.02 TPHCM', 'Nữ', '0866718337', 'Việt Nam'),
('KH015', 'Nguyễn Văn Cường', '233259812', '57 đường 17A khu B an phú an khánh P.An Phú Q.02 TPHCM', 'Nam', '0913741210', 'Việt Nam'),
('KH016', 'Đặng Văn Phúc', '233259804', 'F106 cc 5 tầng Đường 8 B Khu an phú P.An Khánh Q.02 TPHCM', 'Nữ', '0905611688', 'Việt Nam'),
('KH017', 'Đinh Thị Mĩ Thanh', '233259000', '31 Trần Chánh Chiếu, P14, Q5', 'Nữ', '0909522702', 'Việt Nam'),
('KH018', 'Bùi Thị Mai Hương', '233252228', '1080 đường 10B p. An Phú Q.02 TPHCM', 'Nữ', '0983277149-0', 'Việt Nam'),
('KH019', 'Đoàn Bá Thước', '233259818', '31 Trần Chánh Chiếu, P14, Q5', 'Nam', '0903190490', 'Việt Nam'),
('KH020', 'Đỗ Minh Tuấn', '223253212', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0982782043', 'Việt Nam'),
('KH021', 'Đặng Thị Bích Liễu', '233252312', '246/28 Phan Huy Ích, P12, Q Gò Vấp', 'Nữ', '0989782343', 'Việt Nam'),
('KH022', 'Đào Trung Thu', '233259333', '1A73 Ấp 1 Phạm Văn Hai, H Bình Chánh', 'Nữ', '0918152212', 'Việt Nam'),
('KH023', 'Johny Ngô', '233259820', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0918157857', 'Paris'),
('KH024', 'Đinh Thị Hoài Phương', '233259824', '2/41 Cao Thắng P5, Q3', 'Nữ', '0907222605', 'Việt Nam'),
('KH025', 'Henry', '233259825', '33/5, Trần Bình Trọng, P1, Q5', 'Nam', '0908327657', 'Mỹ'),
('KH026', 'Nguyễn Trung Quân', '233259790', '170/14 Đường số 204 Cao Lỗ P, P4, Q8', 'Nam', '0586072996', 'Việt Nam'),
('KH027', 'A Long', '233259791', '612 Khu phố 2, Phường Bình Trị Đông B, Q.Bình Tân', 'Nam', '0586037708', 'Lào'),
('KH028', 'Y Djok Jakkak', '233259792', '278 Ngô Quyền, Q8', 'Nam', '0586073704', 'Lào'),
('KH029', 'Trần Quỳnh Anh', '233259794', '180B Lê Văn Sỹ, Q10', 'Nữ', '0586512274', 'Việt Nam'),
('KH030', 'Nguyễn Thị Thúy', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH031', 'Nguyễn Thị Ngọc Hiền', '233259812', 'B16 Song hành An phú – An Khánh Q.02 TPHCM', 'Nữ', '0909522702', 'Việt Nam'),
('KH032', 'Bùi Duy Mạnh', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH033', 'Bùi Hoàng Lệ', '233259814', '503 Cao ốc  An Phú P.An Phú Q.2 TPHCM', 'Nữ', '0903734355', 'Việt Nam'),
('KH034', 'Trần Kim Anh', '233259802', '84 Đường 1 khu B , An Phú Q.02 TPHCM', 'Nữ', '0989991211', 'Việt Nam'),
('KH035', 'Lê Thùy Trâm', '233259798', 'lầu 8 , phòng 801, Cao ốc An Khánh, Khu A P.AN KHANH Q.2 TPHCM', 'Nữ', '0907225050', 'Việt Nam'),
('KH036', 'Bùi Tiến Dũng', '233259800', '128 đường 17B p. An Phú Q.02 TPHCM', 'Nam', '0903683668', 'Việt Nam'),
('KH037', 'Lương Anh Minh', '233259801', 'Long Trị, Long Mỹ, Hậu Giang', 'Nữ', '0989209286', 'Việt Nam'),
('KH038', 'Hoàng Trung Chực', '233256888', '300/43 Nguyễn Văn Linh, Phường Bình Thuận, Quận 7', 'Nam', '0907248555', 'Việt Nam'),
('KH039', 'Đặng Huỳnh Phan', '233259802', '41/27 Hương Lộ 14, H/Tân, Q.TP', 'Nam', '0908285117', 'Việt Nam'),
('KH040', 'Phùng Ánh Tuyết', '233259821', '307 cao ốc an khánh song hành tổ 42, An Phú Q.02 TPHCM', 'Nữ', '0866718337', 'Việt Nam'),
('KH041', 'Nguyễn Văn Cơ', '233259812', '57 đường 17A khu B an phú an khánh P.An Phú Q.02 TPHCM', 'Nam', '0913741210', 'Việt Nam'),
('KH042', 'Võ Văn Phúc', '233259804', 'F106 cc 5 tầng Đường 8 B Khu an phú P.An Khánh Q.02 TPHCM', 'Nữ', '0905611688', 'Việt Nam'),
('KH043', 'Đinh Thị Mĩ Vy', '233259000', '31 Trần Chánh Chiếu, P14, Q5', 'Nữ', '0909522702', 'Việt Nam'),
('KH044', 'Lê Thị Mai Hương', '233252228', '1080 đường 10B p. An Phú Q.02 TPHCM', 'Nữ', '0983277149-0', 'Việt Nam'),
('KH045', 'Đoàn Bá Thước', '233259818', '31 Trần Chánh Chiếu, P14, Q5', 'Nam', '0903190490', 'Việt Nam'),
('KH046', 'Đỗ Minh Thiện', '223253212', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0982782043', 'Việt Nam'),
('KH047', 'Đặng Thị Bích ', '233252312', '246/28 Phan Huy Ích, P12, Q Gò Vấp', 'Nữ', '0989782343', 'Việt Nam'),
('KH048', 'Đào Trung Kiên', '233259333', '1A73 Ấp 1 Phạm Văn Hai, H Bình Chánh', 'Nữ', '0918152212', 'Việt Nam'),
('KH049', 'Johny Ngô Ngố', '233259820', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0918157857', 'Paris'),
('KH050', 'Phan Hoài Phương', '233259824', '2/41 Cao Thắng P5, Q3', 'Nữ', '0907222605', 'Việt Nam'),
('KH051', 'Henry Tran', '233259825', '33/5, Trần Bình Trọng, P1, Q5', 'Nam', '0908327657', 'Mỹ'),
('KH052', 'Nguyễn Thị Thúy An', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH053', 'Nguyễn Thị Huyền Trang', '233259812', 'B16 Song hành An phú – An Khánh Q.02 TPHCM', 'Nữ', '0909522702', 'Việt Nam'),
('KH054', 'Bùi Gia Khương', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH055', 'Lê Thị Mỹ Lệ', '233259814', '503 Cao ốc  An Phú P.An Phú Q.2 TPHCM', 'Nữ', '0903734355', 'Việt Nam'),
('KH056', 'Nguyễn Ngọc Ánh', '233259802', '84 Đường 1 khu B , An Phú Q.02 TPHCM', 'Nữ', '0989991211', 'Việt Nam'),
('KH057', 'Hoàng Thùy Trâm', '233259798', 'lầu 8 , phòng 801, Cao ốc An Khánh, Khu A P.AN KHANH Q.2 TPHCM', 'Nữ', '0907225050', 'Việt Nam'),
('KH058', 'Nguyễn Ngọc Minh', '233259800', '128 đường 17B p. An Phú Q.02 TPHCM', 'Nam', '0903683668', 'Việt Nam'),
('KH059', 'Huỳnh Anh Phụng', '233259801', 'Long Trị, Long Mỹ, Hậu Giang', 'Nữ', '0989209286', 'Việt Nam'),
('KH060', 'Hoàng Văn Chính', '233256888', '300/43 Nguyễn Văn Linh, Phường Bình Thuận, Quận 7', 'Nam', '0907248555', 'Việt Nam'),
('KH061', 'Đặng Thị Xuân', '233259802', '41/27 Hương Lộ 14, H/Tân, Q.TP', 'Nam', '0908285117', 'Việt Nam'),
('KH062', 'Phan Thị Ánh Tuyết', '233259821', '307 cao ốc an khánh song hành tổ 42, An Phú Q.02 TPHCM', 'Nữ', '0866718337', 'Việt Nam'),
('KH063', 'Nguyễn Văn Chung', '233259812', '57 đường 17A khu B an phú an khánh P.An Phú Q.02 TPHCM', 'Nam', '0913741210', 'Việt Nam'),
('KH064', 'Vũ Anh Phúc', '233259804', 'F106 cc 5 tầng Đường 8 B Khu an phú P.An Khánh Q.02 TPHCM', 'Nữ', '0905611688', 'Việt Nam'),
('KH065', 'Đinh Thị Mĩ Tiên', '233259000', '31 Trần Chánh Chiếu, P14, Q5', 'Nữ', '0909522702', 'Việt Nam'),
('KH066', 'Bùi Thị Thúy Vy', '233252228', '1080 đường 10B p. An Phú Q.02 TPHCM', 'Nữ', '0983277149-0', 'Việt Nam'),
('KH067', 'Đoàn Bá Phong', '233259818', '31 Trần Chánh Chiếu, P14, Q5', 'Nam', '0903190490', 'Việt Nam'),
('KH068', 'Trương Minh Tuấn', '223253212', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0982782043', 'Việt Nam'),
('KH069', 'Đặng Thị Liễu', '233252312', '246/28 Phan Huy Ích, P12, Q Gò Vấp', 'Nữ', '0989782343', 'Việt Nam'),
('KH070', 'Đào Trung ', '233259333', '1A73 Ấp 1 Phạm Văn Hai, H Bình Chánh', 'Nữ', '0918152212', 'Việt Nam'),
('KH071', 'Johny Đặng', '233259820', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0918157857', 'Paris'),
('KH072', 'Đinh Thị Hoài ', '233259824', '2/41 Cao Thắng P5, Q3', 'Nữ', '0907222605', 'Việt Nam'),
('KH073', 'Joker Amen', '233259825', '33/5, Trần Bình Trọng, P1, Q5', 'Nam', '0908327657', 'Mỹ'),
('KH074', 'Phùng Thị Bành', '233259821', '307 cao ốc an khánh song hành tổ 42, An Phú Q.02 TPHCM', 'Nữ', '0866718337', 'Việt Nam'),
('KH075', 'Nguyễn Chí Trung', '233259812', '57 đường 17A khu B an phú an khánh P.An Phú Q.02 TPHCM', 'Nam', '0913741210', 'Việt Nam'),
('KH076', 'Đinh Thị Mơ', '233259804', 'F106 cc 5 tầng Đường 8 B Khu an phú P.An Khánh Q.02 TPHCM', 'Nữ', '0905611688', 'Việt Nam'),
('KH077', 'Đinh Thị Mĩ Hạnh', '233259000', '31 Trần Chánh Chiếu, P14, Q5', 'Nữ', '0909522702', 'Việt Nam'),
('KH078', 'Bùi Thị Mai Lan', '233252228', '1080 đường 10B p. An Phú Q.02 TPHCM', 'Nữ', '0983277149-0', 'Việt Nam'),
('KH079', 'Đoàn Ngọc Hải', '233259818', '31 Trần Chánh Chiếu, P14, Q5', 'Nam', '0903190490', 'Việt Nam'),
('KH080', 'Đỗ Minh Kiệt', '223253212', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0982782043', 'Việt Nam'),
('KH081', 'Đặng Thị Bích Ngọc', '233252312', '246/28 Phan Huy Ích, P12, Q Gò Vấp', 'Nữ', '0989782343', 'Việt Nam'),
('KH082', 'Đào Trần', '233259333', '1A73 Ấp 1 Phạm Văn Hai, H Bình Chánh', 'Nữ', '0918152212', 'Việt Nam'),
('KH083', 'Ngô Kiến Huy', '233259820', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0918157857', 'Paris'),
('KH084', 'Đinh Thị Hoài Mong', '233259824', '2/41 Cao Thắng P5, Q3', 'Nữ', '0907222605', 'Việt Nam'),
('KH085', 'A Lô Ha', '233259791', '612 Khu phố 2, Phường Bình Trị Đông B, Q.Bình Tân', 'Nam', '0586037708', 'Lào'),
('KH086', 'Y Djokovick', '233259792', '278 Ngô Quyền, Q8', 'Nam', '0586073704', 'Lào'),
('KH087', 'Trần Quỳnh Hảo', '233259794', '180B Lê Văn Sỹ, Q10', 'Nữ', '0586512274', 'Việt Nam'),
('KH088', 'Nguyễn An Nhiên', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH089', 'Nguyễn Huỳnh Huyền', '233259812', 'B16 Song hành An phú – An Khánh Q.02 TPHCM', 'Nữ', '0909522702', 'Việt Nam'),
('KH090', 'Bùi Kha Khương', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH091', 'Bùi Lệ Diễm Lê', '233259814', '503 Cao ốc  An Phú P.An Phú Q.2 TPHCM', 'Nữ', '0903734355', 'Việt Nam'),
('KH092', 'Bùi Kim Pha', '233259802', '84 Đường 1 khu B , An Phú Q.02 TPHCM', 'Nữ', '0989991211', 'Việt Nam'),
('KH093', 'Bùi Lê Thùy Thổ', '233259798', 'lầu 8 , phòng 801, Cao ốc An Khánh, Khu A P.AN KHANH Q.2 TPHCM', 'Nữ', '0907225050', 'Việt Nam'),
('KH094', 'Bùi Ngọc Tiến', '233259800', '128 đường 17B p. An Phú Q.02 TPHCM', 'Nam', '0903683668', 'Việt Nam'),
('KH095', 'Lương Anh Phượng', '233259801', 'Long Trị, Long Mỹ, Hậu Giang', 'Nữ', '0989209286', 'Việt Nam'),
('KH096', 'Hoàng Trung Trác', '233256888', '300/43 Nguyễn Văn Linh, Phường Bình Thuận, Quận 7', 'Nam', '0907248555', 'Việt Nam'),
('KH097', 'Nguyễn Thành Nhân', '233259802', '41/27 Hương Lộ 14, H/Tân, Q.TP', 'Nam', '0908285117', 'Việt Nam'),
('KH098', 'Phùng Thị Ánh Ly', '233259821', '307 cao ốc an khánh song hành tổ 42, An Phú Q.02 TPHCM', 'Nữ', '0866718337', 'Việt Nam'),
('KH099', 'Nguyễn Văn Chờ', '233259812', '57 đường 17A khu B an phú an khánh P.An Phú Q.02 TPHCM', 'Nam', '0913741210', 'Việt Nam'),
('KH100', 'Đặng Thế Phúc', '233259804', 'F106 cc 5 tầng Đường 8 B Khu an phú P.An Khánh Q.02 TPHCM', 'Nữ', '0905611688', 'Việt Nam'),
('KH101', 'Đinh Thị Mĩ ', '233259000', '31 Trần Chánh Chiếu, P14, Q5', 'Nữ', '0909522702', 'Việt Nam'),
('KH102', 'Bùi Thị Lan Hương', '233252228', '1080 đường 10B p. An Phú Q.02 TPHCM', 'Nữ', '0983277149-0', 'Việt Nam'),
('KH103', 'Đoàn Bá Đạo', '233259818', '31 Trần Chánh Chiếu, P14, Q5', 'Nam', '0903190490', 'Việt Nam'),
('KH104', 'Đỗ Minh Trai', '223253212', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0982782043', 'Việt Nam'),
('KH105', 'Đặng Thị Bích Hằng', '233252312', '246/28 Phan Huy Ích, P12, Q Gò Vấp', 'Nữ', '0989782343', 'Việt Nam'),
('KH106', 'Đào Trung Thiện', '233259333', '1A73 Ấp 1 Phạm Văn Hai, H Bình Chánh', 'Nữ', '0918152212', 'Việt Nam'),
('KH107', 'Johny Cho', '233259820', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0918157857', 'Paris'),
('KH108', 'Lê Hoài Phương Linh', '233259824', '2/41 Cao Thắng P5, Q3', 'Nữ', '0907222605', 'Việt Nam'),
('KH109', 'John', '233259825', '33/5, Trần Bình Trọng, P1, Q5', 'Nam', '0908327657', 'Mỹ'),
('KH110', 'Nguyễn Trung Hòa', '233259790', '170/14 Đường số 204 Cao Lỗ P, P4, Q8', 'Nam', '0586072996', 'Việt Nam'),
('KH111', 'A Labaraha', '233259791', '612 Khu phố 2, Phường Bình Trị Đông B, Q.Bình Tân', 'Nam', '0586037708', 'Lào'),
('KH112', 'Y Dyhakimi', '233259792', '278 Ngô Quyền, Q8', 'Nam', '0586073704', 'Lào'),
('KH113', 'Trương Quỳnh Anh', '233259794', '180B Lê Văn Sỹ, Q10', 'Nữ', '0586512274', 'Việt Nam'),
('KH114', 'Nguyễn Thị Uyên', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH115', 'Lê Ngọc Hiền', '233259812', 'B16 Song hành An phú – An Khánh Q.02 TPHCM', 'Nữ', '0909522702', 'Việt Nam'),
('KH116', 'Trần Duy Mạnh', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH117', 'Bùi Hoàng Thế Phong', '233259814', '503 Cao ốc  An Phú P.An Phú Q.2 TPHCM', 'Nữ', '0903734355', 'Việt Nam'),
('KH118', 'Trần Kim Chi', '233259802', '84 Đường 1 khu B , An Phú Q.02 TPHCM', 'Nữ', '0989991211', 'Việt Nam'),
('KH119', 'Lê Thùy My', '233259798', 'lầu 8 , phòng 801, Cao ốc An Khánh, Khu A P.AN KHANH Q.2 TPHCM', 'Nữ', '0907225050', 'Việt Nam'),
('KH120', 'Hòa Tiến Dũng', '233259800', '128 đường 17B p. An Phú Q.02 TPHCM', 'Nam', '0903683668', 'Việt Nam'),
('KH121', 'Lương Anh Thơ', '233259801', 'Long Trị, Long Mỹ, Hậu Giang', 'Nữ', '0989209286', 'Việt Nam'),
('KH122', 'Hoàng Trung ', '233256888', '300/43 Nguyễn Văn Linh, Phường Bình Thuận, Quận 7', 'Nam', '0907248555', 'Việt Nam'),
('KH123', 'Đặng Anh Phan', '233259802', '41/27 Hương Lộ 14, H/Tân, Q.TP', 'Nam', '0908285117', 'Việt Nam'),
('KH124', 'Nguyến Ánh Tuyết', '233259821', '307 cao ốc an khánh song hành tổ 42, An Phú Q.02 TPHCM', 'Nữ', '0866718337', 'Việt Nam'),
('KH125', 'Lê Văn Cấc', '233259812', '57 đường 17A khu B an phú an khánh P.An Phú Q.02 TPHCM', 'Nam', '0913741210', 'Việt Nam'),
('KH126', 'Lê Văn Cáp', '233259804', 'F106 cc 5 tầng Đường 8 B Khu an phú P.An Khánh Q.02 TPHCM', 'Nữ', '0905611688', 'Việt Nam'),
('KH127', 'Lê Văn Dô Cơ', '233259000', '31 Trần Chánh Chiếu, P14, Q5', 'Nữ', '0909522702', 'Việt Nam'),
('KH128', 'Trần Đình Nện', '233252228', '1080 đường 10B p. An Phú Q.02 TPHCM', 'Nữ', '0983277149-0', 'Việt Nam'),
('KH129', 'Nam Tab', '233259818', '31 Trần Chánh Chiếu, P14, Q5', 'Nam', '0903190490', 'Việt Nam'),
('KH130', 'Trần Minh Huy', '223253212', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0982782043', 'Việt Nam'),
('KH131', 'Đặng Thị Bích Thạch', '233252312', '246/28 Phan Huy Ích, P12, Q Gò Vấp', 'Nữ', '0989782343', 'Việt Nam'),
('KH132', 'Đào Trung Bảo', '233259333', '1A73 Ấp 1 Phạm Văn Hai, H Bình Chánh', 'Nữ', '0918152212', 'Việt Nam'),
('KH133', 'Mbappe', '233259820', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0918157857', 'Paris'),
('KH134', 'Phan Hoài Thương', '233259824', '2/41 Cao Thắng P5, Q3', 'Nữ', '0907222605', 'Việt Nam'),
('KH135', 'David ', '233259825', '33/5, Trần Bình Trọng, P1, Q5', 'Nam', '0908327657', 'Mỹ'),
('KH136', 'Nguyễn Thị Thúy Kiều', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH137', 'Trần Huyền Trang', '233259812', 'B16 Song hành An phú – An Khánh Q.02 TPHCM', 'Nữ', '0909522702', 'Việt Nam'),
('KH138', 'Bùi Gia Hòa', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH139', 'Bùi Duy Khương', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH140', 'Nguyễn Thị Trường An', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH141', 'Bùi Hoàng Lệ Diễm', '233259814', '503 Cao ốc  An Phú P.An Phú Q.2 TPHCM', 'Nữ', '0903734355', 'Việt Nam'),
('KH142', 'Trần Quỳnh Nhi', '233259794', '180B Lê Văn Sỹ, Q10', 'Nữ', '0586512274', 'Việt Nam');

-- --------------------------------------------------------

--
-- Table structure for table `loaichiphi`
--

CREATE TABLE `loaichiphi` (
  `MaLoaiChiPhi` char(10) NOT NULL,
  `TenLoaiChiPhi` varchar(200) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loaichiphi`
--

INSERT INTO `loaichiphi` (`MaLoaiChiPhi`, `TenLoaiChiPhi`) VALUES
('LCP01', 'Chi phí vé máy bay'),
('LCP02', 'Chi phí di chuyển'),
('LCP03', 'Chi phí lưu trú'),
('LCP04', 'Chi phí ăn uống'),
('LCP05', 'Chi phí tham quan'),
('LCP06', 'Chi phí phát sinh');

-- --------------------------------------------------------

--
-- Table structure for table `loaihinhdulich`
--

CREATE TABLE `loaihinhdulich` (
  `MaLoaiHinh` char(10) NOT NULL,
  `TenLoaiHinh` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loaihinhdulich`
--

INSERT INTO `loaihinhdulich` (`MaLoaiHinh`, `TenLoaiHinh`) VALUES
('LH01', 'Du lịch di động'),
('LH02', 'Du lịch kết hợp nghề nghiệp'),
('LH03', 'Du lịch xã hội và gia đình'),
('LH04', 'Du lịch sinh thái'),
('LH05', 'Du lịch văn hóa, lịch sử'),
('LH06', 'Du lịch Teambuilding'),
('LH07', 'Du lịch ẩm thực'),
('LH08', 'Du lịch Mice'),
('LH09', 'Du lịch nghỉ dưỡng'),
('LH10', 'Du lịch thể thao');

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `MaNV` char(10) NOT NULL,
  `HoTenNV` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  `GioiTinh` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `SĐT` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DiaChi` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Luong` int(11) DEFAULT NULL,
  `TrangThai` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`MaNV`, `HoTenNV`, `NgaySinh`, `GioiTinh`, `SĐT`, `DiaChi`, `Luong`, `TrangThai`) VALUES
('NV01', 'Lê Thanh Hiếu', '1997-07-17', 'Nam', '0906522017', '159 Lê Đức Thọ, Phường 16, Q. Gò Vấp\r\n', 10000000, 'Đang làm việc'),
('NV02', 'Nguyễn Xuân Yến', '1999-09-24', 'Nữ', '0903659781', '375 Hai Bà Trưng, Phường 8, Quận 3, TP.HCM', 15000000, 'Đang làm việc'),
('NV03', 'Lê Vân Anh', '1999-10-08', 'Nữ', '0786269875', '309 Hoàng Diệu, Phường 6, Quận 4, TP. Hồ Chí Minh', 20000000, 'Đang làm việc'),
('NV04', 'Hồ Duy Khải', '1987-02-28', 'Nam', '0986789156', '161 Bạch Đằng, Phường 2, Tân Bình, TPHCM', 18000000, 'Đang làm việc'),
('NV05', 'Kim Thị Thanh Thúy', '1988-05-28', 'Nữ', '0986498212', '1030 Tỉnh Lộ 43, P. Bình Chiểu, Q. Thủ Đức, TP. HCM', 15600000, 'Đang làm việc'),
('NV06', 'Đoàn Quốc Dũng', '1983-07-03', 'Nam', '0697498126', '95C, đường Hòa Hưng, Phường 12, Quận 10', 12000000, 'Đang làm việc'),
('NV07', 'Nguyễn Chí Hiếu', '1995-10-15', 'Nam', '0654988691', '458 Tân Thới Hiệp 02, Khu Phố 3A, P. Tân Thới Hiệp, Q. 12, TP. HCM', 15000000, 'Đang làm việc'),
('NV08', 'Lý Anh Kiệt', '1995-10-23', 'Nam', '0659875196', '644 Kinh Dương Vương, An Lạc, Bình Tân, TP.HCM ', 15000000, 'Đang làm việc'),
('NV09', 'Nguyễn Ngọc Phi Giao', '1993-08-02', 'Nữ', '0986546214', '596 Nguyễn Chí Thanh, Phường 7, Quận 11, TP. HCM ', 1000000, 'Đang làm việc'),
('NV10', 'Nguyễn Văn Tuấn Anh', '1999-02-03', 'Nam', '0986162164', '137 Nguyễn Thái Sơn, Phường 4, Gò Vấp, TP. HCM', 16000000, 'Đang làm việc'),
('NV11', 'Ngô Thị Thúy Liễu', '1985-03-12', 'Nữ', '0964564589', '514 Đường 3 Tháng 2, P. 14, Q. 10, Tp. Hồ Chí Minh ', 15600000, 'Đang làm việc'),
('NV12', 'Lê Quốc Nghĩa', '1999-11-26', 'Nam', '0659875165', '107 Điện Biên Phủ, Đa Kao, Quận 1, TP.HCM ', 15000000, 'Đang làm việc'),
('NV13', 'Lê Kiều Nguyệt', '1993-01-14', 'Nữ', '065498479', '11 Nguyễn Kiệm, Phường 3, Quận Gò Vấp, TP.HCM ', 15000000, 'Đang làm việc'),
('NV14', 'Vũ Phan Hoàng Phúc', '1993-02-28', 'Nam', '0316541236', '161 Bạch đằng, P. 2, Q. Tân Bình, Thành phố Hồ Chí Minh', 16500000, 'Đang làm việc'),
('NV15', 'Trần Ngọc Quyên', '1997-09-09', 'Nữ', '0956498456', '248A Nơ Trang Long, P. 12, Q. Bình Thạnh, TP.HCM', 13000000, 'Đang làm việc'),
('NV16', 'Võ Công Tạo', '1994-02-23', 'Nam', '0783216545', '514 Đường 3 Tháng 2, Phường 14, Quận 1', 18900000, 'Đang làm việc'),
('NV17', 'Đặng Thị Phương Thảo', '1985-12-08', 'Nữ', '0990065465', '31 Hồ Biểu Chánh, Phường 12, Quận Phú Nhuận, TP.HCM', 15000000, 'Đang làm việc'),
('NV18', 'Trần Mai Thảo', '1980-12-08', 'Nữ', '0786546597', '1 Tô Hiến Thành, Phường 14, Quận 10, Thành phố Hồ Chí Minh.', 16500000, 'Đang làm việc'),
('NV19', 'Trần Trương Phương Thắm', '1881-12-01', 'Nữ', '0909616545', '148 Hoàng Hoa Thám, Phường 12, Quận Tân Bình, TP.HCM', 12000000, 'Đang làm việc'),
('NV20', 'Huỳnh Thị Minh Thư', '1987-05-05', 'Nữ', '065465469', '115 Tân Quý, P. Tân Quý, Q.Tân Phú', 10000000, 'Đang làm việc'),
('NV21', 'Lê Thanh Hải', '1997-07-17', 'Nam', '0906522017', '159 Lê Đức Thọ, Phường 16, Q. Gò Vấp\r\n', 10000000, 'Đang làm việc'),
('NV22', 'Nguyễn Xuân ', '1999-09-24', 'Nữ', '0903659781', '375 Hai Bà Trưng, Phường 8, Quận 3, TP.HCM', 15000000, 'Đang làm việc'),
('NV23', 'Lê Vân ', '1999-10-08', 'Nữ', '0786269875', '309 Hoàng Diệu, Phường 6, Quận 4, TP. Hồ Chí Minh', 20000000, 'Đang làm việc'),
('NV24', 'Hồ Duy Khánh', '1987-02-28', 'Nam', '0986789156', '161 Bạch Đằng, Phường 2, Tân Bình, TPHCM', 18000000, 'Đang làm việc'),
('NV25', 'Kim Thị Thanh ', '1988-05-28', 'Nữ', '0986498212', '1030 Tỉnh Lộ 43, P. Bình Chiểu, Q. Thủ Đức, TP. HCM', 15600000, 'Đang làm việc'),
('NV26', 'Đoàn Quốc Hùng', '1983-07-03', 'Nam', '0697498126', '95C, đường Hòa Hưng, Phường 12, Quận 10', 12000000, 'Đang làm việc'),
('NV27', 'Nguyễn Chí Thanh', '1995-10-15', 'Nam', '0654988691', '458 Tân Thới Hiệp 02, Khu Phố 3A, P. Tân Thới Hiệp, Q. 12, TP. HCM', 15000000, 'Đang làm việc'),
('NV28', 'Lý Anh Vĩ', '1995-10-23', 'Nam', '0659875196', '644 Kinh Dương Vương, An Lạc, Bình Tân, TP.HCM ', 15000000, 'Đang làm việc'),
('NV29', 'Nguyễn Ngọc Phi Nhung', '1993-08-02', 'Nữ', '0986546214', '596 Nguyễn Chí Thanh, Phường 7, Quận 11, TP. HCM ', 1000000, 'Đang làm việc'),
('NV30', 'Nguyễn Văn Tuấn', '1999-02-03', 'Nam', '0986162164', '137 Nguyễn Thái Sơn, Phường 4, Gò Vấp, TP. HCM', 16000000, 'Đang làm việc'),
('NV31', 'Ngô Thị Thúy ', '1985-03-12', 'Nữ', '0964564589', '514 Đường 3 Tháng 2, P. 14, Q. 10, Tp. Hồ Chí Minh ', 15600000, 'Đang làm việc'),
('NV32', 'Phan Quốc Nghĩa', '1999-11-26', 'Nam', '0659875165', '107 Điện Biên Phủ, Đa Kao, Quận 1, TP.HCM ', 15000000, 'Đang làm việc'),
('NV33', 'Lê Thanh Nguyệt', '1993-01-14', 'Nữ', '065498479', '11 Nguyễn Kiệm, Phường 3, Quận Gò Vấp, TP.HCM ', 15000000, 'Đang làm việc'),
('NV34', 'Phan Hoàng Phúc', '1993-02-28', 'Nam', '0316541236', '161 Bạch đằng, P. 2, Q. Tân Bình, Thành phố Hồ Chí Minh', 16500000, 'Đang làm việc'),
('NV35', 'Trần Hoàng Uyên', '1997-09-09', 'Nữ', '0956498456', '248A Nơ Trang Long, P. 12, Q. Bình Thạnh, TP.HCM', 13000000, 'Đang làm việc'),
('NV36', 'Võ Công Toàn', '1994-02-23', 'Nam', '0783216545', '514 Đường 3 Tháng 2, Phường 14, Quận 1', 18900000, 'Đang làm việc'),
('NV37', 'Đặng Thị Phương ', '1985-12-08', 'Nữ', '0990065465', '31 Hồ Biểu Chánh, Phường 12, Quận Phú Nhuận, TP.HCM', 15000000, 'Đang làm việc'),
('NV38', 'Trần Mai Tư', '1980-12-08', 'Nữ', '0786546597', '1 Tô Hiến Thành, Phường 14, Quận 10, Thành phố Hồ Chí Minh.', 16500000, 'Đang làm việc'),
('NV39', 'Trần  Phương Thắm', '1881-12-01', 'Nữ', '0909616545', '148 Hoàng Hoa Thám, Phường 12, Quận Tân Bình, TP.HCM', 12000000, 'Đang làm việc'),
('NV40', 'Huỳnh Minh Thư', '1987-05-05', 'Nữ', '065465469', '115 Tân Quý, P. Tân Quý, Q.Tân Phú', 10000000, 'Đang làm việc'),
('NV41', 'Nguyễn Xuân Sang', '1999-09-24', 'Nữ', '0903659781', '375 Hai Bà Trưng, Phường 8, Quận 3, TP.HCM', 15000000, 'Đang làm việc'),
('NV42', 'Lê Vân Thùy', '1999-10-08', 'Nữ', '0786269875', '309 Hoàng Diệu, Phường 6, Quận 4, TP. Hồ Chí Minh', 20000000, 'Đang làm việc'),
('NV43', 'Hồ Duy Nến', '1987-02-28', 'Nam', '0986789156', '161 Bạch Đằng, Phường 2, Tân Bình, TPHCM', 18000000, 'Đang làm việc'),
('NV44', 'Kim Thị Thúy Kiều', '1988-05-28', 'Nữ', '0986498212', '1030 Tỉnh Lộ 43, P. Bình Chiểu, Q. Thủ Đức, TP. HCM', 15600000, 'Đang làm việc'),
('NV45', 'Đoàn Quốc Huy', '1983-07-03', 'Nam', '0697498126', '95C, đường Hòa Hưng, Phường 12, Quận 10', 12000000, 'Đang làm việc'),
('NV46', 'Nguyễn Văn Phi', '1995-10-15', 'Nam', '0654988691', '458 Tân Thới Hiệp 02, Khu Phố 3A, P. Tân Thới Hiệp, Q. 12, TP. HCM', 15000000, 'Đang làm việc'),
('NV47', 'Lý Vĩ Hào', '1995-10-23', 'Nam', '0659875196', '644 Kinh Dương Vương, An Lạc, Bình Tân, TP.HCM', 15000000, 'Đang nghỉ việc'),
('NV48', 'Nguyễn Ngọc Giàu', '1993-08-02', 'Nữ', '0986546214', '596 Nguyễn Chí Thanh, Phường 7, Quận 11, TP. HCM', 1000000, 'Đang nghỉ việc'),
('NV49', 'Nguyễn Văn Vũ', '1999-02-03', 'Nam', '0986162164', '137 Nguyễn Thái Sơn, Phường 4, Gò Vấp, TP. HCM', 16000000, 'Đang nghỉ việc'),
('NV50', 'Ngô Lê Gia  Liễu', '1985-03-12', 'Nữ', '0964564589', '514 Đường 3 Tháng 2, P. 14, Q. 10, Tp. Hồ Chí Minh', 15600000, 'Đang nghỉ việc'),
('NV51', 'Trần Tuấn Nghĩa', '1999-11-26', 'Nam', '0659875165', '107 Điện Biên Phủ, Đa Kao, Quận 1, TP.HCM', 15000000, 'Đang nghỉ việc'),
('NV52', 'Lê Nguyệt Nhi', '1993-01-14', 'Nữ', '065498479', '11 Nguyễn Kiệm, Phường 3, Quận Gò Vấp, TP.HCM', 15000000, 'Đang nghỉ việc'),
('NV53', 'Vũ Văn Thiê', '1993-02-28', 'Nam', '0316541236', '161 Bạch đằng, P. 2, Q. Tân Bình, Thành phố Hồ Chí Minh', 16500000, 'Đang nghỉ việc'),
('NV54', 'Trần Quốc Toàn', '1997-09-09', 'Nữ', '0956498456', '248A Nơ Trang Long, P. 12, Q. Bình Thạnh, TP.HCM', 13000000, 'Đang nghỉ việc'),
('NV55', 'Võ Chí Trung', '1994-02-23', 'Nam', '0783216545', '514 Đường 3 Tháng 2, Phường 14, Quận 1', 18900000, 'Đang nghỉ việc'),
('NV56', 'Nguyễnị Phương Thảo', '1985-12-08', 'Nữ', '0990065465', '31 Hồ Biểu Chánh, Phường 12, Quận Phú Nhuận, TP.HCM', 15000000, 'Đang nghỉ việc');

-- --------------------------------------------------------

--
-- Table structure for table `phanbonvdoan`
--

CREATE TABLE `phanbonvdoan` (
  `MaNhanvien` char(10) NOT NULL,
  `MaDoan` char(10) NOT NULL,
  `NhiemVu` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `phanbonvdoan`
--

INSERT INTO `phanbonvdoan` (`MaNhanvien`, `MaDoan`, `NhiemVu`) VALUES
('NV01', 'DOAN01', 'Trưởng đoàn'),
('NV02', 'DOAN01', 'Phó đoàn'),
('NV03', 'DOAN02', 'Trưởng đoàn'),
('NV04', 'DOAN02', 'Phó đoàn'),
('NV05', 'DOAN03', 'Trưởng đoàn'),
('NV06', 'DOAN03', 'Phó đoàn'),
('NV07', 'DOAN04', 'Trưởng đoàn'),
('NV08', 'DOAN04', 'Phó đoàn'),
('NV09', 'DOAN05', 'Trưởng đoàn'),
('NV10', 'DOAN05', 'Phó đoàn'),
('NV11', 'DOAN06', 'Trưởng đoàn'),
('NV12', 'DOAN06', 'Phó đoàn'),
('NV13', 'DOAN07', 'Trưởng đoàn'),
('NV14', 'DOAN07', 'Phó đoàn'),
('NV15', 'DOAN08', 'Trưởng đoàn'),
('NV16', 'DOAN08', 'Phó đoàn'),
('NV17', 'DOAN09', 'Trưởng đoàn'),
('NV18', 'DOAN09', 'Phó đoàn'),
('NV19', 'DOAN10', 'Trưởng đoàn'),
('NV20', 'DOAN10', 'Phó đoàn'),
('NV21', 'DOAN11', 'Trưởng đoàn'),
('NV22', 'DOAN11', 'Phó đoàn'),
('NV23', 'DOAN12', 'Trưởng đoàn'),
('NV24', 'DOAN12', 'Phó đoàn'),
('NV25', 'DOAN13', 'Trưởng đoàn'),
('NV26', 'DOAN13', 'Phó đoàn'),
('NV27', 'DOAN14', 'Trưởng đoàn'),
('NV28', 'DOAN14', 'Phó đoàn'),
('NV29', 'DOAN15', 'Trưởng đoàn'),
('NV30', 'DOAN15', 'Phó đoàn'),
('NV31', 'DOAN16', 'Trưởng đoàn'),
('NV32', 'DOAN16', 'Phó đoàn'),
('NV33', 'DOAN17', 'Trưởng đoàn'),
('NV34', 'DOAN17', 'Phó đoàn'),
('NV35', 'DOAN18', 'Trưởng đoàn'),
('NV36', 'DOAN18', 'Phó đoàn'),
('NV37', 'DOAN19', 'Trưởng đoàn'),
('NV38', 'DOAN19', 'Phó đoàn'),
('NV39', 'DOAN20', 'Trưởng đoàn'),
('NV40', 'DOAN20', 'Phó đoàn'),
('NV41', 'DOAN21', 'Trưởng đoàn'),
('NV42', 'DOAN21', 'Phó đoàn'),
('NV43', 'DOAN22', 'Trưởng đoàn'),
('NV44', 'DOAN22', 'Phó đoàn'),
('NV45', 'DOAN23', 'Trưởng đoàn'),
('NV46', 'DOAN23', 'Phó đoàn');

-- --------------------------------------------------------

--
-- Table structure for table `thamquan`
--

CREATE TABLE `thamquan` (
  `MaTour` char(10) NOT NULL,
  `MaDiaDiem` char(10) NOT NULL,
  `ThuTu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `thamquan`
--

INSERT INTO `thamquan` (`MaTour`, `MaDiaDiem`, `ThuTu`) VALUES
('T01', 'DD01', 1),
('T01', 'DD02', 2),
('T01', 'DD03', 3),
('T02', 'DD04', 1),
('T02', 'DD05', 2),
('T02', 'DD06', 3),
('T03', 'DD07', 1),
('T03', 'DD08', 2),
('T03', 'DD09', 3),
('T04', 'DD10', 1),
('T04', 'DD11', 2),
('T04', 'DD12', 3),
('T05', 'DD13', 1),
('T05', 'DD14', 2),
('T05', 'DD15', 3),
('T06', 'DD16', 1),
('T06', 'DD17', 2),
('T06', 'DD18', 3),
('T07', 'DD19', 1),
('T07', 'DD20', 2),
('T08', 'DD21', 1),
('T08', 'DD22', 2),
('T08', 'DD23', 3),
('T08', 'DD24', 4),
('T08', 'DD25', 5),
('T08', 'DD26', 6),
('T09', 'DD27', 1),
('T09', 'DD28', 2),
('T09', 'DD29', 3),
('T09', 'DD30', 4),
('T10', 'DD31', 1),
('T10', 'DD32', 2),
('T10', 'DD33', 3),
('T11', 'DD34', 1),
('T11', 'DD35', 2),
('T11', 'DD36', 3),
('T11', 'DD37', 4),
('T12', 'DD38', 1),
('T12', 'DD39', 2),
('T12', 'DD40', 3),
('T12', 'DD41', 4),
('T12', 'DD42', 5),
('T13', 'DD43', 1),
('T13', 'DD44', 2),
('T13', 'DD45', 3),
('T13', 'DD46', 4),
('T13', 'DD47', 5),
('T13', 'DD48', 6),
('T13', 'DD49', 7),
('T13', 'DD50', 8),
('T13', 'DD51', 9),
('T14', 'DD52', 1),
('T14', 'DD53', 2),
('T14', 'DD54', 3),
('T14', 'DD55', 4),
('T14', 'DD56', 5),
('T14', 'DD57', 6),
('T15', 'DD58', 1),
('T15', 'DD59', 2),
('T15', 'DD60', 3),
('T15', 'DD61', 4),
('T15', 'DD62', 5),
('T15', 'DD63', 6),
('T15', 'DD64', 7),
('T15', 'DD65', 8),
('T16', 'DD66', 1),
('T16', 'DD67', 2),
('T16', 'DD68', 3),
('T16', 'DD69', 4),
('T16', 'DD70', 5),
('T16', 'DD71', 6),
('T16', 'DD72', 7),
('T17', 'DD73', 1),
('T17', 'DD74', 2),
('T17', 'DD75', 3),
('T17', 'DD76', 4),
('T17', 'DD77', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tourdulich`
--

CREATE TABLE `tourdulich` (
  `MaTour` char(10) NOT NULL,
  `TenTour` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `DacDiem` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `MaLoaiHinh` char(10) DEFAULT NULL,
  `MaGiaTour` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tourdulich`
--

INSERT INTO `tourdulich` (`MaTour`, `TenTour`, `DacDiem`, `MaLoaiHinh`, `MaGiaTour`) VALUES
('T01', 'Sài Gòn - Đà Nẵng', '- Chiêm ngưỡng khung cảnh thiên nhiên ngoạn mục trên Cây Cầu Vàng.', 'LH01', 'GT01'),
('T02', 'Sài Gòn -  Hạ Long', '- Tham quan cửa khẩu Hoành Mô, “check-in” cột mốc 1317. ', 'LH02', 'GT02'),
('T03', 'Sài Gòn - Cà Mau', 'Đến Đất Mũi, đoàn tham quan Công viên Văn Hóa Mũi Cà Mau - tọa lạc trong khu dự trữ sinh quyển thế giới Mũi Cà Mau được UNESCO công nhận vào tháng 5/2009.', 'LH03', 'GT03'),
('T04', 'Sài Gòn - Đà Lạt', '- Tham quan Khu Du Lịch Trang Trại Rau và Hoa (Làng hoa Vạn Thành), nằm trải rộng cả một thung lũng với bốn bề là rau và hoa đẹp tuyệt vời.', 'LH04', 'GT04'),
('T05', 'Sài Gòn - Huế', '- Hội An với Chùa Cầu Nhật Bản, Nhà Cổ hàng trăm năm tuổi, Hội Quán Phước Kiến & Xưởng thủ công mỹ nghệ.', 'LH05', 'GT05'),
('T06', 'Sài Gòn - Nha Trang', '- Tham quan thành phố biển Nha Trang xinh đẹp\r\n- Tới khu du lịch Hòn Lao - đảo Khỉ, thư giãn, xem xiếc hoặc thử các môn thể thao bãi biển', 'LH03', 'GT06'),
('T07', 'TP.HCM - Củ Chi', '- Du Lịch Xanh Cùng Lữ Hành Saigontourist\r\n- Tham quan Khu Di Tích Địa Đạo Củ Chi – Đền Bến Dược', 'LH05', 'GT07'),
('T08', 'TP. Hồ Chí Minh - Cần Giờ ', '- Du Lịch Xanh Cùng Lữ Hành Saigontourist\r\n- Tham quan Chiến Khu Rừng Sác – Khu Du Lịch Sinh Thái Vàm Sát', 'LH05', 'GT08'),
('T09', 'TP. Hồ Chí Minh - Long An', '- Tham quan KDL Cánh Đồng Bất Tận – Khu bảo tồn và phát triển dược liệu Đồng Tháp Mười\r\n- Khám khá khu vực Rừng Tràm Gió nguyên sinh -  len lỏi giữa hệ thống kênh rạch với thảm thực vật xanh mướt; những cây tràm gió gần trăm năm tuổi được bao bọc bằng dây leo bòng bong, những đầm hoa sen, rặng hoa súng nở rộ thơm ngát', 'LH04', 'GT09'),
('T10', 'TP. Hồ Chí Minh - Tây Ninh ', '- Tham quan Củ Chi – vùng đất được phong tặng danh hiệu “đất thép thành đồng”\r\n- Trải nghiệm tuyến cáp treo Vân Sơn, được chứng nhận là hệ thống nhà ga lớn nhất thế giới bởi tổ chức kỉ lục Guinness, chỉ mất 4 phút', 'LH04', 'GT10'),
('T11', 'TP. Hồ Chí Minh - Vĩnh Long ', '- Trải nghiệm một đêm tại homestay Út Trinh\r\n- Tham quan các làng nghề :đan lục bình, làm bánh tráng rế, may nón đệm/cối..\r\n- Tham gia tát mương bắt cá', 'LH03', 'GT11'),
('T12', 'TP. Hồ Chí Minh - Cần Thơ', '- Du thuyền trên sông Mekong ngắm cảnh bốn cù lao Long, Lân Qui, Phụng\r\n- Tham quan chợ nổi Cái Răng - một chợ nổi lớn của Đồng Bằng sông Cửu Long\r\n- Viếng Thiền viện Trúc Lâm Phương Nam', 'LH01', 'GT12'),
('T13', 'TP. Hồ Chí Minh - Côn Đảo', '- Đến với Côn Đảo (hay với các tên khác như Côn Sơn, Côn Lôn) đã gợi cho người nghe hình ảnh mịt mù của một khu vực xa xôi, mờ mịt, có lúc được ví như địa ngục trần gian nổi tiếng từ thời Pháp thuộc đến giữa năm 1975. \r\n- Tham quan Cầu tàu 914, Trại Phú Hải, Chuồng Cọp Pháp - Mỹ, Mũi Cá Mập, Đỉnh Tình Yêu.', 'LH05', 'GT13'),
('T14', 'TP. Hồ Chí Minh - Phú Quốc', '- Tham quan suối Tranh - quý khách có thể đi dạo trong rừng, thư giãn, tắm suối (suối đặc biệt nhiều nước trong mùa hè)\r\n- Trải nghiệm “Cáp treo 3 dây vượt biển dài nhất thế giới tại Hòn Thơm”', 'LH03', 'GT14'),
('T15', 'TP. Hồ Chí Minh - Buôn Ma Thuột', '- Trải nghiệm cảm giác thú vị khi ngồi trên lưng những Chú voi Bản Đôn vượt Sông Serepok, thử cảm giác phiêu lưu khi chinh phục hệ thống cầu treo dài hơn 1km được bắt trên những sàn si.\r\n- Viếng thăm Chùa Sắc Tứ Khải Đoan. Ngôi Chùa đầu tiên đặt nền móng cho Phật giáo tại Tây Nguyên với kiến trúc độc đáo.\r\n- Quý khách tự do tham quan khu du lịch Sinh Thái KoTam, ngắm vườn hoa, khu bến nước,….', 'LH04', 'GT15'),
('T16', 'TP. Hồ Chí Minh - Ninh Bình', '- Hành trình tham quan kỉ niệm 130 năm chủ tịch Hồ Chí Minh\r\n- Tham quan quê Ngoại – Làng Hoàng Trù là nơi đã sinh thành ra Bác Hồ\r\n- Tham quan và viếng khu du lịch tâm linh Chùa Tam Chúc tại Hà Nam – Một trong những quần thể tâm linh lớn nhất thế giới với cảnh sắc non nước tuyệt đẹp. \r\n- Xem phim về cuộc đời của chủ tịch Hồ Chí Minh', 'LH03', 'GT16'),
('T17', 'TP. Hồ Chí Minh - Sapa', '- Du lịch Sapa: tham quan nhà thờ Đá, núi Hàm Rồng, ...\r\n- Đặc biệt quý khách có cơ hội chinh phục đình Fasipan', 'LH09', 'GT17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chiphi`
--
ALTER TABLE `chiphi`
  ADD PRIMARY KEY (`MaChiPhi`),
  ADD KEY `fk_CP_MaDoan` (`MaDoan`),
  ADD KEY `fk_CP_MaLoaiChiPhi` (`MaLoaiChiPhi`);

--
-- Indexes for table `chitietdoan`
--
ALTER TABLE `chitietdoan`
  ADD PRIMARY KEY (`MaDoan`,`MaKhachHang`),
  ADD KEY `fk_CTD_MaKhachHang` (`MaKhachHang`);

--
-- Indexes for table `diadiem`
--
ALTER TABLE `diadiem`
  ADD PRIMARY KEY (`MaDiaDiem`);

--
-- Indexes for table `doandulich`
--
ALTER TABLE `doandulich`
  ADD PRIMARY KEY (`MaDoan`),
  ADD KEY `fk_DDL_MaTour` (`MaTour`);

--
-- Indexes for table `giatour`
--
ALTER TABLE `giatour`
  ADD PRIMARY KEY (`MaGiaTour`),
  ADD KEY `fk_GT_MaTour` (`MaTour`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`MaKhachHang`);

--
-- Indexes for table `loaichiphi`
--
ALTER TABLE `loaichiphi`
  ADD PRIMARY KEY (`MaLoaiChiPhi`);

--
-- Indexes for table `loaihinhdulich`
--
ALTER TABLE `loaihinhdulich`
  ADD PRIMARY KEY (`MaLoaiHinh`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`MaNV`);

--
-- Indexes for table `phanbonvdoan`
--
ALTER TABLE `phanbonvdoan`
  ADD PRIMARY KEY (`MaNhanvien`,`MaDoan`),
  ADD KEY `fk_PBNV_MaDoan` (`MaDoan`);

--
-- Indexes for table `thamquan`
--
ALTER TABLE `thamquan`
  ADD PRIMARY KEY (`MaTour`,`MaDiaDiem`),
  ADD KEY `fk_TQ_MaDiaDiem` (`MaDiaDiem`);

--
-- Indexes for table `tourdulich`
--
ALTER TABLE `tourdulich`
  ADD PRIMARY KEY (`MaTour`),
  ADD KEY `fk_TDL_MaLoaiHinh` (`MaLoaiHinh`),
  ADD KEY `magiatour_idx` (`MaGiaTour`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chitietdoan`
--
ALTER TABLE `chitietdoan`
  ADD CONSTRAINT `fk_CTD_MaDoan` FOREIGN KEY (`MaDoan`) REFERENCES `doandulich` (`MaDoan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_CTD_MaKhachHang` FOREIGN KEY (`MaKhachHang`) REFERENCES `khachhang` (`MaKhachHang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doandulich`
--
ALTER TABLE `doandulich`
  ADD CONSTRAINT `fk_DDL_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `giatour`
--
ALTER TABLE `giatour`
  ADD CONSTRAINT `fk_GT_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `thamquan`
--
ALTER TABLE `thamquan`
  ADD CONSTRAINT `fk_TQ_MaDiaDiem` FOREIGN KEY (`MaDiaDiem`) REFERENCES `diadiem` (`MaDiaDiem`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_TQ_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tourdulich`
--
ALTER TABLE `tourdulich`
  ADD CONSTRAINT `fk_TDL_MaLoaiHinh` FOREIGN KEY (`MaLoaiHinh`) REFERENCES `loaihinhdulich` (`MaLoaiHinh`),
  ADD CONSTRAINT `magiatour` FOREIGN KEY (`MaGiaTour`) REFERENCES `giatour` (`MaGiaTour`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
