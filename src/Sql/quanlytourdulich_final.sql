-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2021 at 06:56 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quanlytourdulich`
--

-- --------------------------------------------------------

--
-- Table structure for table `chiphi`
--

CREATE TABLE `chiphi` (
  `MaChiPhi` char(10) NOT NULL,
  `MaDoan` char(10) DEFAULT NULL,
  `MaLoaiChiPhi` char(10) DEFAULT NULL,
  `SoTien` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chiphi`
--

INSERT INTO `chiphi` (`MaChiPhi`, `MaDoan`, `MaLoaiChiPhi`, `SoTien`) VALUES
('CP01', 'DOAN17', 'LCP02', 5000000),
('CP02', 'DOAN17', 'LCP04', 2000000),
('CP03', 'DOAN17', 'LCP05', 3000000);

-- --------------------------------------------------------

--
-- Table structure for table `chitietdoan`
--

CREATE TABLE `chitietdoan` (
  `MaDoan` char(10) NOT NULL,
  `MaKhachHang` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chitietdoan`
--

INSERT INTO `chitietdoan` (`MaDoan`, `MaKhachHang`) VALUES
('DOAN01', 'KH01'),
('DOAN01', 'KH02'),
('DOAN01', 'KH06'),
('DOAN01', 'KH08'),
('DOAN01', 'KH10'),
('DOAN02', 'KH03'),
('DOAN02', 'KH04'),
('DOAN02', 'KH05'),
('DOAN02', 'KH07'),
('DOAN02', 'KH09'),
('DOAN02', 'KH11'),
('DOAN16', 'KH12'),
('DOAN16', 'KH13'),
('DOAN16', 'KH14'),
('DOAN16', 'KH15'),
('DOAN16', 'KH16'),
('DOAN16', 'KH17'),
('DOAN16', 'KH18'),
('DOAN16', 'KH19'),
('DOAN16', 'KH20'),
('DOAN17', 'KH21'),
('DOAN17', 'KH22'),
('DOAN17', 'KH23'),
('DOAN17', 'KH24'),
('DOAN17', 'KH25');

-- --------------------------------------------------------

--
-- Table structure for table `diadiem`
--

CREATE TABLE `diadiem` (
  `MaDiaDiem` char(10) NOT NULL,
  `TenDiaDiem` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `diadiem`
--

INSERT INTO `diadiem` (`MaDiaDiem`, `TenDiaDiem`) VALUES
('DD01', 'Cầu Sông Hàn'),
('DD02', 'Bảo tàng tranh 3D'),
('DD03', 'Phố cổ Hội An'),
('DD04', 'Vịnh Hạ Long'),
('DD05', 'Chợ đêm Hạ Long'),
('DD06', 'Công viên hoa Hạ Long'),
('DD07', 'Vườn dâu Cái Tàu'),
('DD08', 'Hòn Đá Bạc'),
('DD09', 'Rừng U Minh Hạ'),
('DD10', 'Quảng trường Lâm Viên'),
('DD11', 'Núi Lang Biang'),
('DD12', 'Thung Lũng Tình Yêu'),
('DD13', 'Chùa Thiên Mụ'),
('DD14', 'Sông Hương'),
('DD15', 'Vườn Quốc Gia Bạch Mã'),
('DD16', 'Tháp Bà Ponagar'),
('DD17', 'Hòn Lao – Đảo Khỉ'),
('DD18', 'Đảo Vinpearland'),
('DD19', 'Khu di tích Địa đạo Bến Dược'),
('DD20', 'Đền Bến Dược'),
('DD21', 'Lâm Viên Cần Giờ'),
('DD22', ''),
('DD23', 'Khu du lịch Dần Xây'),
('DD24', 'Khu du lịch Sinh Thái Vàm Sát'),
('DD25', 'Khu bảo tồn chim'),
('DD26', 'Tháp Tang Bồng'),
('DD27', 'Làng cổ Phước Lộc Thọ'),
('DD28', 'KDL Cánh Đồng Bất Tận'),
('DD29', 'Khu vực Rừng Tràm Gió'),
('DD30', 'Khu vực Đồng Tháp Mười'),
('DD31', 'Núi Bà Đen'),
('DD32', 'Thung lũng Ma Thiên Lãnh'),
('DD33', 'Chùa Hang'),
('DD34', 'Bến tàu du lịch Vĩnh Long'),
('DD35', 'Cù lao Minh'),
('DD36', 'Làng nghề'),
('DD37', 'Chợ Nổi Cái Bè'),
('DD38', 'Chùa Vĩnh Tràng'),
('DD39', 'Ngắm cảnh bốn cù lao Long'),
('DD40', 'Tham quan lò kẹo dừa'),
('DD41', 'Chợ nổi Cái Răng'),
('DD42', 'Viếng Thiền viện Trúc Lâm Phương Nam'),
('DD43', 'Làng Cỏ Ống'),
('DD44', 'Nghĩa địa đầu tiên tại Côn Đảo'),
('DD45', 'Dinh chúa đảo'),
('DD46', 'Trại tù Phú Hải'),
('DD47', 'Cầu tàu 914'),
('DD48', 'Viếng nghĩa trang Hàng Dương'),
('DD49', 'Khu Chuồng Cọp Pháp - Mỹ'),
('DD50', 'Cảng Bến Đầm'),
('DD51', 'Bãi Nhát'),
('DD52', 'Tham quan Dinh Cậu'),
('DD53', 'Chợ đêm Dương Đông'),
('DD54', 'Khám phá Hòn Thơm'),
('DD55', 'Đảo Hòn Dừa'),
('DD56', 'Khu du lịch Sun World'),
('DD57', 'Khu di tích lịch sử'),
('DD58', 'Khu Du Lịch Làng Cafe Trung Nguyên'),
('DD59', 'Khu Du Lịch Buôn Đôn'),
('DD60', 'Sông Serepok'),
('DD61', 'Khu lăng mộ vua săn voi Khun Yu Nốp'),
('DD62', 'CHÙA SẮC TỨ KHẢI ĐOAN'),
('DD63', 'BẢO TÀNG ĐAKLAK'),
('DD64', 'KHU DU LỊCH SINH THÁI KOTAM  '),
('DD65', 'khu du lịch sinh thái Ko Tam'),
('DD66', 'Làng Hoàng Trù'),
('DD67', 'Mộ 13 nữ thanh niên xung phong Truông Bồn'),
('DD68', 'Viếng đền thờ Bà Triệu'),
('DD69', 'Khu du lịch sinh thái Tràng An'),
('DD70', 'Tâm linh Chùa Tam Chúc'),
('DD71', 'Đá Chông – K9'),
('DD72', 'Quảng Trường Ba Đình'),
('DD73', 'Nhà Thờ Đá'),
('DD74', 'Hồ Sapa'),
('DD75', 'Tham quan bản Cát Cát'),
('DD76', 'Khu du lịch núi Hàm Rồng'),
('DD77', 'CHINH PHỤC FANSIPAN');

-- --------------------------------------------------------

--
-- Table structure for table `doandulich`
--

CREATE TABLE `doandulich` (
  `MaDoan` char(10) NOT NULL,
  `MaTour` char(10) DEFAULT NULL,
  `NgayKhoiHanh` date DEFAULT NULL,
  `NgayKetThuc` date DEFAULT NULL,
  `DoanhThu` bigint(20) DEFAULT NULL,
  `HanhTrinh` varchar(10000) CHARACTER SET utf8 DEFAULT NULL,
  `KhachSan` varchar(200) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doandulich`
--

INSERT INTO `doandulich` (`MaDoan`, `MaTour`, `NgayKhoiHanh`, `NgayKetThuc`, `DoanhThu`, `HanhTrinh`, `KhachSan`) VALUES
('DOAN01', 'T01', '2021-10-20', '2021-10-27', 2000000, '#', 'Phúc Thành Luxury'),
('DOAN02', 'T01', '2021-10-20', '2021-10-27', 2000000, '#', 'Yến Vy Hotel'),
('DOAN03', 'T01', '2021-10-20', '2021-10-27', 2000000, '#', 'Robin Hotel'),
('DOAN04', 'T01', '2021-10-20', '2021-10-27', 2000000, '#', 'Sen Boutique Hotel'),
('DOAN05', 'T01', '2021-10-20', '2021-10-27', 2000000, '#', 'Mai Boutique Villa'),
('DOAN06', 'T02', '2021-11-01', '2021-11-07', 2000000, '#', 'The Confetti Hotel'),
('DOAN07', 'T02', '2021-11-01', '2021-11-07', 2000000, '#', 'Draha Halong Hotel'),
('DOAN08', 'T02', '2021-11-01', '2021-11-07', 2000000, NULL, 'The Marine Hotel'),
('DOAN09', 'T03', '2021-11-01', '2021-11-07', 2000000, NULL, 'Quốc Tế Hotel'),
('DOAN10', 'T03', '2021-11-01', '2021-11-07', 2000000, NULL, 'Thanh Trúc Hotel'),
('DOAN11', 'T04', '2021-11-01', '2021-11-07', 2000000, NULL, 'Kings Hotel'),
('DOAN12', 'T04', '2021-11-01', '2021-11-07', 2000000, NULL, 'Mỹ Hy Hotel'),
('DOAN13', 'T04', '2021-11-01', '2021-11-07', 2000000, NULL, 'Dalat Eco Hotel'),
('DOAN14', 'T05', '2021-11-01', '2021-11-07', 1200000, NULL, 'Nice Huế Hotel'),
('DOAN15', 'T05', '2021-11-01', '2021-11-07', 4800000, NULL, 'Hotel La Perle'),
('DOAN16', 'T06', '2021-11-09', '2021-11-11', 4500000, 'NGÀY 1 : SÂN BAY TÂN SƠN NHẤT - NHA TRANG (Ăn trưa, tối)\r\n- 15h30 hướng dẫn viên Saigontourist đón đoàn tại.......nội thành phố HCM ra sân sân bay làm thủ tục chuyến bay đi Cam Ranh. Sau hơn 1,5 tiếng đồng hồ Quý khách đến sân bay Cam Ranh, xe và hướng dẫn đón quý khách đi vào trung tâm thành phố Nha Trang dùng bữa tối tại nhà hàng.\r\n- Tối tự do đi chợ đêm Nha Trang\r\nNGÀY 2 :  THAM QUAN NHA TRANG Ăn sáng, trưa, tối)\r\n- 6h30 đoàn ăn sáng. Tiếp đó đoàn đi tham quan công trình kiến trúc tâm linh đầy ấn tượng Tháp Bà Ponagar\r\n- Tiếp đó: đoàn tham quan Hòn Lao – Đảo Khỉ. - một trong những khu du lịch sinh thái sinh động nhất tại Nha Trang\r\n- 11h30 đoàn ăn trưa và về lại khách sạn chuẩn bị hành trang lên cáp treo ra đảo Vinpearland\r\n- 18h00 dùng bữa tối trên đảo và tiếp tục khám phá vui chơi trong ánh đèn lung linh về đêm rất đẹp. 19h00 Quý khách không quên xem tiết mục nhạc nước hiện đại nhất Việt Nam\r\n- 20h00 đoàn lên cáp treo về đất liền. Nghỉ đêm tại Tp. Nha Trang.\r\nNGÀY 3 : NHA TRANG – SÂN BAY CAM RANH - TP VINH (Ăn sáng)\r\n- 6h30 đoàn ăn sáng và trả phòng khách sạn. 7h30 đoàn đi ra sân bay làm thủ tục lên máy bay chuyến VJ lúc 10h35 về HCM. Chia tay đoàn kết thúc chương trình\r\n', 'Rex Hotel & Apartment'),
('DOAN17', 'T07', '2021-11-16', '2021-11-16', 8000000, '- 7h00  Xe và hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.\r\n- 7h30  Đoàn khởi hành đi Củ Chi – vùng đất được phong tặng danh hiệu “đất thép thành đồng” nhờ vào những chiến công hiển hách trong thời kỳ kháng chiến. Phần ăn sáng sẽ được gửi đến đoàn trên xe.\r\n- 9h – 11h  Đoàn đến thăm khu di tích Địa đạo Bến Dược - công trình quân sự đặc biệt với hệ thống đường ngầm dày đặc trong lòng đất\r\n- 11h – 12h  Đoàn đến viếng Đền Bến Dược – nơi tưởng niệm các Anh Hùng liệt sĩ đã hi sinh trong cuộc kháng chiến chống Pháp và Mỹ.\r\n- 12h – 13h  Đoàn nghỉ ngơi, dùng cơm trưa với các món ăn đặc sản địa phương.\r\n- 13h   Khởi hành trở về trung tâm TP.HCM.\r\n- 14h30 Xe và hướng dẫn viên Lữ Hành Saigontourist  đưa đoàn về đến điểm đón ban đầu.\r\nKết thúc chương trình tham quan.\r\n', NULL),
('DOAN18', 'T08', '2021-11-30', '2021-11-30', 7000000, '- 6h00  Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.\r\n- 6h30  Đoàn khởi hành đi Cần Giờ - khu rừng ngập mặn lớn nhất và cũng là lá phổi xanh của Thành phố Hồ Chí Minh\r\n- 8h30 – 10h  Đoàn đến Lâm Viên Cần Giờ, quý khách xuống canô đi len lỏi trong rừng đước, thăm Khu Căn Cứ Cách Mạng Rừng Sác.\r\n- 10h – 11h   Đoàn di chuyển đến Khu du lịch Dần Xây\r\n- 11h – 12h30   Đoàn lên canô đến tham quan Khu du lịch Sinh Thái Vàm Sát:\r\n+ Ngắm cảnh quan sông nước trong lõi rừng ngập mặn xanh mát. \r\n+ Đi xuồng chèo len lỏi trong rừng đước, tham quan Khu bảo tồn dơi nghệ, câu cua.\r\n+ Tham quan Khu bảo tồn chim (đặc biệt vào mùa chim làm tổ từ tháng 4-10 AL)\r\n+ Tham quan, trải nghiệm cảm giác mạo hiểm với “Du thuyền câu sấu”\r\n+ Chinh phục Tháp Tang Bồng chiêm ngưỡng toàn cảnh rừng ngập mặn từ độ cao 26m.\r\n- 12h30 – 13h30   Đoàn nghỉ ngơi, dùng cơm trưa với các món ăn đặc trưng của điạ phương.\r\n- 13h30 - 14h30  Canô đưa đoàn về đến bến tàu Dần Xây. Khởi hành trở về trung tâm TP.HCM.\r\n- 16h30 Xe và hướng dẫn viên Lữ Hành Saigontourist  đưa đoàn về đến điểm đón ban đầu.\r\nKết thúc chương trình tham quan.', NULL),
('DOAN19', 'T09', '2021-11-25', '2021-11-25', 1000000, '- 5h30 Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 1 Nguyễn Chí Thanh, Quận 5. \r\n- 6h Đoàn khởi hành đi Mộc Hóa – một huyện biên giới của tỉnh Long An.\r\n- 7h  Đoàn dừng chân ăn sáng và tham quan Làng cổ Phước Lộc Thọ\r\n- 8h – 9h30 Đoàn đi chuyển đến bến tàu của KDL Cánh Đồng Bất Tận – Khu bảo tồn và phát triển dược liệu Đồng Tháp Mười. Tắc ráng đưa đoàn vào khu vực trung tâm, đoàn nghỉ ngơi và dùng nước vối giải nhiệt...\r\n- 9h30 – 11h30 Đoàn tiếp tục di chuyển bằng tắc ráng, khám khá khu vực Rừng Tràm Gió nguyên sinh -  len lỏi giữa hệ thống kênh rạch với thảm thực vật xanh mướt\r\n+ Cánh đồng cỏ bàng – phim trường của bộ phim nổi tiếng Cánh đồng bất tận. Quý khách có thể đi bộ trên con đường gỗ giữa cánh đồng cỏ bàng ngập nước và có những tấm ảnh “check-in” thú vị ...\r\n+ Khu trồng dược liệu Sao Mai –  nơi bảo tồn nguồn gene các loài cây dược liệu quý hiếm và đặc thù của khu vực Đồng Tháp Mười. Đoàn thưởng thức món ăn dân dã “khoai mì nấu nước cốt dừa”, uống trà lá sen trắng được trồng và chế biến ngay tại khu bảo tồn...\r\n- 11h30 – 12h Đoàn trở về khu vực trung tâm, đến tham quan:\r\n+ Khu đền thờ – nơi thờ phụng 2 vị tổ sư của ngành Y học cổ truyền dân tộc: Tuệ Tĩnh, Hải Thượng Lãn Ông và chú Ba Bé\r\n+ Nhà máy Hoa Mộc Tràm -  đoàn tìm hiểu quy trình điều chế và sản xuất các sản phẩm dược liệu từ thiên nhiên\r\n- 12h – 13h30 Đoàn nghỉ ngơi, dùng cơm trưa với các món ăn dân dã, đặc trưng từ nguồn thực phẩm thiên nhiên xanh – sạch của điạ phương, mùa nào thức nấy ...\r\n- 13h30 – 15h30 Đoàn cùng tham gia các hoạt động trải nghiệm:\r\n+ Liệu pháp “tắm rừng” –  quý khách dạo bộ hoặc đi xe đạp tham quan và đắm mình vào không gian rừng trong lành và thư thái, giúp cơ thể thư giãn, tăng cường hệ miễn dịch...\r\n+ Chèo xuồng Kayak hoặc composite trên dòng kênh dài hơn 3km soi bóng những hàng cây xanh mát, tận hưởng khung cảnh nên thơ với bầu không khí trong lành, cảm giác an nhiên thư thả ...\r\n+  Bơi lội, lặn ngắm thảm thực vật tại hồ bơi dài 1km, trong làn nước mưa tự nhiên trong vắt, thư giãn giữa đất trời thiên nhiên ...\r\n- 16h Đoàn trở về khu vực trung tâm khu du lịch. Tắc ráng đưa đoàn về lại bến tàu.\r\n- Quý khách, khởi hành trở về TP.HCM theo quốc lộ 62 và đường cao tốcTrung Lương. Trên đường, đoàn dừng chân nghỉ ngơi và mua sắm đặc sản địa phương ...\r\n18h Xe và hướng dẫn viên Lữ Hành Saigontourist  đưa đoàn về đến điểm đón ban đầu.\r\nKết thúc chương trình tham quan.\r\n', NULL),
('DOAN20', 'T10', '2021-11-22', '2021-11-22', 4000000, '- 06h00 Xe và Hướng dẫn viên của Lữ Hành Saigontourist đón đoàn tại 45 Lê Thánh Tôn, Quận 1.Đoàn khởi hành đi Núi Bà Đen. Quý khách sẽ đi tuyến cáp treo Vân Sơn, được chứng nhận là hệ thống nhà ga lớn nhất thế giới bởi tổ chức kỉ lục Guinness\r\n- Quý khách sẽ lướt cùng cáp treo qua những khung cảnh núi non kỳ vĩ của quần thể núi Bà Đen và thung lũng Ma Thiên Lãnh, cảm nhận trọn vẹn vẻ đẹp hoang sơ của thiên nhiên nơi đây với hệ động thực vật đa dạng, phong phú để tiếp tục hành trình chiêm bái quần thể Chùa Hang, chùa Bà có tuổi đời hơn 300 năm để tham quan và cầu nguyện cho mọi điều may mắn đến với quý du khách và gia đình tại vùng đất linh thiêng này.\r\n Khởi hành trở về trung tâm TP.HCM\r\nXe và hướng dẫn viên Lữ Hành Saigontourist  đưa đoàn về đến điểm đón ban đầu.', NULL),
('DOAN21', 'T11', '2021-12-08', '2021-12-09', 3000000, 'NGÀY 01: TP. HỒ CHÍ MINH - VĨNH LONG (Ăn sáng, trưa, chiều)\r\n- 07h00 Hướng dẫn viên và xe của Lữ hành Saigontourist sẽ đón khách tại điểm hẹn trong Tp.HCM& đưa đoàn đến nhà hàng của Khách sạn Continental để thưởng thức bữa điểm tâm sáng\r\n- 10h00 Khởi hành đi Vĩnh Long bằng bằng đường cao tốc Sài Gòn - Trung Lương. Tại Bến tàu du lịch Vĩnh Long, quý khách lên tàu, xuôi dòng Cổ Chiên, đến với cù lao Minh, đoàn sẽ đến nhà vườn Út Trinh để thưởng thức bữa trưa với các món ngon của miền sông nước. Dùng cơm xong, nhận phòng tại Út Trinh homestay, nghỉ ngơi.\r\n- Buổi chiều, đi bộ đến Làng nghề gần đó, tham quan: lò cốm. kẹo dừa, bánh tráng, nấu rượu... Trở về Homestay Út Trinh, thay quần áo bà ba đen để tham gia tát mương bắt cá… Sau đó chuẩn bị bữa cơm tối cùng gia đình cô Út. Ăn tối với những “chiến lợi phẩm” vừa bắt được!\r\n- Sau bữa ăn tối, đoàn sẽ thưởng thức và giao lưu Đờn ca tài tử. Trải nghiệm một đêm tại Nhà Út Trinh thưởng thức không gian thanh bình dưới những làn gió trong lành, tiếng côn trùng rả rít.\r\nNGÀY 02: VĨNH LONG - TP. HỒ CHÍ  MINH (Ăn sáng, trưa)\r\n- Sau khi ăn sáng, đoàn xuống tàu vượt sông tiền, đến Chợ Nổi Cái Bè. Quý khách tiếp tục xuống tàu đến Cù lao Tân Phong\r\n- Sau khi ăn trưa, tàu đưa đoàn về Bến tàu Vĩnh Long. Quý khách tham quan chợ Vĩnh Long, với nhiều loại trái cây nhiệt đới. Tham quan chùa Vàm Rai - một ngôi chùa hoành tráng, lộng lẫy\r\n- Xe đưa quý khách về điểm đón. Kết thúc chương trình.', 'Minh Khuê'),
('DOAN27', 'T17', '2022-01-01', '2021-12-03', 2000000, 'NGÀY 1 : HẢI PHÒNG – SAPA – KHÁM PHÁ CHỢ ĐÊM SAPA (Ăn trưa, tối)\r\n- 06:45 Quý khách có mặt tại bến xe, lên xe giường nằm khởi hành đi Sapa (dự kiến 07:00).\r\n- Chiều: Đến Sapa, Hướng dẫn viên đón đoàn đưa đi ăn trưa sau đó về khách sạn nhận phòng, nghỉ ngơi. 15h00: Quý khách tự do khám phá Sapa như Nhà Thờ Đá, Hồ Sapa, Mua sắm đồ tại các dãy phố ...18h00: Sau bữa tối, quý khách đi chợ tình SaPa (nếu vào tối thứ 7) - một nét văn hóa đặc sắc của đồng bào dân tộc thiểu số tại vùng núi Tây Bắc và  tự do dạo chơi chợ đêm Sapa, thưởng thức các món nướng đặc sắc vùng cao.\r\nNGÀY 2 : SAPA – CÁT CÁT – HÀM RỒNG (Ăn sáng, trưa, tối)\r\n- Quý khách dùng bữa sáng tại khách sạn . 07h30: HDV và xe đưa đoàn đi tham quan bản Cát Cát, bản của người H’Mông đen, thăm thác nước Cát Cát, thuỷ điện Cát cát nơi có ba con suối gặp nhau tạo thành thung lũng Mường Hoa quý khách chụp ảnh lưu niệm. Ăn trưa.\r\n14h30 HDV đưa quý khách đi tham quan khu du lịch núi Hàm Rồng - hòa mình trong bốn bề của các loài hoa & ngắm nhìn toàn cảnh thị trấn Sapa, xem biểu diễn của những chàng trai cô gái trong điệu khèn, điệu múa đặc trưng của dân tộc  miền Tây Bắc…18h00: Quý khách dùng bữa tối tại nhà hang, sau bữa tối Quý khách tự do dạo chơi và khám phá thị trấn Sapa về đêm.\r\nNGÀY 3 : CHINH PHỤC FANSIPAN– LÀO CAI - HẢI PHÒNG (Ăn sáng, trưa)\r\n- Quý khách dùng bữa sáng tại khách sạn. .HDV đưa Quý khách tới nhà Ga SAPA, quý khách trải nghiệm tàu hỏa leo núi Mường Hoa (TỰ TÚC CHI PHÍ) ngắm nhìn khung cảnh thiên nhiên hùng vĩ của thung lũng Mường Hoa với núi đồi trập trùng. Đến Ga cáp treo, du khách sẽ tiếp tục hành trình khám phá Sun World Fansipan Legend với cáp treo ba dây hiện đại nhất thế giới băng qua dãy Hoàng Liên Sơn, chinh phục đỉnh Fansipan - nóc nhà Đông Dương và chiêm bái quần thể văn hóa tâm linh trên đỉnh Fansipan. (TỰ TÚC CHI PHÍ)\r\n- 11h30: Quý khách về nhà hàng dùng bữa trưa, trả phòng khách sạn sau đó quý khách tự do đi chợ Sapa mua sắm về làm quà cho người thân. Quý khách có mặt tại văn phòng xe hoặc bến xe Sapa, lên xe giường nằm khởi hành về Hà Nội (dự kiến chuyến 13:30 hoặc 16:00- tùy ngày khởi hành). Về đến tp Hải Phòng, Quý khách tự túc phương tiện trở về nhà. Kết thúc chương trình tham quan.', 'Bamboo Sapa Hotel\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `giatour`
--

CREATE TABLE `giatour` (
  `MaGiaTour` char(10) NOT NULL,
  `ThoiGianBD` date DEFAULT NULL,
  `ThoiGianKT` date DEFAULT NULL,
  `GiaTien` int(11) DEFAULT NULL,
  `MaTour` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `giatour`
--

INSERT INTO `giatour` (`MaGiaTour`, `ThoiGianBD`, `ThoiGianKT`, `GiaTien`, `MaTour`) VALUES
('GT01', '2021-09-01', '2022-01-01', 5000000, 'T01'),
('GT02', '2021-09-01', '2022-01-01', 6000000, 'T02'),
('GT03', '2021-09-01', '2022-01-01', 7000000, 'T03'),
('GT04', '2021-09-01', '2022-01-01', 8000000, 'T04'),
('GT05', '2021-09-01', '2022-01-01', 10000000, 'T05'),
('GT06', '2021-09-01', '2022-01-01', 4000000, 'T06'),
('GT07', '2021-09-01', '2022-01-01', 650000, 'T07'),
('GT08', '2021-09-01', '2022-01-01', 1600000, 'T08'),
('GT09', '2021-09-01', '2022-01-01', 1279000, 'T09'),
('GT10', '2021-09-01', '2022-01-01', 2000000, 'T10'),
('GT11', '2021-09-01', '2022-01-01', 3000000, 'T11'),
('GT12', '2021-09-01', '2022-01-01', 1979000, 'T12'),
('GT13', '2021-09-01', '2022-01-01', 5700000, 'T13'),
('GT14', '2021-09-01', '2022-01-01', 5510000, 'T14'),
('GT15', '2021-09-01', '2022-01-01', 1550000, 'T15'),
('GT16', '2021-09-01', '2022-01-01', 8000000, 'T16'),
('GT17', '2021-09-01', '2022-01-01', 2350000, 'T17');

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `MaKhachHang` char(10) NOT NULL,
  `HoTen` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `SoCMND` char(13) DEFAULT NULL,
  `DiaChi` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `GioiTinh` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `SDT` char(12) DEFAULT NULL,
  `QuocTich` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MaKhachHang`, `HoTen`, `SoCMND`, `DiaChi`, `GioiTinh`, `SDT`, `QuocTich`) VALUES
('KH00', 'Nguyễn Trung Thông', '233259790', '170/14 Đường số 204 Cao Lỗ P, P4, Q8', 'Nam', '0586072996', 'Việt Nam'),
('KH01', 'A Giơ', '233259791', '612 Khu phố 2, Phường Bình Trị Đông B, Q.Bình Tân', 'Nam', '0586037708', 'Lào'),
('KH02', 'Y Djok', '233259792', '278 Ngô Quyền, Q8 ', 'Nam', '0586073704', 'Lào'),
('KH03', 'Trần Quỳnh Nhi', '233259794', '180B Lê Văn Sỹ, Q10', 'Nữ', '0586512274', 'Việt Nam'),
('KH04', 'Nguyễn Thị Trường An', '233259811', '03 Tân Tạo, Q8', 'Nữ', '0586935079', 'Việt Nam'),
('KH05', 'Nguyễn Thị Huyền', '233259812', 'B16 Song hành An phú – An Khánh Q.02 TPHCM', 'Nữ', '0909522702', 'Việt Nam'),
('KH06', 'Bùi Duy Khương', '233259810', '408 A2 C.c An Phú', 'Nam', '0918793284', 'Việt Nam'),
('KH07', 'Bùi Hoàng Lệ Diễm', '233259814', '503 Cao ốc  An Phú P.An Phú Q.2 TPHCM', 'Nữ', '0903734355', 'Việt Nam'),
('KH08', 'Bùi Kim Anh', '233259802', '84 Đường 1 khu B , An Phú Q.02 TPHCM', 'Nữ', '0989991211', 'Việt Nam'),
('KH09', 'Bùi Lê Thùy Trâm', '233259798', 'lầu 8 , phòng 801, Cao ốc An Khánh, Khu A P.AN KHANH Q.2 TPHCM', 'Nữ', '0907225050', 'Việt Nam'),
('KH10', 'Bùi Ngọc Minh', '233259800', '128 đường 17B p. An Phú Q.02 TPHCM', 'Nam', '0903683668', 'Việt Nam'),
('KH11', 'Lương Anh Phụng', '233259801', 'Long Trị, Long Mỹ, Hậu Giang', 'Nữ', '0989209286', 'Việt Nam'),
('KH12', 'Hoàng Trung Chính', '233256888', '300/43 Nguyễn Văn Linh, Phường Bình Thuận, Quận 7', 'Nam', '0907248555', 'Việt Nam'),
('KH13', 'Đặng Huỳnh Thành Nhân', '233259802', '41/27 Hương Lộ 14, H/Tân, Q.TP', 'Nam', '0908285117', 'Việt Nam'),
('KH14', 'Phùng Thị Ánh Tuyết', '233259821', '307 cao ốc an khánh song hành tổ 42, An Phú Q.02 TPHCM', 'Nữ', '0866718337', 'Việt Nam'),
('KH15', 'Nguyễn Văn Cường', '233259812', '57 đường 17A khu B an phú an khánh P.An Phú Q.02 TPHCM', 'Nam', '0913741210', 'Việt Nam'),
('KH16', 'Đặng Văn Phúc', '233259804', 'F106 cc 5 tầng Đường 8 B Khu an phú P.An Khánh Q.02 TPHCM', 'Nữ', '0905611688', 'Việt Nam'),
('KH17', 'Đinh Thị Mĩ Thanh', '233259000', '31 Trần Chánh Chiếu, P14, Q5', 'Nữ', '0909522702', 'Việt Nam'),
('KH18', 'Bùi Thị Mai Hương', '233252228', '1080 đường 10B p. An Phú Q.02 TPHCM', 'Nữ', '0983277149-0', 'Việt Nam'),
('KH19', 'Đoàn Bá Thước', '233259818', '31 Trần Chánh Chiếu, P14, Q5', 'Nam', '0903190490', 'Việt Nam'),
('KH20', 'Đỗ Minh Tuấn', '223253212', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0982782043', 'Việt Nam'),
('KH21', 'Đặng Thị Bích Liễu', '233252312', '246/28 Phan Huy Ích, P12, Q Gò Vấp', 'Nữ', '0989782343', 'Việt Nam'),
('KH22', 'Đào Trung Thu', '233259333', '1A73 Ấp 1 Phạm Văn Hai, H Bình Chánh', 'Nữ', '0918152212', 'Việt Nam'),
('KH23', 'Johny Ngô', '233259820', '276 Phan Văn Trị, Q.Bình Thạnh', 'Nam', '0918157857', 'Paris'),
('KH24', 'Đinh Thị Hoài Phương', '233259824', '2/41 Cao Thắng P5, Q3', 'Nữ', '0907222605', 'Việt Nam'),
('KH25', 'Henry', '233259825', '33/5, Trần Bình Trọng, P1, Q5', 'Nam', '0908327657', 'Mỹ');

-- --------------------------------------------------------

--
-- Table structure for table `loaichiphi`
--

CREATE TABLE `loaichiphi` (
  `MaLoaiChiPhi` char(10) NOT NULL,
  `TenLoaiChiPhi` varchar(200) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loaichiphi`
--

INSERT INTO `loaichiphi` (`MaLoaiChiPhi`, `TenLoaiChiPhi`) VALUES
('LCP01', 'Chi phí vé máy bay'),
('LCP02', 'Chi phí di chuyển'),
('LCP03', 'Chi phí lưu trú'),
('LCP04', 'Chi phí ăn uống'),
('LCP05', 'Chi phí tham quan'),
('LCP06', 'Chi phí phát sinh');

-- --------------------------------------------------------

--
-- Table structure for table `loaihinhdulich`
--

CREATE TABLE `loaihinhdulich` (
  `MaLoaiHinh` char(10) NOT NULL,
  `TenLoaiHinh` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loaihinhdulich`
--

INSERT INTO `loaihinhdulich` (`MaLoaiHinh`, `TenLoaiHinh`) VALUES
('LH01', 'Du lịch di động'),
('LH02', 'Du lịch kết hợp nghề nghiệp'),
('LH03', 'Du lịch xã hội và gia đình'),
('LH04', 'Du lịch sinh thái'),
('LH05', 'Du lịch văn hóa, lịch sử'),
('LH06', 'Du lịch Teambuilding'),
('LH07', 'Du lịch ẩm thực'),
('LH08', 'Du lịch Mice'),
('LH09', 'Du lịch nghỉ dưỡng'),
('LH10', 'Du lịch thể thao');

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `MaNV` char(10) NOT NULL,
  `HoTenNV` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  `GioiTinh` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `SĐT` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `DiaChi` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `Luong` int(11) DEFAULT NULL,
  `TrangThai` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`MaNV`, `HoTenNV`, `NgaySinh`, `GioiTinh`, `SĐT`, `DiaChi`, `Luong`, `TrangThai`) VALUES
('NV01', 'Lê Thanh Hiếu', '1997-07-17', 'Nam', '0906522017', '159 Lê Đức Thọ, Phường 16, Q. Gò Vấp\r\n', 10000000, NULL),
('NV02', 'Nguyễn Xuân Yến', '1999-09-24', 'Nữ', '0903659781', '375 Hai Bà Trưng, Phường 8, Quận 3, TP.HCM', 15000000, NULL),
('NV03', 'Lê Vân Anh', '1999-10-08', 'Nữ', '0786269875', '309 Hoàng Diệu, Phường 6, Quận 4, TP. Hồ Chí Minh', 20000000, NULL),
('NV04', 'Hồ Duy Khải', '1987-02-28', 'Nam', '0986789156', '161 Bạch Đằng, Phường 2, Tân Bình, TPHCM', 18000000, NULL),
('NV05', 'Kim Thị Thanh Thúy', '1988-05-28', 'Nữ', '0986498212', '1030 Tỉnh Lộ 43, P. Bình Chiểu, Q. Thủ Đức, TP. HCM', 15600000, NULL),
('NV06', 'Đoàn Quốc Dũng', '1983-07-03', 'Nam', '0697498126', '95C, đường Hòa Hưng, Phường 12, Quận 10', 12000000, NULL),
('NV07', 'Nguyễn Chí Hiếu', '1995-10-15', 'Nam', '0654988691', '458 Tân Thới Hiệp 02, Khu Phố 3A, P. Tân Thới Hiệp, Q. 12, TP. HCM', 15000000, NULL),
('NV08', 'Lý Anh Kiệt', '1995-10-23', 'Nam', '0659875196', '644 Kinh Dương Vương, An Lạc, Bình Tân, TP.HCM ', 15000000, NULL),
('NV09', 'Nguyễn Ngọc Phi Giao', '1993-08-02', 'Nữ', '0986546214', '596 Nguyễn Chí Thanh, Phường 7, Quận 11, TP. HCM ', 1000000, NULL),
('NV10', 'Nguyễn Văn Tuấn Anh', '1999-02-03', 'Nam', '0986162164', '137 Nguyễn Thái Sơn, Phường 4, Gò Vấp, TP. HCM', 16000000, NULL),
('NV11', 'Ngô Thị Thúy Liễu', '1985-03-12', 'Nữ', '0964564589', '514 Đường 3 Tháng 2, P. 14, Q. 10, Tp. Hồ Chí Minh ', 15600000, NULL),
('NV12', 'Lê Quốc Nghĩa', '1999-11-26', 'Nam', '0659875165', '107 Điện Biên Phủ, Đa Kao, Quận 1, TP.HCM ', 15000000, NULL),
('NV13', 'Lê Kiều Nguyệt', '1993-01-14', 'Nữ', '065498479', '11 Nguyễn Kiệm, Phường 3, Quận Gò Vấp, TP.HCM ', 15000000, NULL),
('NV14', 'Vũ Phan Hoàng Phúc', '1993-02-28', 'Nam', '0316541236', '161 Bạch đằng, P. 2, Q. Tân Bình, Thành phố Hồ Chí Minh', 16500000, NULL),
('NV15', 'Trần Ngọc Quyên', '1997-09-09', 'Nữ', '0956498456', '248A Nơ Trang Long, P. 12, Q. Bình Thạnh, TP.HCM', 13000000, NULL),
('NV16', 'Võ Công Tạo', '1994-02-23', 'Nam', '0783216545', '514 Đường 3 Tháng 2, Phường 14, Quận 1', 18900000, NULL),
('NV17', 'Đặng Thị Phương Thảo', '1985-12-08', 'Nữ', '0990065465', '31 Hồ Biểu Chánh, Phường 12, Quận Phú Nhuận, TP.HCM', 15000000, NULL),
('NV18', 'Trần Mai Thảo', '1980-12-08', 'Nữ', '0786546597', '1 Tô Hiến Thành, Phường 14, Quận 10, Thành phố Hồ Chí Minh.', 16500000, NULL),
('NV19', 'Trần Trương Phương Thắm', '1881-12-01', 'Nữ', '0909616545', '148 Hoàng Hoa Thám, Phường 12, Quận Tân Bình, TP.HCM', 12000000, NULL),
('NV20', 'Huỳnh Thị Minh Thư', '1987-05-05', 'Nữ', '065465469', '115 Tân Quý, P. Tân Quý, Q.Tân Phú', 456000, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `phanbonvdoan`
--

CREATE TABLE `phanbonvdoan` (
  `MaNhanvien` char(10) NOT NULL,
  `MaDoan` char(10) NOT NULL,
  `NhiemVu` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `phanbonvdoan`
--

INSERT INTO `phanbonvdoan` (`MaNhanvien`, `MaDoan`, `NhiemVu`) VALUES
('NV06', 'DOAN03', 'Phó đoàn');

-- --------------------------------------------------------

--
-- Table structure for table `thamquan`
--

CREATE TABLE `thamquan` (
  `MaTour` char(10) NOT NULL,
  `MaDiaDiem` char(10) NOT NULL,
  `ThuTu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `thamquan`
--

INSERT INTO `thamquan` (`MaTour`, `MaDiaDiem`, `ThuTu`) VALUES
('T01', 'DD01', 1),
('T01', 'DD02', 2),
('T01', 'DD03', 3),
('T02', 'DD04', 1),
('T02', 'DD05', 2),
('T02', 'DD06', 3),
('T03', 'DD07', 1),
('T03', 'DD08', 2),
('T03', 'DD09', 3),
('T04', 'DD10', 1),
('T04', 'DD11', 2),
('T04', 'DD12', 3),
('T05', 'DD13', 1),
('T05', 'DD14', 2),
('T05', 'DD15', 3),
('T06', 'DD16', 1),
('T06', 'DD17', 2),
('T06', 'DD18', 3),
('T07', 'DD19', 1),
('T07', 'DD20', 2),
('T08', 'DD21', 1),
('T08', 'DD22', 2),
('T08', 'DD23', 3),
('T08', 'DD24', 4),
('T08', 'DD25', 5),
('T08', 'DD26', 6),
('T09', 'DD27', 1),
('T09', 'DD28', 2),
('T09', 'DD29', 3),
('T09', 'DD30', 4),
('T10', 'DD31', 1),
('T10', 'DD32', 2),
('T10', 'DD33', 3),
('T11', 'DD34', 1),
('T11', 'DD35', 2),
('T11', 'DD36', 3),
('T11', 'DD37', 4),
('T12', 'DD38', 1),
('T12', 'DD39', 2),
('T12', 'DD40', 3),
('T12', 'DD41', 4),
('T12', 'DD42', 5),
('T13', 'DD43', 1),
('T13', 'DD44', 2),
('T13', 'DD45', 3),
('T13', 'DD46', 4),
('T13', 'DD47', 5),
('T13', 'DD48', 6),
('T13', 'DD49', 7),
('T13', 'DD50', 8),
('T13', 'DD51', 9),
('T14', 'DD52', 1),
('T14', 'DD53', 2),
('T14', 'DD54', 3),
('T14', 'DD55', 4),
('T14', 'DD56', 5),
('T14', 'DD57', 6),
('T15', 'DD58', 1),
('T15', 'DD59', 2),
('T15', 'DD60', 3),
('T15', 'DD61', 4),
('T15', 'DD62', 5),
('T15', 'DD63', 6),
('T15', 'DD64', 7),
('T15', 'DD65', 8),
('T16', 'DD66', 1),
('T16', 'DD67', 2),
('T16', 'DD68', 3),
('T16', 'DD69', 4),
('T16', 'DD70', 5),
('T16', 'DD71', 6),
('T16', 'DD72', 7),
('T17', 'DD73', 1),
('T17', 'DD74', 2),
('T17', 'DD75', 3),
('T17', 'DD76', 4),
('T17', 'DD77', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tourdulich`
--

CREATE TABLE `tourdulich` (
  `MaTour` char(10) NOT NULL,
  `TenTour` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `DacDiem` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `MaLoaiHinh` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tourdulich`
--

INSERT INTO `tourdulich` (`MaTour`, `TenTour`, `DacDiem`, `MaLoaiHinh`) VALUES
('T01', 'Sài Gòn - Đà Nẵng', '- Chiêm ngưỡng khung cảnh thiên nhiên ngoạn mục trên Cây Cầu Vàng.', 'LH01'),
('T02', 'Sài Gòn -  Hạ Long', '- Tham quan cửa khẩu Hoành Mô, “check-in” cột mốc 1317. ', 'LH02'),
('T03', 'Sài Gòn - Cà Mau', 'Đến Đất Mũi, đoàn tham quan Công viên Văn Hóa Mũi Cà Mau - tọa lạc trong khu dự trữ sinh quyển thế giới Mũi Cà Mau được UNESCO công nhận vào tháng 5/2009.', 'LH03'),
('T04', 'Sài Gòn - Đà Lạt', '- Tham quan Khu Du Lịch Trang Trại Rau và Hoa (Làng hoa Vạn Thành), nằm trải rộng cả một thung lũng với bốn bề là rau và hoa đẹp tuyệt vời.', 'LH04'),
('T05', 'Sài Gòn - Huế', '- Hội An với Chùa Cầu Nhật Bản, Nhà Cổ hàng trăm năm tuổi, Hội Quán Phước Kiến & Xưởng thủ công mỹ nghệ.', 'LH05'),
('T06', 'Sài Gòn - Nha Trang', '- Tham quan thành phố biển Nha Trang xinh đẹp\r\n- Tới khu du lịch Hòn Lao - đảo Khỉ, thư giãn, xem xiếc hoặc thử các môn thể thao bãi biển', 'LH03'),
('T07', 'TP.HCM - Củ Chi', '- Du Lịch Xanh Cùng Lữ Hành Saigontourist\r\n- Tham quan Khu Di Tích Địa Đạo Củ Chi – Đền Bến Dược', 'LH05'),
('T08', 'TP. Hồ Chí Minh - Cần Giờ ', '- Du Lịch Xanh Cùng Lữ Hành Saigontourist\r\n- Tham quan Chiến Khu Rừng Sác – Khu Du Lịch Sinh Thái Vàm Sát', 'LH05'),
('T09', 'TP. Hồ Chí Minh - Long An', '- Tham quan KDL Cánh Đồng Bất Tận – Khu bảo tồn và phát triển dược liệu Đồng Tháp Mười\r\n- Khám khá khu vực Rừng Tràm Gió nguyên sinh -  len lỏi giữa hệ thống kênh rạch với thảm thực vật xanh mướt; những cây tràm gió gần trăm năm tuổi được bao bọc bằng dây leo bòng bong, những đầm hoa sen, rặng hoa súng nở rộ thơm ngát', 'LH04'),
('T10', 'TP. Hồ Chí Minh - Tây Ninh ', '- Tham quan Củ Chi – vùng đất được phong tặng danh hiệu “đất thép thành đồng”\r\n- Trải nghiệm tuyến cáp treo Vân Sơn, được chứng nhận là hệ thống nhà ga lớn nhất thế giới bởi tổ chức kỉ lục Guinness, chỉ mất 4 phút', 'LH04'),
('T11', 'TP. Hồ Chí Minh - Vĩnh Long ', '- Trải nghiệm một đêm tại homestay Út Trinh\r\n- Tham quan các làng nghề :đan lục bình, làm bánh tráng rế, may nón đệm/cối..\r\n- Tham gia tát mương bắt cá', 'LH03'),
('T12', 'TP. Hồ Chí Minh - Cần Thơ', '- Du thuyền trên sông Mekong ngắm cảnh bốn cù lao Long, Lân Qui, Phụng\r\n- Tham quan chợ nổi Cái Răng - một chợ nổi lớn của Đồng Bằng sông Cửu Long\r\n- Viếng Thiền viện Trúc Lâm Phương Nam', 'LH01'),
('T13', 'TP. Hồ Chí Minh - Côn Đảo', '- Đến với Côn Đảo (hay với các tên khác như Côn Sơn, Côn Lôn) đã gợi cho người nghe hình ảnh mịt mù của một khu vực xa xôi, mờ mịt, có lúc được ví như địa ngục trần gian nổi tiếng từ thời Pháp thuộc đến giữa năm 1975. \r\n- Tham quan Cầu tàu 914, Trại Phú Hải, Chuồng Cọp Pháp - Mỹ, Mũi Cá Mập, Đỉnh Tình Yêu.', 'LH05'),
('T14', 'TP. Hồ Chí Minh - Phú Quốc', '- Tham quan suối Tranh - quý khách có thể đi dạo trong rừng, thư giãn, tắm suối (suối đặc biệt nhiều nước trong mùa hè)\r\n- Trải nghiệm “Cáp treo 3 dây vượt biển dài nhất thế giới tại Hòn Thơm”', 'LH03'),
('T15', 'TP. Hồ Chí Minh - Buôn Ma Thuột', '- Trải nghiệm cảm giác thú vị khi ngồi trên lưng những Chú voi Bản Đôn vượt Sông Serepok, thử cảm giác phiêu lưu khi chinh phục hệ thống cầu treo dài hơn 1km được bắt trên những sàn si.\r\n- Viếng thăm Chùa Sắc Tứ Khải Đoan. Ngôi Chùa đầu tiên đặt nền móng cho Phật giáo tại Tây Nguyên với kiến trúc độc đáo.\r\n- Quý khách tự do tham quan khu du lịch Sinh Thái KoTam, ngắm vườn hoa, khu bến nước,….', 'LH04'),
('T16', 'TP. Hồ Chí Minh - Ninh Bình', '- Hành trình tham quan kỉ niệm 130 năm chủ tịch Hồ Chí Minh\r\n- Tham quan quê Ngoại – Làng Hoàng Trù là nơi đã sinh thành ra Bác Hồ\r\n- Tham quan và viếng khu du lịch tâm linh Chùa Tam Chúc tại Hà Nam – Một trong những quần thể tâm linh lớn nhất thế giới với cảnh sắc non nước tuyệt đẹp. \r\n- Xem phim về cuộc đời của chủ tịch Hồ Chí Minh', 'LH03'),
('T17', 'TP. Hồ Chí Minh - Sapa', '- Du lịch Sapa: tham quan nhà thờ Đá, núi Hàm Rồng, ...\r\n- Đặc biệt quý khách có cơ hội chinh phục đình Fasipan', 'LH09'),
('T18', NULL, NULL, NULL),
('T19', NULL, NULL, NULL),
('T20', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chiphi`
--
ALTER TABLE `chiphi`
  ADD PRIMARY KEY (`MaChiPhi`),
  ADD KEY `fk_CP_MaDoan` (`MaDoan`),
  ADD KEY `fk_CP_MaLoaiChiPhi` (`MaLoaiChiPhi`);

--
-- Indexes for table `chitietdoan`
--
ALTER TABLE `chitietdoan`
  ADD PRIMARY KEY (`MaDoan`,`MaKhachHang`),
  ADD KEY `fk_CTD_MaKhachHang` (`MaKhachHang`);

--
-- Indexes for table `diadiem`
--
ALTER TABLE `diadiem`
  ADD PRIMARY KEY (`MaDiaDiem`);

--
-- Indexes for table `doandulich`
--
ALTER TABLE `doandulich`
  ADD PRIMARY KEY (`MaDoan`),
  ADD KEY `fk_DDL_MaTour` (`MaTour`);

--
-- Indexes for table `giatour`
--
ALTER TABLE `giatour`
  ADD PRIMARY KEY (`MaGiaTour`),
  ADD KEY `fk_GT_MaTour` (`MaTour`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`MaKhachHang`);

--
-- Indexes for table `loaichiphi`
--
ALTER TABLE `loaichiphi`
  ADD PRIMARY KEY (`MaLoaiChiPhi`);

--
-- Indexes for table `loaihinhdulich`
--
ALTER TABLE `loaihinhdulich`
  ADD PRIMARY KEY (`MaLoaiHinh`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`MaNV`);

--
-- Indexes for table `phanbonvdoan`
--
ALTER TABLE `phanbonvdoan`
  ADD PRIMARY KEY (`MaNhanvien`,`MaDoan`),
  ADD KEY `fk_PBNV_MaDoan` (`MaDoan`);

--
-- Indexes for table `thamquan`
--
ALTER TABLE `thamquan`
  ADD PRIMARY KEY (`MaTour`,`MaDiaDiem`),
  ADD KEY `fk_TQ_MaDiaDiem` (`MaDiaDiem`);

--
-- Indexes for table `tourdulich`
--
ALTER TABLE `tourdulich`
  ADD PRIMARY KEY (`MaTour`),
  ADD KEY `fk_TDL_MaLoaiHinh` (`MaLoaiHinh`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chitietdoan`
--
ALTER TABLE `chitietdoan`
  ADD CONSTRAINT `fk_CTD_MaDoan` FOREIGN KEY (`MaDoan`) REFERENCES `doandulich` (`MaDoan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_CTD_MaKhachHang` FOREIGN KEY (`MaKhachHang`) REFERENCES `khachhang` (`MaKhachHang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doandulich`
--
ALTER TABLE `doandulich`
  ADD CONSTRAINT `fk_DDL_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `giatour`
--
ALTER TABLE `giatour`
  ADD CONSTRAINT `fk_GT_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `thamquan`
--
ALTER TABLE `thamquan`
  ADD CONSTRAINT `fk_TQ_MaDiaDiem` FOREIGN KEY (`MaDiaDiem`) REFERENCES `diadiem` (`MaDiaDiem`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_TQ_MaTour` FOREIGN KEY (`MaTour`) REFERENCES `tourdulich` (`MaTour`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tourdulich`
--
ALTER TABLE `tourdulich`
  ADD CONSTRAINT `fk_TDL_MaLoaiHinh` FOREIGN KEY (`MaLoaiHinh`) REFERENCES `loaihinhdulich` (`MaLoaiHinh`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
